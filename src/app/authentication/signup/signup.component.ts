import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { DatePipe } from '@angular/common';
declare var paypal;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  @ViewChild('paypal') paypal: ElementRef;
  public idcliente;
  public idplan ='pwdzaiscvyeddexw72gt';
  formtajeta:boolean = false;
  suscrito:boolean = false;
  private baseURL = 'https://animatiomx.com/odonto/';
  private baseURL2 = 'https://animatiomx.com/odonto/openpay/';
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  @ViewChild(FormGroupDirective) formGroupDirective2: FormGroupDirective;
  RegistroForm: FormGroup;
  TarjetaForm: FormGroup;
  submitted = false;
  hide = true;
  chide = true;
  coincidir=true;
  loader=false;
  hoy= new Date();
  manana;
  fecha = '';
  hora='';
  url;
  idSubscripcion;
  accestoken;
  datosTarjeta;
  nombrecliente;
  constructor(
    private httpClient:HttpClient,private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
  ) {
    this.manana = new Date(this.hoy.getTime() + 1000 * 60 * 60 * 24);
     this.fecha = this.datePipe.transform(this.manana, 'yyyy-MM-dd');
     this.hora = this.datePipe.transform(new Date(), 'hh:mm:ss');
  }
  ngOnInit() {
    const self = this;
    this.RegistroForm = this.formBuilder.group({
      nombreUsuario: ['', Validators.required],
      apellido: ['', Validators.required],
      correo: ['',[Validators.required, Validators.email]],
      telefono: ['', Validators.required],
      direccion: ['', Validators.required],
      contra: ['', Validators.required],
      contraC: ['', Validators.required],
    });

    this.TarjetaForm = this.formBuilder.group({
      numerocard: ['', Validators.required],
      anio: ['',Validators.required],
      mes: ['', Validators.required],
      cvv: ['', Validators.required],
    });

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
  myHeaders.append("Authorization", "Basic QWV0cExaSVFHYTBqa2E1ZXlLWG9iLUYxWWg2Yy1KQ1p3MmRSa1BraU1LUkZFV2EwdkpQZmtXMFdXYlVBblJvVlkwNlF5Zl83cDFaTTN4M1E6RUFwc1c1TlBzaDVaYnpuc3I4M0MxRlhPYTczb2FPakNsa2l3cm81YV9YNDB6MGJGY3ljcE9EQVp6V25ZOTNvREdMa3BrTENqYlBDc0t1TWY=");

  var urlencoded = new URLSearchParams();
  urlencoded.append("grant_type", "client_credentials");

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: urlencoded,
  };

  fetch("https://api-m.sandbox.paypal.com/v1/oauth2/token", requestOptions)
    .then(response => response.text())
    .then(result => {
      let resp = JSON.parse(result);
      this.accestoken=resp['access_token']
      var ProductHeaders = new Headers();
        ProductHeaders.append("Content-Type", "application/json");
        ProductHeaders.append("Authorization", `Bearer ${resp['access_token']}`);

        var raw = JSON.stringify({"name":"Suscripción Animatiomx-Odonto","description":"Acceso a la plataforma Odonto","type":"DIGITAL","category":"SOFTWARE","image_url":"https://example.com/streaming.jpg","home_url":"https://example.com/home"});

        var requestOptionsProduct = {
          method: 'POST',
          headers: ProductHeaders,
          body: raw
        };

        fetch("https://api-m.sandbox.paypal.com/v1/catalogs/products", requestOptionsProduct)
          .then(Producto => Producto.text())
          .then(idProduct => {
            let idProducto = JSON.parse(idProduct);

            var PlanHeaders = new Headers();
            PlanHeaders.append("Content-Type", "application/json");
            PlanHeaders.append("Authorization", `Bearer ${resp['access_token']}`);

            var rawPlan = JSON.stringify({"product_id":`${idProducto['id']}`,"name":"Plan de subscripción Odonto","description":"Subscripción a la plataforma Odonto","status":"ACTIVE","billing_cycles":[{"frequency":{"interval_unit":"MONTH","interval_count":1},"tenure_type":"REGULAR","sequence":1,"total_cycles":999,"pricing_scheme":{"fixed_price":{"value":"1","currency_code":"USD"}}}],"payment_preferences":{"auto_bill_outstanding":true,"setup_fee_failure_action":"CONTINUE","payment_failure_threshold":3},"taxes":{"percentage":"0","inclusive":false}});

            var PlanrequestOptions = {
              method: 'POST',
              headers: PlanHeaders,
              body: rawPlan,
            };

            fetch("https://api.sandbox.paypal.com/v1/billing/plans", PlanrequestOptions)

              .then(responsePlan => responsePlan.text())
              .then(resultPlan => {
                let idPlan = JSON.parse(resultPlan);
                this.idplan = idPlan['id'];
              })
              .catch(errorPlan => console.log('error', errorPlan));
          })
          .catch(error1 => console.log('error', error1));
    })
    .catch(error => console.log('error', error));

    paypal.Buttons({
      createSubscription: function (data, actions) {
        return actions.subscription.create({
          'plan_id': self.idplan,
        });
      },
      onApprove: function (data, actions) {
        console.log(data.subscriptionID);
        self.idSubscripcion = data.subscriptionID;
        self.suscrito=true;
        self.onSubmit();
      },
      onCancel: function (data) {
        // Show a cancel page, or return to cart
        console.log(data);
      },
      onError: function (err) {
        // Show an error page here, when an error occurs
        console.log(err);
      }

    }).render(document.getElementById('roott'));

  }
  get f() {
    return this.RegistroForm.controls;
  }

 public creausuario(){
   if (this.RegistroForm.get('nombreUsuario').value!=''&&this.RegistroForm.get('correo').value!='') {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = {'nombre':this.RegistroForm.get('nombreUsuario').value,'email':this.RegistroForm.get('correo').value};
    const URL: any = this.baseURL2 + 'crea_cliente.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        console.log(respuesta);
        this.idcliente = respuesta['id'];
        this.nombrecliente = respuesta['name'];
        this.formtajeta=true;
      });
   }else{
      this.showNotification(
          'black',
          'debe llenar los campos nombre y correo y dirección',
          'bottom',
          'center'
            );
     return;
   }
  }

  verFormTarjeta(){
    if (this.formtajeta==false) {
      this.formtajeta=true;
    }else{
      this.formtajeta=false;
    }
  }

  crearSuscripcion(){
    this.loader=true;
    this.datosTarjeta=this.TarjetaForm.value;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'datostajeta':this.TarjetaForm.value,'idcliente':this.idcliente};
    const URL: any = this.baseURL2 + 'suscripcion.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        console.log(respuesta);
          if (respuesta['id']) {
            this.loader=false;
            this.showNotification(
            'snackbar-success',
            'Se regitro su pago y el usuario',
            'bottom',
            'center'
              );
            setTimeout(() => this.formGroupDirective2.resetForm(), 0);
            this.idSubscripcion=respuesta['id'];
            this.onSubmit();
            this.router.navigateByUrl('/authentication/signin');
            }else{
            this.loader=false;
            this.showNotification(
            'black',
            'Los datos de la tarjeta no son validos',
            'bottom',
            'center'
              );
          }
        });
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.RegistroForm.valid) {
      if (this.RegistroForm.get('contra').value!==this.RegistroForm.get('contraC').value) {
        this.coincidir=false;
        // console.log('las contraseñas deben coincidir');
        this.showNotification(
          'black',
          'las contraseñas deben coincidir',
          'bottom',
          'center'
            );
      }else{
        console.log('Form Value', this.RegistroForm.value);
        const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
        const options: any = { 'caso': 0,'usuario':this.RegistroForm.value,'idsubs':this.idSubscripcion};
        const URL: any = this.baseURL + 'usuario.php';
        this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
          respuesta => {
          // console.log(respuesta);
          this.showNotification(
          'snackbar-success',
          'Se regitro su pago y el usuario',
          'bottom',
          'center'
            );
            setTimeout(() => this.formGroupDirective.resetForm(), 0);
            this.router.navigateByUrl('/authentication/signin');
          });
      }
    } else {
      return;
    }
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 5000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}