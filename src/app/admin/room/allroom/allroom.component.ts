import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RoomService } from './room.service';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Room } from './room.model';
import { DataSource } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormDialogComponent } from './dialog/form-dialog/form-dialog.component';
import { DeleteDialogComponent } from './dialog/delete/delete.component';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Notas } from './notas.model';
import { NotasService } from './notas.service';
import { AuthService } from 'src/app/shared/security/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-allroom',
  templateUrl: './allroom.component.html',
  styleUrls: ['./allroom.component.sass'],
})
export class AllroomComponent implements OnInit {
  dataChange: BehaviorSubject<Notas[]> = new BehaviorSubject<Notas[]>([]);
  idNota = '';
  NotasV: any = [];
  valor = '';
  baseURL = 'https://animatiomx.com/odonto/';
  roomForm: FormGroup;
  idClinica = '';

  constructor(
  private httpClient: HttpClient,
  private fb: FormBuilder,
  public servicioNota: NotasService,
  private authService: AuthService,
  private snackBar: MatSnackBar,
  private router: Router
  ) {
      this.idClinica = this.authService.getIdClinica();
      if (this.idClinica === null){
        let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
        snack.onAction().subscribe( () => this.iraclinica());
      }
      this.roomForm = this.fb.group({
        Note: ['', [Validators.required]],
      });
  }

  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }

  ngOnInit(): void {
    this.verificarNota();
  }
  onSubmit() {
    console.log('Form Value', this.roomForm.value);
  }
  public confirmAdd(): void {
    if (this.valor === '')
    {
      this.servicioNota.agregarNota(this.roomForm.getRawValue(), String(this.idClinica)).subscribe( respuesta =>{
        const res = respuesta;
        if (res.toString() === 'Se inserto'){
          this.showNotification(
            'snackbar-success',
            'Nota agregada correctamente...!!!',
            'bottom',
            'center'
          );
          this.verificarNota();
        }else {
          this.showNotification(
            'snackbar-error',
            'Ocurrio un error, intente de nuevo por favor...!!!',
            'bottom',
            'center'
          );
        }
      });;
    }
    if (this.valor === '1') {
      this.servicioNota.ModificarNota(this.roomForm.getRawValue(), Number(this.idNota)).subscribe( respuesta =>{
        const res = respuesta;
        if (res.toString() === 'Se modifico'){
          this.showNotification(
            'snackbar-success',
            'Nota editada correctamente...!!!',
            'bottom',
            'center'
          );
          this.verificarNota();
        }else {
          this.showNotification(
            'snackbar-error',
            'Ocurrio un error, intente de nuevo por favor...!!!',
            'bottom',
            'center'
          );
        }
      });
    }
  }
  
  // metodo para mandar toast 

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  // Consulta si ya tiene clinica
  verificarNota() {
    const idclinica = this.authService.getIdClinica();
    if (idclinica === null) {
      this.valor = '';
    } else {
      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { caso: 0, idc: idclinica };
      const URL: any = this.baseURL + 'notas.php';
      this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
        respuesta => {
          this.NotasV = respuesta;
          if (this.NotasV !== null){
            this.dataChange.next(this.NotasV);
            console.log(respuesta);
            console.log(this.dataChange);
          }
          console.log(this.NotasV);
          for (let i = 0; i < this.NotasV.length; i++) {
            const element = this.NotasV[0].Nota;
            this.roomForm.setValue({
              Note: this.NotasV[0].Nota,
            });
            this.idNota = this.NotasV[0].idNotas;
            }
          this.valor = '1';
          console.log(this.valor + 'este es el valor');
        },
        (error: HttpErrorResponse) => {
          console.log(error.name + ' ' + error.message);
        }
        );
    }

  }
}