import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import {  Router } from '@angular/router';
import { Notas } from './notas.model';
import Swal from 'sweetalert2';
@Injectable({
  providedIn: 'root'
})
export class NotasService {
  dataChange: BehaviorSubject<Notas[]> = new BehaviorSubject<Notas[]>([]);
  diagnosticos: any = [];
  baseURL = 'https://animatiomx.com/odonto/';
  dialogData: any;
  valor = '';
  constructor(private router: Router,
              private httpClient: HttpClient) {}
    // Agrega nota
    agregarNota(nota: Notas, Clinica: String) {
      this.dialogData = nota;
      let nom = this.dialogData.Note;

      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { 'caso': 1, 'not':nom, 'idc': Clinica};
      const URL: any = this.baseURL + 'notas.php';
      return this.httpClient.post(URL, JSON.stringify(options), headers);

    }
        // Modifica receta
        ModificarNota(notaS: Notas, idNota: number) {
          this.dialogData = notaS;
          let nota = this.dialogData.Note;
          const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
          const options: any = { 'caso': 2, 'not':nota, 'idn': idNota};
          const URL: any = this.baseURL + 'notas.php';
          return this.httpClient.post(URL, JSON.stringify(options), headers);
        }
}
