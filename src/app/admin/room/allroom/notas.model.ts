export class Notas {
    idNotas: number;
    Nota: string;
    idClinica: number;
    constructor(nota) {
        {
          this.idNotas = nota.idNotas || this.getRandomID();
          this.Nota = nota.Nota || '';
          this.idClinica = nota.idClinica || '';
        }
      }
      public getRandomID(): string {
        var S4 = function() {
          return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
      }
}