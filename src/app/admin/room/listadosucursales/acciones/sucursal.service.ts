import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Sucursal } from '../acciones/sucursal.model';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class SucursalService {
  //intercambio de datos
  dataChange: BehaviorSubject<Sucursal[]> = new BehaviorSubject<Sucursal[]>([]);
  baseURL = 'https://animatiomx.com/odonto/';
  sucursalData: any;
  sucursal: any = [];
  dialogData: any;

  constructor(private httpClient: HttpClient) {}
  // poner los valores de la consulta en el dataChange
  get data(): Sucursal[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */

  // obtener en un arreglo mis sucursales
  obtenersucursales(idC): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0,'idClinica': idC};
    const URL: any = this.baseURL + 'sucursal.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.sucursal = respuesta;
        if (this.sucursal !== null){
          this.dataChange.next(this.sucursal);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }

  // agregar sucursal
  agregarsucursal(sucursal: Sucursal,idclinica: Number) {
    
    this.dialogData = sucursal;

    let noms = this.dialogData.NombreSucursal;
    let rfc = this.dialogData.Rfc;
    let Direccion = this.dialogData.Direccion;
    let Telefono = this.dialogData.Telefono;
    let Whatsapp = this.dialogData.Whatsapp;
    let Correo = this.dialogData.Correo;
    let Nombrecorto = this.dialogData.Nombrecorto;
    let logo = this.dialogData.logo;
    let fecha = moment(this.dialogData.fecha).format('YYYY/MM/DD');

    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1,'Nombresucursal':noms,'rfc':rfc,'Direccion':Direccion,'Telefono':Telefono,'WhatsApp':Whatsapp,
    'Correo':Correo,'Nombrecort':Nombrecorto,'Logo':logo,'Fecha':fecha,'idClinica':idclinica};
    const URL: any = this.baseURL + 'sucursal.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);

  }
   //actualizar informacion de sucursal
  actualizarsucursal(sucursal: Sucursal,idSucursal: Number) {
    this.dialogData = sucursal;
    let noms = this.dialogData.NombreSucursal;
    let rfc = this.dialogData.Rfc;
    let Direccion = this.dialogData.Direccion;
    let Telefono = this.dialogData.Telefono;
    let Whatsapp = this.dialogData.Whatsapp;
    let Correo = this.dialogData.Correo;
    let Nombrecorto = this.dialogData.Nombrecorto;
    let logo = this.dialogData.logo;
    let idS = idSucursal;
    
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'Nombresucursal':noms,'rfc':rfc,'Direccion':Direccion,'Telefono':Telefono,'WhatsApp':Whatsapp,
    'Correo':Correo,'Nombrecort':Nombrecorto,'Logo':logo,'idScursal':idS};
    const URL: any = this.baseURL + 'sucursal.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);
    
  }

}
