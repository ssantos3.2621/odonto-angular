export class Sucursal {
    idSucursal: number;
    NombreSucursal: string;
    Rfc: string;
    Direccion: string;
    Telefono: number;
    Whatsapp: number;
    Correo: string;
    Nombrecorto: string;
    Logo: string;
    Fecha: string;
    idClinica: number;
    fechahoy = new Date();
  
    constructor(sucursal) {
      {
        this.idSucursal = sucursal.idSucursal || this.getRandomID();
        this.NombreSucursal = sucursal.NombreSucursal || '';
        this.Rfc= sucursal.Rfc || '';
        this.Direccion = sucursal.Direccion || '';
        this.Telefono = sucursal.Telefono || '';
        this.Whatsapp = sucursal.Whatsapp || '';
        this.Correo = sucursal.Correo || '';
        this.Nombrecorto = sucursal.Nombrecorto || '';
        this.Logo = sucursal.Logo || '';
        this.Fecha = sucursal.Fecha || this.fechahoy;
        this.idClinica = sucursal.idClinica || '';
      }
    }
    public getRandomID(): string {
      var S4 = function() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      };
      return S4() + S4();
    }
  }
  