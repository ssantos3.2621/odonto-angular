import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpHeaders } from '@angular/common/http';
import * as xlsx from 'xlsx';
import { SubirarchivoService } from 'src/app/subirarchivo.service';
import { SucursalService } from './acciones/sucursal.service';
import { Sucursal } from './acciones/sucursal.model';
import { AuthService } from 'src/app/shared/security/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listadosucursales',
  templateUrl: './listadosucursales.component.html',
  styleUrls: ['./listadosucursales.component.scss']
})
export class ListadosucursalesComponent implements OnInit {


  @ViewChild('paginator', { static: true }) paginadorSucursal: MatPaginator;
  @ViewChild('tbsucursal', { static: true }) ordenarsucursal: MatSort;
  @ViewChild('filtroSucursales', { static: true }) filtroSucursales: ElementRef;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  clientes: any = [];
  columnasucursales = [
    'select',
    'img',
    'name',
    'direcccion',
    'whatsapp',
    'correo',
    'actions',
  ];
  SucursalDB: SucursalService | null;
  datosucursal: sucursalDatos | null;
  selection = new SelectionModel<Sucursal>(true, []);
  idSucursal: number;
  idClinica = '';


  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public sucursalService: SucursalService,
    private snackBar: MatSnackBar,
    private enviandoImagen: SubirarchivoService,
    private authService: AuthService,
    private router: Router,
  ) {  
    this.idClinica = this.authService.getIdClinica();
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  ngOnInit() {
    if (this.idClinica !== null){
      this.cargardatossucursal();
    }else{
      this.cargardatossucursal();
      let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
      snack.onAction().subscribe( () => this.iraclinica());
    }
  }

  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
      xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Pacientes.xlsx');
  }

  refrescartablasucursal() {
    this.cargardatossucursal();
  }

  private refrescartabladesucursal() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  
  public cargardatossucursal() {
    this.SucursalDB = new SucursalService(this.httpClient);
    this.datosucursal = new sucursalDatos(
      this.SucursalDB,
      this.paginator,
      this.sort,
      this.idClinica
    );
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.datosucursal) {
          return;
        }
        this.datosucursal.filter = this.filter.nativeElement.value;
      });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}
export class sucursalDatos extends DataSource<Sucursal> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  filteredData: Sucursal[] = [];
  renderedData: Sucursal[] = [];
  constructor(
    public _exampleDatabase: SucursalService,
    public _paginator: MatPaginator,
    public _sort: MatSort,
    public idclinica
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Sucursal[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page,
    ];
    this._exampleDatabase.obtenersucursales(this.idclinica);
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._exampleDatabase.data
          .slice()
          .filter((sucursal: Sucursal) => {
            const searchStr = (
              sucursal.NombreSucursal
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {}
  /** Returns a sorted copy of the database data. */
  sortData(data: Sucursal[]): Sucursal[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.idSucursal, b.idSucursal];
          break;
        case 'name':
          [propertyA, propertyB] = [a.NombreSucursal, b.NombreSucursal];
          break;
        case 'gender':
          [propertyA, propertyB] = [a.Rfc, b.Rfc];
          break;
        case 'date':
          [propertyA, propertyB] = [a.Direccion, b.Direccion];
          break;
        case 'mobile':
          [propertyA, propertyB] = [a.Telefono, b.Telefono];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}

