import { Component } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubirarchivoService } from 'src/app/subirarchivo.service';
import { Sucursal } from '../listadosucursales/acciones/sucursal.model';
import { SucursalService } from '../listadosucursales/acciones/sucursal.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { AuthService } from 'src/app/shared/security/auth.service';


@Component({
  selector: 'app-edit-allotment',
  templateUrl: './edit-allotment.component.html',
  styleUrls: ['./edit-allotment.component.sass']
})
export class EditAllotmentComponent {

  private baseURL = 'https://animatiomx.com/odonto/';
  sucursal: Sucursal;
  formulariosucursal: FormGroup;

  idSucursal = '';
  Archivoseleccionado: null;
  nombrefoto: any;
  fotoFinal: any;
  Dataarchivo: File = null;
  foto = '';
  public respuestaImagenEnviada;
  public resultadoCarga;
  idClinica = '';

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public sucursalServicio: SucursalService,
    private enviandoImagen: SubirarchivoService,
    private snackBar: MatSnackBar,
    private httpClient: HttpClient,
    private authService: AuthService
    ) {
      this.idClinica = this.authService.getIdClinica();
    this.idSucursal = this.route.snapshot.paramMap.get('id');

    if (this.idSucursal === null){
      this.sucursal = new Sucursal({});
      this.formulariosucursal = this.formulariodediagnostico();
    }else {
      this.infosucursal(this.idSucursal).subscribe( res => {
        this.sucursal = res[0];
        this.formulariosucursal = this.formulariodediagnostico();
      });
    }

  }
  onSubmit() {
   // console.log('Form Value', this.roomForm.value);
  }

  // obtener informacion de sucursal
  infosucursal(idS){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2,'idScursal': idS};
    const URL: any = this.baseURL + 'sucursal.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers,);
  }

  //llenar el formulario con el modelo de servicio

  formulariodediagnostico(): FormGroup {
    const fecha = moment(this.sucursal.Fecha).format('MM/DD/YYYY');
    return this.fb.group({
      NombreSucursal: [this.sucursal.NombreSucursal,[Validators.required]],
      Rfc: [this.sucursal.Rfc,[Validators.required]],
      Direccion: [this.sucursal.Direccion,[Validators.required]],
      Telefono: [this.sucursal.Telefono,[Validators.required]],
      Whatsapp: [this.sucursal.Whatsapp,[Validators.required]],
      Correo: [this.sucursal.Correo, [Validators.required, Validators.email, Validators.minLength(5)]],
      Nombrecorto: [this.sucursal.Nombrecorto,[Validators.required]],
      logo: [this.sucursal.Logo,[Validators.required]],
      fecha: [new Date(fecha)],
    });
  }

  //metdo para obtener la imagen de input file

  Subirimagen(event, files: FileList, fileInput: any) {
    console.log('entro')
    const Numero1 = Math.floor(Math.random() * 10001).toString();
    const Numero2 = Math.floor(Math.random() * 1001).toString();
    // Nombre del archivo
    this.Archivoseleccionado = event.target.files[0];
    this.nombrefoto = event.target.files[0].name;
    this.foto = Numero1 + Numero2 + this.nombrefoto.replace(/ /g, "");
    this.fotoFinal = files;
    this.Dataarchivo = <File>fileInput.target.files[0];
    //this.cargandoImagen(this.fotoFinal, this.foto);
  }


  //metodo para subir la imagen y guardar el resultado en base de datos

  public cargandoImagen(files: FileList, nombre: string) {
    this.mensajesubiendoinf();
    this.enviandoImagen.postFileImagen(this.fotoFinal[0] , this.foto).subscribe(
      response => {
          this.respuestaImagenEnviada = response;
          if (this.respuestaImagenEnviada.msj === 'Imagen subida') {
            this.formulariosucursal.controls['logo'].setValue(this.foto);
            this.sucursalServicio.agregarsucursal(this.formulariosucursal.getRawValue(), Number(this.idClinica)).subscribe( respuesta => {
              const res = respuesta;
              if (res.toString() === 'Se inserto'){
                Swal.close();
                this.router.navigateByUrl('/admin/room/Listado-Sucursales');
                this.showNotification(
                  'snackbar-success',
                  'Sucursal guardada correctamente...!!!',
                  'bottom',
                  'center'
                );
              }else {
                this.showNotification(
                  'snackbar-error',
                  'Ocurrio un error, intente de nuevo por favor...!!!',
                  'bottom',
                  'center'
                );
              }
            });

          } else {
            this.showNotification(
              'snackbar-error',
              'La imagen no pudo subirse, intente de nuevo por favor...!!!',
              'bottom',
              'center'
            );
          }
      }
  
    );
  }


  //metodo para subir la imagen y editar informacion de sucursal

  public cargarimageneditar() {
    this.mensajesubiendoinf();
    this.enviandoImagen.postFileImagen(this.fotoFinal[0] , this.foto).subscribe(
      response => {
          this.respuestaImagenEnviada = response;
          if (this.respuestaImagenEnviada.msj === 'Imagen subida') {
            this.formulariosucursal.controls['logo'].setValue(this.foto);
            this.sucursalServicio.actualizarsucursal(this.formulariosucursal.getRawValue(), Number(this.idSucursal)).subscribe( respuesta => {
              const res = respuesta;
              if (res.toString() === 'Se modifico'){
                Swal.close();
                this.router.navigateByUrl('/admin/room/Listado-Sucursales');
                this.showNotification(
                  'snackbar-success',
                  'Sucursal editada correctamente...!!!',
                  'bottom',
                  'center'
                );
              }else {
                this.showNotification(
                  'snackbar-error',
                  'Ocurrio un error, intente de nuevo por favor...!!!',
                  'bottom',
                  'center'
                );
              }
            });

          } else {
            this.showNotification(
              'snackbar-error',
              'La imagen no pudo subirse, intente de nuevo por favor...!!!',
              'bottom',
              'center'
            );
          }
      }
  
    );
  }

  //metodo para editar informacion de sucursal con condicion si se cambia imagen o no 

  editarinformacionsucursal(){
    if (this.foto !== '') {
      console.log('con imagen')
      this.cargarimageneditar();
    } else {
      console.log('sin imagen')
      this.sucursalServicio.actualizarsucursal(this.formulariosucursal.getRawValue(), Number(this.idSucursal)).subscribe( respuesta => {
        const res = respuesta;
        if (res.toString() === 'Se modifico'){
          Swal.close();
          this.router.navigateByUrl('/admin/room/Listado-Sucursales');
          this.showNotification(
            'snackbar-success',
            'Sucursal editada correctamente...!!!',
            'bottom',
            'center'
          );
        }else {
          this.showNotification(
            'snackbar-error',
            'Ocurrio un error, intente de nuevo por favor...!!!',
            'bottom',
            'center'
          );
        }
      });
    }
  }

  // mandar un dialogo donde se le indique al cliente que se esta procesando su informacion 
  mensajesubiendoinf(){
    Swal.fire({
      title: 'Subiendo información',
      html: 'Espere un momento por favor',
      allowOutsideClick: false,
      onBeforeOpen: () => {
          Swal.showLoading()
      },
  });
  }

  // metodo para mandar toast 

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

}
