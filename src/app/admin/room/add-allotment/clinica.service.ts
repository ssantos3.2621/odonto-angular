import { Injectable } from '@angular/core';
import { Clinica } from './clinica.model';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import {  Router } from '@angular/router';

import Swal from 'sweetalert2';
@Injectable({
  providedIn: 'root'
})
export class ClinicaService {
  dataChange: BehaviorSubject<Clinica[]> = new BehaviorSubject<Clinica[]>([]);
  diagnosticos: any = [];
  baseURL = 'https://animatiomx.com/odonto/';
  dialogData: any;
  valor = '';
  constructor(private router: Router,
              private httpClient: HttpClient) {}
    // Agrega clinica
    agregarClinica(clinica: Clinica, idusu: string) {
      this.dialogData = clinica;
      let nom = this.dialogData.rNo + ' matriz';
      let rf = this.dialogData.rType;
      let dir = this.dialogData.pName;
      let te = this.dialogData.aDate;
      let wha = this.dialogData.dDate;
      let corr = this.dialogData.email;
      let corto = this.dialogData.namecrto;
      let logo = this.dialogData.logo;
      let idU = idusu;
      let fecha = moment(this.dialogData.fecha).format('YYYY/MM/DD');
      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { 'caso': 1,'nombreC':nom,'rfc':rf,'direc':dir, 'tele':te, 'wsp':wha, 'mail':corr, 'nombreCorto':corto, 'Logo': logo,'Fecha':fecha, 'user': idU};
      const URL: any = this.baseURL + 'clinicas.php';
      return this.httpClient.post(URL, JSON.stringify(options), headers);
    }
    // Modifica receta
    ModificarCLinica(clinica: Clinica, idClinica: number) {
      this.dialogData = clinica;
      let nom = this.dialogData.rNo;
      let rf = this.dialogData.rType;
      let dir = this.dialogData.pName;
      let te = this.dialogData.aDate;
      let wha = this.dialogData.dDate;
      let corr = this.dialogData.email;
      let corto = this.dialogData.namecrto;
      let logo = this.dialogData.logo;
      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { 'caso': 2,'nombreC':nom,'rfc':rf,'direc':dir, 'tele':te, 'wsp':wha, 'mail':corr, 'nombreCorto':corto, 'Logo': logo, 'idc': idClinica};
      const URL: any = this.baseURL + 'clinicas.php';
      return this.httpClient.post(URL, JSON.stringify(options), headers);
    }
}
