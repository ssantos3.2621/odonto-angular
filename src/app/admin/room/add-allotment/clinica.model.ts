export class Clinica {
    idClinicas: number;
    NombreClinica: string;
    RFC: string;
    Direccion: string;
    Telefono: string;
    Whatsapp: string;
    Email: string;
    NombreCorto: string;
    idUser: number;
    constructor(clinica) {
        {
          this.idClinicas = clinica.idClinicas || this.getRandomID();
          this.NombreClinica = clinica.Fecha || '';
          this.RFC = clinica.RFC || '';
          this.Direccion = clinica.Direccion || '';
          this.Telefono = clinica.Telefono || '';
          this.Whatsapp = clinica.Whatsapp || '';
          this.NombreCorto = clinica.NombreCorto || '';
          this.idUser = clinica.idUser || '';
        }
      }
      public getRandomID(): string {
        var S4 = function() {
          return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
      }
}