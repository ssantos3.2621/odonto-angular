import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClinicaService } from './clinica.service';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { Clinica } from './clinica.model';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/shared/security/auth.service';
import Swal from 'sweetalert2';
import { LocalService } from 'src/app/shared/security/local.service';
import { SubirarchivoService } from 'src/app/subirarchivo.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-allotment',
  templateUrl: './add-allotment.component.html',
  styleUrls: ['./add-allotment.component.sass']
})
export class AddAllotmentComponent {
  dataChange: BehaviorSubject<Clinica[]> = new BehaviorSubject<Clinica[]>([]);
  Archivoseleccionado: null;
  nombrefoto: any;
  fotoFinal: any;
  Dataarchivo: File = null;
  foto = '';
  public respuestaImagenEnviada;
  public resultadoCarga;
  idClinica = '';
  idUsuario = '';
  Datosclinica: any = [];
  valor = '';
  baseURL = 'https://animatiomx.com/odonto/';
  roomForm: FormGroup;
  constructor(
              private httpClient: HttpClient,
              private fb: FormBuilder,
              public servicioClinica: ClinicaService,
              private authService: AuthService,
              private localService: LocalService,
              private enviandoImagen: SubirarchivoService,
              private snackBar: MatSnackBar,
              ) {
    this.idUsuario = this.authService.getIdUser();
    this.roomForm = this.fb.group({
      rNo: ['', [Validators.required]],
      rType: ['', [Validators.required]],
      pName: ['', [Validators.required]],
      aDate: ['', [Validators.required]],
      dDate: ['', [Validators.required]],
      email: ['', [Validators.required]],
      namecrto: ['', [Validators.required]],
      logo: ['', [Validators.required]]
    });
  }
  ngOnInit(): void {
    this.verificarClinica();
  }
  onSubmit() {
    console.log('Form Value', this.roomForm.value);
  }
  public confirmAdd() {
    if (this.valor === '') {
        this.cargandoImagen();
    }
    if (this.valor === '1') {
      this.editarinformacionclinica();
    }
  }

    //metdo para obtener la imagen de input file

    Subirimagen(event, files: FileList, fileInput: any) {
      console.log('entro')
      const Numero1 = Math.floor(Math.random() * 10001).toString();
      const Numero2 = Math.floor(Math.random() * 1001).toString();
      // Nombre del archivo
      this.Archivoseleccionado = event.target.files[0];
      this.nombrefoto = event.target.files[0].name;
      this.foto = Numero1 + Numero2 + this.nombrefoto.replace(/ /g, "");
      this.fotoFinal = files;
      this.Dataarchivo = <File>fileInput.target.files[0];
      //this.cargandoImagen(this.fotoFinal, this.foto);
    }

  //metodo para subir la imagen y guardar el resultado en base de datos

   cargandoImagen() {
    this.mensajesubiendoinf();
    this.enviandoImagen.postFileImagen(this.fotoFinal[0] , this.foto).subscribe(
      response => {
          this.respuestaImagenEnviada = response;
          if (this.respuestaImagenEnviada.msj === 'Imagen subida') {
            this.roomForm.controls['logo'].setValue(this.foto);
            this.servicioClinica.agregarClinica(this.roomForm.getRawValue(),this.idUsuario).subscribe( res => {
              console.log(res);
              if (res !== null){
                Swal.close();
                this.localService.setJsonValue("IDCLINICA", res[0]);
                this.localService.setJsonValue("IDSUCURSAL", res[1]);
                this.showNotification(
                  'snackbar-success',
                  'Clinica agregada correctamente...!!!',
                  'bottom',
                  'center'
                );
                this.verificarClinica();
              }else {
                this.showNotification(
                  'snackbar-error',
                  'Ocurrio un error, intente de nuevo por favor...!!!',
                  'bottom',
                  'center'
                );
              }
            });

          } else {
            this.showNotification(
              'snackbar-error',
              'La imagen no pudo subirse, intente de nuevo por favor...!!!',
              'bottom',
              'center'
            );
          }
      }
  
    );
  }


  //metodo para editar informacion de clinica con condicion si se cambia imagen o no 

  editarinformacionclinica(){
    if (this.foto !== '') {
      console.log('con imagen')
      this.cargarimageneditar();
    } else {
      console.log('sin imagen')
      this.servicioClinica.ModificarCLinica(this.roomForm.getRawValue(), Number(this.idClinica)).subscribe( respuesta => {
        const res = respuesta;
        if (res.toString() === 'Se modifico'){
          Swal.close();
          this.showNotification(
            'snackbar-success',
            'Clinica editada correctamente...!!!',
            'bottom',
            'center'
          );
          this.verificarClinica();
        }else {
          this.showNotification(
            'snackbar-error',
            'Ocurrio un error, intente de nuevo por favor...!!!',
            'bottom',
            'center'
          );
        }
      });
    }
  }

    //metodo para subir la imagen y editar informacion de clinica

    public cargarimageneditar() {
      this.mensajesubiendoinf();
      this.enviandoImagen.postFileImagen(this.fotoFinal[0] , this.foto).subscribe(
        response => {
            this.respuestaImagenEnviada = response;
            if (this.respuestaImagenEnviada.msj === 'Imagen subida') {
              this.roomForm.controls['logo'].setValue(this.foto);
              this.servicioClinica.ModificarCLinica(this.roomForm.getRawValue(), Number(this.idClinica)).subscribe( respuesta => {
                const res = respuesta;
                if (res.toString() === 'Se modifico'){
                  Swal.close();
                  this.showNotification(
                    'snackbar-success',
                    'Clinica editada correctamente...!!!',
                    'bottom',
                    'center'
                  );
                  this.verificarClinica();
                }else {
                  this.showNotification(
                    'snackbar-error',
                    'Ocurrio un error, intente de nuevo por favor...!!!',
                    'bottom',
                    'center'
                  );
                }
              });
  
            } else {
              this.showNotification(
                'snackbar-error',
                'La imagen no pudo subirse, intente de nuevo por favor...!!!',
                'bottom',
                'center'
              );
            }
        }
    
      );
    }

  // metodo para mandar toast 

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  // mandar un dialogo donde se le indique al cliente que se esta procesando su informacion 
  mensajesubiendoinf(){
    Swal.fire({
      title: 'Subiendo información',
      html: 'Espere un momento por favor',
      allowOutsideClick: false,
      onBeforeOpen: () => {
          Swal.showLoading()
      },
  });
  }

  // Consulta si ya tiene clinica
  verificarClinica() {
    // obtener id de la clinica 
    const idclinica = this.authService.getIdClinica();
    if (idclinica === null) {
      this.valor = ''
    } else {
      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { caso: 0, idc: idclinica };
      const URL: any = this.baseURL + 'clinicas.php';
      this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
        respuesta => {
          this.Datosclinica = respuesta;
          if (this.Datosclinica !== null){
            this.dataChange.next(this.Datosclinica);
            console.log(respuesta);
            console.log(this.dataChange);
          }
          console.log(this.Datosclinica);
          for (let i = 0; i < this.Datosclinica.length; i++) {
            const element = this.Datosclinica[0].Direccion;
            this.roomForm.setValue({
              rNo: this.Datosclinica[0].NombreClinica,
              rType: this.Datosclinica[0].RFC,
              pName: this.Datosclinica[0].Direccion,
              aDate: this.Datosclinica[0].Telefono,
              dDate: this.Datosclinica[0].Whatsapp,
              email: this.Datosclinica[0].Email,
              namecrto: this.Datosclinica[0].NombreCorto,
              logo: this.Datosclinica[0].Logo
            });
            this.idClinica = this.Datosclinica[0].idClinicas;
            }
          this.valor = '1';
        },
        (error: HttpErrorResponse) => {
          console.log(error.name + ' ' + error.message);
        }
        );
    }
  }
}
