import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Presup } from './Presup.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PresupService {
private readonly API_URL = 'assets/data/payment.json';
  dataChange: BehaviorSubject<Presup[]> = new BehaviorSubject<Presup[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private httpClient: HttpClient) {}
  get data(): Presup[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllPayments(): void {
    this.httpClient.get<Presup[]>(this.API_URL).subscribe(
      data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
    );
  }
  // DEMO ONLY, you can find working methods below
  addPayment(payment: Presup): void {
    this.dialogData = payment;
  }
  updatePayment(payment: Presup): void {
    this.dialogData = payment;
  }
  deletePayment(id: number): void {
    console.log(id);
  }
}
