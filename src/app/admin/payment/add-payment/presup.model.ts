export class Presup {
    id: number;
  pName: string;
  dName: string;
  charges: string;
  date: string;
  date2: string;
  total: string;
  constructor(payment) {
    {
      this.id = payment.id || this.getRandomID();
      this.pName = payment.pName || '';
      this.dName = payment.dName || '';
      this.charges = payment.charges || '';
      this.date = payment.date || '';
      this.date2 = payment.discount || '';
      this.total = payment.total || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
