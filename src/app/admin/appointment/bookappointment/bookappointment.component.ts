import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from 'sweetalert2';
@Component({
  selector: "app-bookappointment",
  templateUrl: "./bookappointment.component.html",
  styleUrls: ["./bookappointment.component.sass"],
})
export class BookappointmentComponent {
  private baseURL = 'https://animatiomx.com/odonto/';
  NuevaCitaForm: FormGroup;
  hide3 = true;
  agree3 = false;
  pacientes: any = [];
  doctores: any = [];
  servicios: any = [];
  constructor(private fb: FormBuilder,public httpClient: HttpClient,) {
    //declaracion de las variables del formulario
    this.NuevaCitaForm = this.fb.group({
      paciente: ["", [Validators.required]],
      doctor: ["",[Validators.required]],
      fecha: ["", [Validators.required]],
      horario: ["", [Validators.required]],
      motivo: ["",[Validators.required]],
      servicio: ["",[Validators.required]],
      observaciones: [""],
    });
    this.GetPacientes();
    this.GetDoctores();
    this.GetServicios();
  }
  //guarda el contenido del formulario ne la BD
  onSubmit() {
    console.log("Form Value", this.NuevaCitaForm.value);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'nuevacita':this.NuevaCitaForm.value};
    const URL: any = this.baseURL + 'citas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       console.log(respuesta);
      });
  }
//obtiene el listado de pacientes
  GetPacientes(){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0};
    const URL: any = this.baseURL + 'citas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.pacientes = respuesta;
      });
  }
//obtiene el listado de doctores
  GetDoctores(){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1};
    const URL: any = this.baseURL + 'citas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.doctores = respuesta;
      });
  }
//obtiene el listado de servicios
  GetServicios(){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2};
    const URL: any = this.baseURL + 'citas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.servicios = respuesta;
      });
  }
//valida si el doctor esta disponible en esa fecha y horario
  ValidarHorario(event){
    console.log(this.NuevaCitaForm.value.horario);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 4,'nuevacita':this.NuevaCitaForm.value};
    const URL: any = this.baseURL + 'citas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        if (respuesta!=null) {
          Swal.fire("El doctor no esta disponible en ese horario", "", "error");
          this.NuevaCitaForm.reset();
        }
        console.log('disponible');
      });
  }

}
