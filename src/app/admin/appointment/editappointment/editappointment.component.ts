import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataSource } from '@angular/cdk/collections';
import { CitasService } from './citas.service';
import { Citas } from './citas.model';
import { EliminarcitaComponent } from './abccitas/eliminarcita/eliminarcita.component';
import { NuevoeditarcitaComponent } from './abccitas/nuevoeditarcita/nuevoeditarcita.component';

@Component({
  selector: 'app-editappointment',
  templateUrl: './editappointment.component.html',
  styleUrls: ['./editappointment.component.sass']
})
export class EditappointmentComponent implements OnInit{
 private baseURL = 'https://animatiomx.com/odonto/';
  ColumnasdeDiagnostico = [
    'fechayhora',
    'clasif',
    'observa',
    'nombre',
    'actions',
  ];
diagnostico : boolean = false;
receta : boolean = false;
evolucion : boolean = false;
Ptratamiento : boolean = false;
CitasBasededatos: CitasService | null;
datosDiagnostico: Diagnosticodatos | null;
selection = new SelectionModel<Citas>(true, []);
index: number;
id: number;
idDiagnostico: number;
//asignar nombre a paginador
@ViewChild(MatPaginator, { static: true }) paginadordiagnostico: MatPaginator;
@ViewChild(MatSort, { static: true }) ordenardiagnostico: MatSort;
@ViewChild('filtrodiagnostico', { static: true }) filtrodiagnostico: ElementRef;
  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public CitasService: CitasService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.cargardatosdiagnostico();
  }
  //recargar datos de la tabla de diagnostico
  refrescartabladiagnostico() {
    this.cargardatosdiagnostico();
  }
  //refrescar la tabla para poner de nuevo los paginadores
  private refrescartabladediagnostico() {
    this.paginadordiagnostico._changePageSize(this.paginadordiagnostico.pageSize);
  }
  public cargardatosdiagnostico() {
    //obtenemos los datos de la tabla de diagnostico y lo guardamos en su variable
    this.CitasBasededatos = new CitasService(this.httpClient);
    // se manda a llamar la clase de diagnostico y se manda los valores de base de datos el paginador
    this.datosDiagnostico = new Diagnosticodatos(
      this.CitasBasededatos,
      this.paginadordiagnostico,
      this.ordenardiagnostico
    );
    // metodo para verificar el evento de filtro
    fromEvent(this.filtrodiagnostico.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.datosDiagnostico) {
          return;
        }
        //obtenemos los valores escritos en el input
        this.datosDiagnostico.letrafiltro = this.filtrodiagnostico.nativeElement.value;
      });
  }
  nuevodiagnostico() {
    const dialogRef = this.dialog.open(NuevoeditarcitaComponent, {
      data: {
        patient: this.diagnostico,
        action: 'nuevo',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.CitasBasededatos.dataChange.value.unshift(
          //this.CitasBasededatos.getDialogData()
        );
        this.refrescartabladiagnostico();
        this.showNotification(
          'snackbar-success',
          'Diagnostico guardado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  editardiagnostico(row) {
    this.idDiagnostico = row.idDiagnostico;
    console.log(this.idDiagnostico);
    const dialogRef = this.dialog.open(NuevoeditarcitaComponent, {
      data: {
        diagnostico: row,
        action: 'editar',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.CitasBasededatos.dataChange.value.findIndex(
          (x) => x.idDiagnostico === this.id
        );
        this.refrescartabladiagnostico();
        this.refrescartabladediagnostico();
        this.showNotification(
          'black',
          'Diagnostico editado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  eliminardiagnostico(i: number, row) {
    this.index = i;
    this.idDiagnostico = row.idDiagnostico;
    console.log(this.idDiagnostico);
    const dialogRef = this.dialog.open(EliminarcitaComponent, {
      data: row,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.CitasBasededatos.dataChange.value.findIndex(
          (x) => x.idDiagnostico === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this.CitasBasededatos.dataChange.value.splice(foundIndex, 1);
        this.refrescartabladiagnostico();
        this.refrescartabladediagnostico();
        this.showNotification(
          'snackbar-danger',
          'Diagnostico eliminado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

}

export class Diagnosticodatos extends DataSource<Citas> {
  //buscadorfiltro es metodo y letrafiltro es los valores del input
  buscadorfiltro = new BehaviorSubject('');
  //obtenemos las letras
  get letrafiltro(): string {
    return this.buscadorfiltro.value;
  }
  //mandamos las letras del filtro
  set letrafiltro(letrafiltro: string) {
    this.buscadorfiltro.next(letrafiltro);
  }
  filtrodatos: Citas[] = [];
  renderizardatos: Citas[] = [];
  constructor(
    public Diagnosticodatos: CitasService,
    public paginadorDiagnostico: MatPaginator,
    public ordenar: MatSort
  ) {
    super();
    // Restablecer a la primera pagina cuando se haga un filtro
    this.buscadorfiltro.subscribe(() => (this.paginadorDiagnostico.pageIndex = 0));
  }
  /** Función de conexión llamada por la tabla para recuperar una secuencia que contiene los datos para representar. */
  connect(): Observable<Citas[]> {
    // escuchar de evento
    const displayDataChanges = [
      this.Diagnosticodatos.dataChange,
      this.ordenar.sortChange,
      this.buscadorfiltro,
      this.paginadorDiagnostico.page,
    ];
    this.Diagnosticodatos.obtenerdiagnostico();
    return merge(...displayDataChanges).pipe(
      map(() => {
        // metodo para filtrar datos
        this.filtrodatos = this.Diagnosticodatos.data
          .slice()
          .filter((diagnostico: Citas) => {
            const searchStr = (
              diagnostico.nombre +
              diagnostico.Observaciones +
              diagnostico.Nombre +
              diagnostico.Apellido
            ).toLowerCase();
            return searchStr.indexOf(this.letrafiltro.toLowerCase()) !== -1;
          });
        // ordenar datos filtrados
        const ordenardatos = this.metodoordenardatos(this.filtrodatos.slice());
        // metodo para order los datos y hacer las paginas
        const empezardelprincipio = this.paginadorDiagnostico.pageIndex * this.paginadorDiagnostico.pageSize;
        this.renderizardatos = ordenardatos.splice(
          empezardelprincipio,
          this.paginadorDiagnostico.pageSize
        );
        return this.renderizardatos;
      })
    );
  }
  disconnect() {}
  /** Devuelve una copia ordenada de los datos de la base de datos. */
  metodoordenardatos(data: Citas[]): Citas[] {
    //copia todos los datos de diagnostico y los pone en una tabla para las propiedades
    if (!this.ordenar.active || this.ordenar.direction === '') {
      return data;
    }
    // metodo para poder poner la tabla en ascendente y descendente
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this.ordenar.active) {
        case 'id':
          [propertyA, propertyB] = [a.idDiagnostico, b.idDiagnostico];
          break;
        case 'fechayhora':
          [propertyA, propertyB] = [a.Fecha, b.Fecha];
          break;
        case 'clasif':
          [propertyA, propertyB] = [a.nombre, b.nombre];
          break;
        case 'observa':
          [propertyA, propertyB] = [a.Observaciones, b.Observaciones];
          break;
        case 'nombre':
          [propertyA, propertyB] = [a.Nombre, b.Nombre];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this.ordenar.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
