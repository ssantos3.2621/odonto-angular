export class Citas {
  idDiagnostico: number;
  Fecha: string;
  pertenece: string;
  nombre: string;
  Observaciones: string;
  idPacientes: number;
  idClasificacion: number;
  Nombre: string;
  Apellido: string;
  fechahoy = new Date();

  constructor(diagnostico) {
    {
      this.idDiagnostico = diagnostico.idDiagnostico || this.getRandomID();
      this.Fecha = diagnostico.Fecha || this.fechahoy;
      this.pertenece = diagnostico.pertenece || '';
      this.nombre = diagnostico.nombre || '';
      this.Observaciones = diagnostico.Observaciones || '';
      this.idPacientes = diagnostico.idPacientes || '';
      this.idClasificacion = diagnostico.idClasificacion || '';
      this.Nombre = diagnostico.Nombre || '';
      this.Apellido = diagnostico.Apellido || '';
    }
  }
  public getRandomID(): string {
    var S4 = function() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
