import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { CitasService } from '../../citas.service';

@Component({
  selector: 'app-eliminarcita',
  templateUrl: './eliminarcita.component.html',
  styles: [
  ]
})
export class EliminarcitaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EliminarcitaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public diagnosticoServicio: CitasService
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    this.diagnosticoServicio.eliminardiagnostico(this.data.idDiagnostico);
  }

  ngOnInit(): void {
  }

}
