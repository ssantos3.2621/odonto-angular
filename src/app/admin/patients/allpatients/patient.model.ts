export class Patient {
  idPacientes: number;
  Nombre: string;
  Apellido: string;
  Direccion: string;
  Celular: string;
  Correo: string;
  FechaNac: string;
  Edad: string;
  GrupoSanguineo: string;
  Genero: string;
  EstadoCivil: string;
  Ocupacion: string;
  Observaciones: string;
  NombreFami: string;
  Parentesco: string;
  idDoctor: string;
  Imagen: string;
  idDoctores: string;
  idHistoriales: string;
  Tratamiento: string;
  constructor(patient) {
    {
      this.idPacientes = patient.id || this.getRandomID();
      this.Imagen = patient.Imagen || 'assets/images/user/user1.jpg';
      this.Nombre = patient.Nombre || '';
      this.Apellido = patient.Apellido || '';
      this.Genero = patient.Genero || 'Femenino';
      this.GrupoSanguineo = patient.Correo || '';
      this.FechaNac = patient.FechaNac || '';
      this.Celular = patient.Celular || '';
      this.Tratamiento = patient.Tratamiento || '';
      this.Direccion = patient.Direccion || '';
    }
  }
  public getRandomID(): string {
    var S4 = function() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
