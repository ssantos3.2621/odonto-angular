import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { PatientService } from '../../patient.service';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { Patient } from '../../patient.model';
import * as moment from 'moment';

@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styleUrls: ['./form-dialog.component.sass'],
})
export class FormDialogComponent {
  action: string;
  dialogTitle: string;
  patientForm: FormGroup;
  patient: Patient;
  constructor(
    public dialogRef: MatDialogRef<FormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public patientService: PatientService,
    private fb: FormBuilder
  ) {
    // Set the defaults
    this.action = data.action;
    if (this.action === 'edit') {
      console.log(data)
      this.dialogTitle = data.patient.Nombre + ' ' +data.patient.Apellido;
      this.patient = data.patient;
    } else {
      this.dialogTitle = 'Nuevo Paciente';
      this.patient = new Patient({});
    }
    this.patientForm = this.createContactForm();
  }
  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }
  createContactForm(): FormGroup {
    const fecha = moment(this.patient.FechaNac).format('MM/DD/YYYY');
    return this.fb.group({
      id: [this.patient.idPacientes],
      img: [this.patient.Imagen],
      name: [this.patient.Nombre],
      ape: [this.patient.Apellido],
      gender: [this.patient.Genero],
      date: [ new Date(fecha)],
      bGroup: [this.patient.GrupoSanguineo],
      mobile: [this.patient.Celular],
      treatment: [this.patient.Tratamiento],
      email: [this.patient.Correo],
      dir: [this.patient.Direccion],
      uploadImg:[]
    });
  }
  submit() {
    // emppty stuff
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  public confirmAdd(): void {
    if (this.action === 'edit') {
      this.patientService.updatePatient(this.patientForm.getRawValue());
    } else {
      this.patientService.addPatient(this.patientForm.getRawValue());
    }
  }
}
