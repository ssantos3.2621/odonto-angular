import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Patient } from './patient.model';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { SubirarchivoService } from 'src/app/subirarchivo.service';
import * as moment from 'moment';
@Injectable()
export class PatientService {
  private readonly API_URL = 'https://animatiomx.com/odonto/prueba.php';
  dataChange: BehaviorSubject<Patient[]> = new BehaviorSubject<Patient[]>([]);
  // Temporarily stores data from dialogs
  baseURL = 'https://animatiomx.com/odonto/';
  dialogData: any;
  clientes: any = [];
  selectedFile: null;
  nombrefoto: any;
  foto = '';
  public respuestaImagenEnviada;
  public resultadoCarga;
  constructor(
    private httpClient: HttpClient,
    private enviandoImagen: SubirarchivoService
    ) {}
  get data(): Patient[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllPatients(idClinica : number) {
    // this.httpClient.get<Patient[]>(this.API_URL).subscribe(
    //   data => {
    //     this.dataChange.next(data);
    //     console.log(data);
    //     console.log(this.dataChange);
    //   },
    //   (error: HttpErrorResponse) => {
    //     console.log(error.name + ' ' + error.message);
    //   }
    // );
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 0, idClinica:idClinica};
    const URL: any = this.baseURL + 'pacientes.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.clientes = respuesta;
        this.dataChange.next(this.clientes);
        console.log(respuesta);
        console.log(this.dataChange);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }

  // DEMO ONLY, you can find working methods below
  addPatient(patient: Patient): void {
    this.dialogData = patient;
    console.log(this.dialogData);
    let nom = this.dialogData.name;
    let ape = this.dialogData.ape;
    let dir = this.dialogData.dir;
    let tel = this.dialogData.mobile;
    let genero = this.dialogData.gender;
    let fecha = moment(this.dialogData.date).format('YYYY-MM-DD');
    let sangre = this.dialogData.bGroup;
    let correo = this.dialogData.email;
    let ima = this.dialogData.uploadImg;
    if (ima === null || ima === '' || ima === undefined || ima === 'null') {
      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { 'caso': 1,'nom':nom,'apel':ape,'dir':dir,'cel':tel,'gen':genero,'fen':fecha,'sangre':sangre,'corr':correo,'img':''};
      const URL: any = this.baseURL + 'pacientes.php';
      this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
        respuesta => {
        });
    } else {
      const Numero1 = Math.floor(Math.random() * 10001).toString();
      const Numero2 = Math.floor(Math.random() * 1001).toString();
      // Nombre del archivo
      this.selectedFile = ima._files[0];
      this.nombrefoto = ima._files[0].name;
      console.log(this.selectedFile)
      console.log(this.nombrefoto)
      this.foto = Numero1 + Numero2 + this.nombrefoto.replace(/ /g, "");
      console.log(this.foto);
      this.enviandoImagen.postFileImagen(this.selectedFile, this.foto).subscribe(
        response => {
          this.respuestaImagenEnviada = response;
          if(this.respuestaImagenEnviada <= 1) {
          console.log("Error en el servidor");
          } else {
            if (this.respuestaImagenEnviada.code == 200 && this.respuestaImagenEnviada.status == "success"){
            this.resultadoCarga = 1;
              const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
              const options: any = { 'caso': 1,'nom':nom,'apel':ape,'dir':dir,'cel':tel,'gen':genero,'fen':fecha,'sangre':sangre,'corr':correo,'img':this.foto,'edad':0};
              const URL: any = this.baseURL + 'pacientes.php';
              this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
                respuesta => {
              });
            } else {
              this.resultadoCarga = 2;
            }
          }
        },
        error => {
        console.log(<any>error);
      }

    );
    }

  }
  updatePatient(patient: Patient): void {
    this.dialogData = patient;
    let nom = this.dialogData.name;
    let ape = this.dialogData.ape;
    let sangre = this.dialogData.bGroup;
    let fecha = this.dialogData.date;
    let dir = this.dialogData.dir;
    let correo = this.dialogData.email;
    let genero = this.dialogData.gender;
    let ima = this.dialogData.img;
    let tel = this.dialogData.mobile;
    let trata = this.dialogData.treatment;
    let id = this.dialogData.id;
    let imagen = this.dialogData.uploadImg;
    if (imagen === null || imagen === '' || imagen === undefined || imagen === 'null') {
      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { 'caso': 2,'nom':nom,'apel':ape,'dir':dir,'cel':tel,'corr':correo,'fen':fecha,'sangre':sangre,'gen':genero,'idPaciente':id,'img':ima};
      const URL: any = this.baseURL + 'pacientes.php';
      this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
        respuesta => {
        });
    } else {
      const Numero1 = Math.floor(Math.random() * 10001).toString();
      const Numero2 = Math.floor(Math.random() * 1001).toString();
      // Nombre del archivo
      this.selectedFile = imagen._files[0];
      this.nombrefoto = imagen._files[0].name;
      console.log(this.selectedFile)
      console.log(this.nombrefoto)
      this.foto = Numero1 + Numero2 + this.nombrefoto.replace(/ /g, "");
      console.log(this.foto);
      this.enviandoImagen.postFileImagen(this.selectedFile, this.foto).subscribe(
        response => {
          this.respuestaImagenEnviada = response;
          if(this.respuestaImagenEnviada <= 1) {
          console.log("Error en el servidor");
          } else {
            if (this.respuestaImagenEnviada.code == 200 && this.respuestaImagenEnviada.status == "success"){
            this.resultadoCarga = 1;
            const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
            const options: any = { 'caso': 2,'nom':nom,'apel':ape,'dir':dir,'cel':tel,'corr':correo,'fen':fecha,'sangre':sangre,'gen':genero,'idPaciente':id,'img':this.foto};
            const URL: any = this.baseURL + 'pacientes.php';
            this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
              respuesta => {
              });
            } else {
              this.resultadoCarga = 2;
            }
          }
        },
        error => {
        console.log(<any>error);
      }

    );
    }
  }
  deletePatient(id: number): void {
    console.log(id);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'idPaciente':id  };
    const URL: any = this.baseURL + 'pacientes.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
}
