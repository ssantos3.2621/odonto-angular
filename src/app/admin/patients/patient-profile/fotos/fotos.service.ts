import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { FotosS } from './fotos.model';

@Injectable({
  providedIn: 'root'
})
export class FotoService {
    //intercambio de datos
    dataChange: BehaviorSubject<FotosS[]> = new BehaviorSubject<FotosS[]>([]);
    baseURL = 'https://animatiomx.com/odonto/';
    dialogData: any;
    fotos: any = [];
    idAborrar = [];
    constructor(private httpClient: HttpClient) {}
    // poner los valores de la consulta en el dataChange
    get data(): FotosS[] {
      return this.dataChange.value;
    }
    getDialogData() {
      return this.dialogData;
    }
    /** CRUD METHODS */
  // Obtiene las recetas del paciente
  obtenerFotos(idP): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0, 'idPaciente': idP };
    const URL: any = this.baseURL + 'fotos.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.fotos = respuesta;
        if (this.fotos !== null){
          this.dataChange.next(this.fotos);
          console.log(respuesta);
          console.log(this.dataChange);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }
  //
  // Elimina fotos
  eliminarFoto(id): void {
    console.log(id);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1,'idFo':id  };
    const URL: any = this.baseURL + 'fotos.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
  }
