export class FotosS {
    idFotos: number;
    Ruta: string;
    Descripcion: string;
    idPacientes: number;
    Fecha: string;
    constructor(fotos) {
        {
          this.idFotos = fotos.idFotos || this.getRandomID();
          this.Ruta = fotos.Ruta || '';
          this.Descripcion = fotos.Descripcion || '';
          this.idPacientes = fotos.Complicaciones || '';
        }
      }
      public getRandomID(): string {
        var S4 = function() {
          return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
      }
}