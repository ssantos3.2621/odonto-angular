import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-abcpresupuestos',
  templateUrl: './abcpresupuestos.component.html',
  styles: [
  ]
})
export class AbcpresupuestosComponent implements OnInit {
patientForm: FormGroup;
  constructor(private fb: FormBuilder,) {

      this.patientForm = this.fb.group({
      first: ['', [Validators.required, Validators.pattern('[a-zA-Z]+')]],
      last: [''],
      gender: ['', [Validators.required]],
      mobile: [''],
      dob: ['', [Validators.required]],
      Edad: ['', [Validators.required]],
      age: [''],
      email: [
        '',
        [Validators.required, Validators.email, Validators.minLength(5)],
      ],
      maritalStatus: [''],
      address: [''],
      bGroup: [''],
      bPresure: [''],
      sugger: [''],
      injury: [''],
      uploadImg: [''],
      rfc: [''],
      convenio: [''],
      notas: [''],
      telefono: [''],
      responsable: [''],
    });
  }

  onSubmit() {
    console.log('Form Value', this.patientForm.value);
  }

  ngOnInit(): void {
  }

}
