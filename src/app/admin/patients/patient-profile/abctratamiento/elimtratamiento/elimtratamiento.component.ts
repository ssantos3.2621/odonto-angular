import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { TratamientoService } from '../../tratamiento/tratamiento.service';

@Component({
  selector: 'app-elimtratamiento',
  templateUrl: './elimtratamiento.component.html',
  styles: [
  ]
})
export class ElimtratamientoComponent {

  servicios: any = [];

  constructor(
    public dialogRef: MatDialogRef<ElimtratamientoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public tratamientoServicio: TratamientoService
  ) { 
    console.log(this.data);
    this.servicios = data.stringplanes.split("\n");
    console.log(this.servicios);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    this.tratamientoServicio.eliminartratamiento(this.data.idPlanTratamiento);
  }

}
