import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import {
  FormControl,
} from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TratamientoService } from '../../tratamiento/tratamiento.service';
import { MatSnackBar } from '@angular/material/snack-bar';

interface Bank {
  id: string;
  name: string;
 }

@Component({
  selector: 'app-nuevoedittratamiento',
  templateUrl: './nuevoedittratamiento.component.html',
  styles: [
  ]
})
export class NuevoedittratamientoComponent {
  private baseURL = 'https://animatiomx.com/odonto/';
  public bankFilterCtrl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  accion: string;
  idPaciente = '';
  textodedialogo: string;
  fechahoy = new Date();
  observaciones = '';
  servicios: any = [];
  servicios1: any = [];
  precioventa = '';
  idservicio = '';
  cantidad= '';
  valor= '';
  Descuento= '';
  subtotal = '';
  total = '';
  arrayservicio: any = [];
  nombreservicio = '';
  sumatotal = '';
  totalconinpuesto = '';
  totalapagar = '';
  veropciondeagregar = false;
  indexelementotabla = '';
  idplantratamiento = '';
  arraytratamientoeliminar: any = [];
  constructor(
    public dialogRef: MatDialogRef<NuevoedittratamientoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public httpClient: HttpClient,
    public tratamientoServicio: TratamientoService,
    private snackBar: MatSnackBar,
  ) {
    // obtenemos la accion que va hacer
    this.accion = data.action;
    this.idPaciente = data.idPaciente;
    if (this.accion === 'editar') {
      // texto del encabezado del modal
      this.textodedialogo = 'Editar plan de tratamiento';
      // le pasamos los valores de diagnostico
      this.fechahoy = data.tratamiento.Fecha;
      this.observaciones = data.tratamiento.Observaciones;
      this.sumatotal = data.tratamiento.Subtotalfinal;
      this.totalconinpuesto = ((Number(this.sumatotal) * 16) / 100).toFixed(2);
      this.totalapagar = data.tratamiento.TotalFinal;
      this.idplantratamiento = data.tratamiento.idPlanTratamiento;
      this.serviciosplandetratamiento(this.idplantratamiento);
      // this.diagnostico = data.diagnostico;
    } else {
      this.textodedialogo = 'Nuevo plan de tratamiento';
    }
    this.datosservicios();
   }

  // metodo para obtener los datos de servicios
  datosservicios() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 0};
    const URL: any = this.baseURL + 'tratamiento.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.servicios = respuesta;
        this.servicios1 = respuesta;
        console.log(this.servicios);
        // costruir el filtro pasandole ondestroy
        this.bankFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filtrobuscar();
        });
      });
  }

  // metodo para pasar los valores de servios a un nuevo arreglo
  initializeItems(){
    this.servicios1 = this.servicios;
  }
  // metodo de buscador
  private filtrobuscar() {
    this.initializeItems();
    const search = this.bankFilterCtrl.value;
    this.servicios1 = this.servicios.filter((item) => {
        return (item.Nombre.toLowerCase().indexOf(search.toLowerCase()) > -1);
      });
  }

  // obtener los servicios del plan de tratamiento en metodo modificar 

  serviciosplandetratamiento(idp) {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 3, idplantratamiento: idp};
    const URL: any = this.baseURL + 'tratamiento.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.arrayservicio = respuesta;
      });
  }

  // obtener la informacion del servicio seleccionado
  valores(servicio){
    console.log(servicio)
    this.valor = servicio.PrecioVenta;
    this.idservicio = servicio.idServicios;
    this.nombreservicio = servicio.Nombre;
    this.operaciones();
  }

  // metodo para realizar las operaciones de tratamiento
  operaciones(){
    this.subtotal = (Number(this.cantidad) * Number(this.valor)).toFixed(2);
    const desc =  ( (Number(this.subtotal) * Number(this.Descuento))/100).toFixed(2);
    console.log(desc)
    this.total = (Number(this.subtotal) - (Number(desc))).toFixed(2);
  }

  // llenar el arreglo con la informacion agregada en los campos
  anadirservicio(){
    const resultado = this.arrayservicio.find( fruta => fruta.idServicios === this.idservicio );
    console.log(resultado);
    if (resultado === '' || resultado === undefined || resultado === 'undefined'){
      if (Number(this.cantidad) > 0){
        if(this.Descuento === ''){
          this.Descuento = '0';
        }
        this.arrayservicio.push({'idServicios':this.idservicio,'Nombre':this.nombreservicio,'Cantidad':this.cantidad,'valor':this.valor,
        'Descuento':this.Descuento,'Subtotal':this.subtotal,'Total':this.total});
        this.sumatotaldeservicios();
        this.limpiarvariables();
      }
    }else{
      console.log(resultado);
      this.mensaje(
        'snackbar-info',
        'El servicio ya se agrego anteriormente...!!!',
        'bottom',
        'center'
      );
    }
  }

  // sumatotal

  sumatotaldeservicios(){
    let suma=0;
    for (let index = 0; index < this.arrayservicio.length; index++) {
      suma+=Number(this.arrayservicio[index].Total);
    }
    this.sumatotal = (suma).toFixed(2);
    this.totalconinpuesto = ((Number(this.sumatotal) * 16)/100).toFixed(2);
    this.totalapagar = (Number(this.sumatotal) + Number(this.totalconinpuesto)).toFixed(2);
    console.log(suma);
  }

  // metodo para limpiar varibles
  limpiarvariables(){
    this.idservicio = '';
    this.nombreservicio = '';
    this.cantidad = '';
    this.valor = '';
    this.Descuento = '';
    this.subtotal = '';
    this.total = '';
    this.indexelementotabla = '';
  }

  // modificar informacion del arreglo

  modificararregloservicio(arrayServ,i){
    console.log(arrayServ);
    this.veropciondeagregar = true;
    this.idservicio = arrayServ.idServicios;
    this.cantidad = arrayServ.Cantidad;
    this.valor = arrayServ.valor;
    this.Descuento = arrayServ.Descuento;
    this.subtotal = arrayServ.Subtotal;
    this.total = arrayServ.Total;
    this.nombreservicio = arrayServ.Nombre;
    this.indexelementotabla = i;
  }

  // regresar vista de añadir
  cambiarcondicion(){
    this.veropciondeagregar = false;
    this.limpiarvariables();
  }

  // modificar elemento de la tabla

  modificarelementotabla(){
    console.log(this.cantidad+'posiciondeelemento'+this.indexelementotabla);
    this.arrayservicio[this.indexelementotabla].idServicios = this.idservicio;
    this.arrayservicio[this.indexelementotabla].Nombre = this.nombreservicio;
    this.arrayservicio[this.indexelementotabla].Cantidad = this.cantidad;
    this.arrayservicio[this.indexelementotabla].valor = this.valor;
    this.arrayservicio[this.indexelementotabla].Descuento = this.Descuento;
    this.arrayservicio[this.indexelementotabla].Subtotal = this.subtotal;
    this.arrayservicio[this.indexelementotabla].Total = this.total;
    this.sumatotaldeservicios();
    this.limpiarvariables();
    this.veropciondeagregar = false;
    this.limpiarvariables();
  }

  // eliminar informacion del arreglo

  eliminararregloservicio(arrayServ,i){
    this.arrayservicio.splice(i, 1);
    this.sumatotaldeservicios();
    console.log(this.arrayservicio);
    if (this.accion === 'editar'){
      if ( arrayServ.idServicioPT === '' || arrayServ.idServicioPT === 'undefined' || arrayServ.idServicioPT === undefined){
        console.log('servicio nuevo');
      }else{
        this.arraytratamientoeliminar.push(arrayServ.idServicioPT);
        console.log(this.arraytratamientoeliminar);
      }
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // metodo para mostrar mensajes de alerta 

  mensaje(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 3000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  public confirmAdd(): void {
    let textop = [];
    let textos = [];
    for (let index = 0; index < this.arrayservicio.length; index++) {
            textop.push(this.arrayservicio[index].Nombre);
            textos.push(this.arrayservicio[index].Subtotal);
    }
    let textopla = textop.join('\n');
    let textosubt = textos.join('\n');
    if (this.accion === 'editar') {
      console.log(this.arrayservicio);
      this.tratamientoServicio.actualizartratamiento(this.observaciones, this.sumatotal, this.totalconinpuesto,
        this.totalapagar, this.arrayservicio, textopla, textosubt,Number(this.idplantratamiento),this.arraytratamientoeliminar);
    } else {
      // mandamos los datos a insertar a la base de datos
    this.tratamientoServicio.agregartratamiento(this.fechahoy, this.observaciones, this.sumatotal, this.totalconinpuesto,
      this.totalapagar, this.arrayservicio, textopla, textosubt, Number(this.idPaciente));
    }
  }

}
