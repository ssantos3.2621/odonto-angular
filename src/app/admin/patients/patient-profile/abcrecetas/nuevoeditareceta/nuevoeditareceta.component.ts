import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Recetas } from '../../recetas/recetas.model';
import { RecetasService } from '../../recetas/recetas.service';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-nuevoeditareceta',
  templateUrl: './nuevoeditareceta.component.html',
  styles: [
  ]
})
export class NuevoeditarecetaComponent {
  private baseURL = 'https://animatiomx.com/odonto/';
  accion: string;
  textodedialogo: string;
  formulariodiagnostico: FormGroup;
  receta: Recetas;
  clasificaciones: any = [];
  idPaciente = '';
  constructor(
    public dialogRef: MatDialogRef<NuevoeditarecetaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public recetaServicio: RecetasService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
  ) {
        // obtenemos la accion que va hacer
        this.accion = data.action;
        this.idPaciente = data.idPaciente;
        if (this.accion === 'editar') {
          //texto del encabezado del modal
          this.textodedialogo = data.receta.Nombre + ' ' +data.receta.Apellido;
          //le pasamos los valores de diagnostico
          this.receta = data.receta;
        } else {
          this.textodedialogo = 'Nuevo diagnostico';
          this.receta = new Recetas({});
        }
        this.formulariodiagnostico = this.formulariodediagnostico();
        this.clasificacionestodos();
  }

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }

  formulariodediagnostico(): FormGroup {
    const fecha = moment(this.receta.Fecha).format('MM/DD/YYYY');
    return this.fb.group({
      id: [this.receta.idRecetas],
      fecha: [new Date(fecha)],
      idpaciente: [this.receta.idPacientes],
      descripcion: [this.receta.Descripcion],
    });
  }
  clasificacionestodos() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 1};
    const URL: any = this.baseURL + 'diagnostico.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.clasificaciones = respuesta;
        console.log(this.clasificaciones);
      });
  }
  submit() {
    // emppty stuff
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  public confirmAdd(): void {
    if (this.accion === 'editar') {
      this.recetaServicio.ModificarReceta(this.formulariodiagnostico.getRawValue());
    } else {
      this.recetaServicio.agregarReceta(this.formulariodiagnostico.getRawValue(), Number(this.idPaciente));
    }
  }
}
