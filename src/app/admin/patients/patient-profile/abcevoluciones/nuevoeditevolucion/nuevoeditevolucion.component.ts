import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EvolucionesService } from '../../evoluciones/evoluciones.service';
import { Evoluciones } from '../../evoluciones/evoluciones.model';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-nuevoeditevolucion',
  templateUrl: './nuevoeditevolucion.component.html',
  styles: [
  ]
})
export class NuevoeditevolucionComponent {
  private baseURL = 'https://animatiomx.com/odonto/';
  accion: string;
  textodedialogo: string;
  formulariodiagnostico: FormGroup;
  evolucion: Evoluciones;
  clasificaciones: any = [];
  idPaciente = '';

  check: boolean;
  constructor(
    public dialogRef: MatDialogRef<NuevoeditevolucionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public ServicioEvolucion: EvolucionesService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
  ) {
            // obtenemos la accion que va hacer
            this.accion = data.action;
            this.idPaciente = data.idPaciente;
            if (this.accion === 'editar') {
              //texto del encabezado del modal
              this.textodedialogo = data.evolucion.Nombre + ' ' +data.evolucion.Apellido;
              //le pasamos los valores de evolucion
              this.evolucion = data.evolucion;
              this.check = true;
            } else {
              this.textodedialogo = 'Nueva evolucion';
              this.evolucion = new Evoluciones({});
            }
            this.formulariodiagnostico = this.formulariodediagnostico();
            this.DiagnosticosSelect(this.idPaciente);
    }

    formControl = new FormControl('', [
      Validators.required,
      Validators.email,
    ]);
    getErrorMessage() {
      return this.formControl.hasError('required')
        ? 'Required field'
        : this.formControl.hasError('email')
        ? 'Not a valid email'
        : '';
    }
    OnChange($event){
      console.log($event);
      if ($event.checked === true)
      {
        this.check = true;
      }
      if ($event.checked === false)
      {
         this.check = false;
      }
      console.log(this.check + ' Este es el check');
  }
    formulariodediagnostico(): FormGroup {
      const fecha = moment(this.evolucion.Fecha).format('MM/DD/YYYY');
      return this.fb.group({
        idE: [this.evolucion.idEvoluciones],
        fecha: [new Date(fecha)],
        obser: [this.evolucion.Observacion],
        descri: [this.evolucion.Descripcion],
        compli: [this.evolucion.Complicaciones],
        idDia: [this.evolucion.idDiagnostico],
        idPaciente: [this.evolucion.idPaciente]
      });
    }
    DiagnosticosSelect(idPa) {
      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { caso: 8, idP: idPa};
      const URL: any = this.baseURL + 'pacientes.php';
      this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
        respuesta => {
          this.clasificaciones = respuesta;
          console.log(this.clasificaciones);
        });
    }
    submit() {
      // emppty stuff
    }
    onNoClick(): void {
      this.dialogRef.close();
    }
    public confirmAdd(): void {
      if (this.accion === 'editar') {
        this.ServicioEvolucion.modificarEvol(this.formulariodiagnostico.getRawValue());
        console.log(this.formulariodiagnostico.getRawValue());
      } else {
        this.ServicioEvolucion.agregarEvolucion(this.formulariodiagnostico.getRawValue(), Number(this.idPaciente));
        console.log(this.formulariodiagnostico.getRawValue());
      }
    }
}
