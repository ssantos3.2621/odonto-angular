import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { EvolucionesService } from '../../evoluciones/evoluciones.service';
@Component({
  selector: 'app-eliminarevolucion',
  templateUrl: './eliminarevolucion.component.html',
  styles: [
  ]
})
export class EliminarevolucionComponent {


  constructor(
    public dialogRef: MatDialogRef<EliminarevolucionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public evolucionServicio: EvolucionesService
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    this.evolucionServicio.eliminarEvolucion(this.data.idEvoluciones);
  }

}
