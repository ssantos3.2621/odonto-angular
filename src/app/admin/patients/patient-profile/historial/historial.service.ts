import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { MatSnackBar, MatSnackBarModule } from "@angular/material/snack-bar";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HistorialService {
baseURL = 'https://animatiomx.com/odonto/';
historialPacient:any=[];
dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
dataChangeObservable=this.dataChange.asObservable();
  constructor(private snackBar: MatSnackBar,private httpClient: HttpClient) { }

  //guarda la inf en BD mediante http
  guardarHistorial = (historial:any,id) => {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = {
  'caso': 0,'Motivo':historial.motivo, 'EnfermedadActual':historial.enfermedadActual, 'Enfermedades':historial.enfermedades, 'AntecedentesQuiru':historial.antecedentesqrs, 'Alergias':historial.alergias, 'Medicamentos':historial.medicamentos, 'Habitos':historial.habitos, 'AntecedentesFamil':historial.antecedentesfam, 'Fuma':historial.Fuma, 'Embarazo':historial.Embarazo, 'ExamenLateral':historial.Examenlateral, 'Sobremordida':historial.Sobremordida, 'Cruzado':historial.cruzado, 'LineaSonrisa':historial.lineasonrisa, 'LineaMediaDentalSuperior':historial.linemediadentalsuperior, 'LineaMediaDentalInferior':historial.lineamediadentalinferior, 'LineaMediaDentalInfMM':historial.milinferior, 'ApiSuperior':historial.apinamientosuperior, 'ApiInferior':historial.apinamientoinferior, 'ApiObservacion':historial.obsapinamiento, 'CambiosEnElPisoDeLaBoca':historial.CambiosPisoBoca, 'Labios':historial.labios, 'Frenillos':historial.frenillo, 'CambioEnElEsmalteYLaDentina':historial.CambiosEsmalteDentina, 'Mejillas':historial.lasMejillas, 'Paladar':historial.paladar, 'Lengua':historial.lengua, 'ObservacionesEstoma':historial.observaestoma, 'SuccionDigi':historial.succionDigital, 'Onychophogia':historial.onychophogia, 'RespiracionOral':historial.resoral, 'MasticarEnfermedad':historial.masticarenfer, 'DeglucionAti':historial.deglucionAti, 'Bruxismo':historial.bruxismo, 'Musculo':historial.musculo, 'ATM':historial.atm, 'EnciasInflamadas':historial.enciasinfla, 'Placa':historial.placaperio, 'Calculo':historial.calculoperio, 'RecesionGingi':historial.recegingival, 'PerdidaInsercion':historial.perdidainser, 'Movilidad':historial.movilidadinser, 'ObservPeriodontal':historial.obserinsert, 'Frio':historial.pulpafrio, 'Caliente':historial.pulpacaliente, 'PruebaElectr':historial.pruebaelec, 'PercuHorizontal':historial.perhorizontal, 'PercuVertica':historial.pervertical, 'ObservacionesSensibi':historial.perobservaciones,'idPaciente':id
     };
    const URL: any = this.baseURL + 'historial.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        if (respuesta==null) {
          this.showNotification(
          'snackbar-success',
          'Historial clinico guardado correctamente...!!!',
          'bottom',
          'center'
        );
        }
      });
  }

  //actuliza la inf en BD mediante http
  actualizarHistorial = (historial:any,id) => {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = {
  'caso': 2,'Motivo':historial.motivo, 'EnfermedadActual':historial.enfermedadActual, 'Enfermedades':historial.enfermedades, 'AntecedentesQuiru':historial.antecedentesqrs, 'Alergias':historial.alergias, 'Medicamentos':historial.medicamentos, 'Habitos':historial.habitos, 'AntecedentesFamil':historial.antecedentesfam, 'Fuma':historial.Fuma, 'Embarazo':historial.Embarazo, 'ExamenLateral':historial.Examenlateral, 'Sobremordida':historial.Sobremordida, 'Cruzado':historial.cruzado, 'LineaSonrisa':historial.lineasonrisa, 'LineaMediaDentalSuperior':historial.linemediadentalsuperior, 'LineaMediaDentalInferior':historial.lineamediadentalinferior, 'LineaMediaDentalInfMM':historial.milinferior, 'ApiSuperior':historial.apinamientosuperior, 'ApiInferior':historial.apinamientoinferior, 'ApiObservacion':historial.obsapinamiento, 'CambiosEnElPisoDeLaBoca':historial.CambiosPisoBoca, 'Labios':historial.labios, 'Frenillos':historial.frenillo, 'CambioEnElEsmalteYLaDentina':historial.CambiosEsmalteDentina, 'Mejillas':historial.lasMejillas, 'Paladar':historial.paladar, 'Lengua':historial.lengua, 'ObservacionesEstoma':historial.observaestoma, 'SuccionDigi':historial.succionDigital, 'Onychophogia':historial.onychophogia, 'RespiracionOral':historial.resoral, 'MasticarEnfermedad':historial.masticarenfer, 'DeglucionAti':historial.deglucionAti, 'Bruxismo':historial.bruxismo, 'Musculo':historial.musculo, 'ATM':historial.atm, 'EnciasInflamadas':historial.enciasinfla, 'Placa':historial.placaperio, 'Calculo':historial.calculoperio, 'RecesionGingi':historial.recegingival, 'PerdidaInsercion':historial.perdidainser, 'Movilidad':historial.movilidadinser, 'ObservPeriodontal':historial.obserinsert, 'Frio':historial.pulpafrio, 'Caliente':historial.pulpacaliente, 'PruebaElectr':historial.pruebaelec, 'PercuHorizontal':historial.perhorizontal, 'PercuVertica':historial.pervertical, 'ObservacionesSensibi':historial.perobservaciones,'idPaciente':id
     };
    const URL: any = this.baseURL + 'historial.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        if (respuesta==null) {
          this.showNotification(
          'snackbar-success',
          'Historial clinico se actualizo correctamente...!!!',
          'bottom',
          'center'
        );
        }
      });
  }

  //obtener inf del historial de BD mediante http
  ObtenerHistorial = (id) => {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = {'caso': 1,'idPaciente':id  };
    const URL: any = this.baseURL + 'historial.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        if (respuesta!==null) {
          this.historialPacient=respuesta;
         this.dataChange.next(this.historialPacient);
        }else{
          this.historialPacient=[];
        }
      });
  }

showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}