import { Component, OnInit, Inject } from '@angular/core';
import { SubirFotoService } from '../subir-foto.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
@Component({
  selector: 'app-nuevoeditafoto',
  templateUrl: './nuevoeditafoto.component.html',
  styleUrls: [

  ]
})
export class NuevoeditafotoComponent implements OnInit {
  private baseURL = "https://animatiomx.com/odonto/";
  nombredearchivos: any = [];
  archivos: string[] = [];
  tipodearchivo: any = [];
  descripcion = '';
  idPaciente = '';
  constructor(public dialogRef: MatDialogRef<NuevoeditafotoComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private subirarchivo: SubirFotoService,
              private http: HttpClient) {
            // obtenemos la accion que va hacer
            this.idPaciente = data.idPaciente;
               }

  ngOnInit(): void {
  }

  onFileChanged(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = event.target.files[i].name.substring(
        event.target.files[i].name.lastIndexOf(".") + 1
      );
      this.tipodearchivo.push(ext);
    }
    // for (let index = 0; index < event.target.files[0].name.length; index++) {
    //   this.nombredearchivos.push(event.target.files[0].name[index]);
    // }
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }
  quitararchivo(i) {
    this.archivos.splice(i, 1);
    this.nombredearchivos.splice(i, 1);
    this.tipodearchivo.splice(i, 1);
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
    (<HTMLInputElement>document.getElementById("archivo")).value = "";
  }

  servidorarchivo(idPaciente, descri) {
    const formData = new FormData();
    for (var i = 0; i < this.archivos.length; i++) {
      formData.append("file[]", this.archivos[i]);
    }
    formData.append("idP", idPaciente);
    formData.append("desc", descri);
    this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
      if (resp.toString() !== "") {
        this.nombredearchivos = [];
        this.archivos = [];
      } else {
        console.log("ocurrio un error");
        this.nombredearchivos = [];
        this.archivos = [];
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  // Inserta la foto en bd
  crearcita2() {
    this.servidorarchivo(this.idPaciente, this.descripcion);
    this.onNoClick();
}
}
