import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubirFotoService {
    nombres: any = [];

    public url_servidor = "https://animatiomx.com/odonto/uploadarchivo.php";

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line:typedef
  public enviararchivo(form) {
  return this.http.post(this.url_servidor, form);
  }
}
