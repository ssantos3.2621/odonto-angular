import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { Recetas } from './recetas.model';
@Injectable({
  providedIn: 'root'
})
export class RecetasService {
    //intercambio de datos
    dataChange: BehaviorSubject<Recetas[]> = new BehaviorSubject<Recetas[]>([]);
    baseURL = 'https://animatiomx.com/odonto/';
    dialogData: any;
    recetas: any = [];
    constructor(private httpClient: HttpClient) {}
    // poner los valores de la consulta en el dataChange
    get data(): Recetas[] {
      return this.dataChange.value;
    }
    getDialogData() {
      return this.dialogData;
    }
    /** CRUD METHODS */
  // Obtiene las recetas del paciente
  obtenerRecetas(idP): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0, 'idPaciente': idP };
    const URL: any = this.baseURL + 'recetas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.recetas = respuesta;
        if (this.recetas !== null){
          this.dataChange.next(this.recetas);
          console.log(respuesta);
          console.log(this.dataChange);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }
  // Agrega receta
  agregarReceta(receta: Recetas, idpaciente: number): void {
    this.dialogData = receta;
    let fecha = moment(this.dialogData.fecha).format('YYYY/MM/DD');
    let Descripcion = this.dialogData.descripcion;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 1,'fecha': fecha,'descri': Descripcion, 'idPaciente': idpaciente };
    const URL: any = this.baseURL + 'recetas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
    // Modifica receta
    ModificarReceta(receta: Recetas): void {
      this.dialogData = receta;
      let idReceta = this.dialogData.id;
      let Descripcion = this.dialogData.descripcion;
      const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
      const options: any = { 'caso': 2, 'descri': Descripcion, 'idR': idReceta};
      const URL: any = this.baseURL + 'recetas.php';
      this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
        respuesta => {
        });
    }
  }
