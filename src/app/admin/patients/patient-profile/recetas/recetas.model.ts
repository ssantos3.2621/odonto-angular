export class Recetas {
    idRecetas: number;
    Fecha: string;
    Descripcion: string;
    idPacientes: number;
    fechahoy = new Date();

    constructor(receta) {
        {
          this.idRecetas = receta.idDiagnostico || this.getRandomID();
          this.Fecha = receta.Fecha || this.fechahoy;
          this.Descripcion = receta.Descripcion || '';
          this.idPacientes = receta.idPaciente || '';
        }
      }
      public getRandomID(): string {
        var S4 = function() {
          return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
      }
}
