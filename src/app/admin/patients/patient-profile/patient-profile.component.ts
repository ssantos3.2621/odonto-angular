import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, from, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataSource } from '@angular/cdk/collections';
import { DiagnosticoService } from './diagnostico/diagnostico.service';
import { Diagnostico } from './diagnostico/diagnostico.model';
import { NuevoeditardiagnosticoComponent } from './abcdiagnostico/nuevoeditardiagnostico/nuevoeditardiagnostico.component';
import { EliminardiagnosticoComponent } from './abcdiagnostico/eliminardiagnostico/eliminardiagnostico.component';
import { Router, ActivatedRoute } from '@angular/router';
import { NuevoedittratamientoComponent } from './abctratamiento/nuevoedittratamiento/nuevoedittratamiento.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HistorialService } from './historial/historial.service';
import {MatCheckbox} from '@angular/material/checkbox';
// Evoluciones
import { EvolucionesService } from './evoluciones/evoluciones.service';
import { Evoluciones } from './evoluciones/evoluciones.model';
import { NuevoeditevolucionComponent } from './abcevoluciones/nuevoeditevolucion/nuevoeditevolucion.component';
import { NuevoeditarpagoComponent } from './abcpagos/nuevoeditarpago/nuevoeditarpago.component';
import { EliminarevolucionComponent } from './abcevoluciones/eliminarevolucion/eliminarevolucion.component';
// Recetas
import { RecetasService } from './recetas/recetas.service';
import { Recetas } from './recetas/recetas.model';
import { NuevoeditarecetaComponent } from './abcrecetas/nuevoeditareceta/nuevoeditareceta.component';
// Fin evolucions
import { Pagos } from './pagos/pagos.model';
import { PagosService } from './pagos/pagos.service';
import { TratamientoService } from './tratamiento/tratamiento.service';
import { Tratamiento } from './tratamiento/tratamiento.model';
import { ElimtratamientoComponent } from './abctratamiento/elimtratamiento/elimtratamiento.component';
// Fotos
import { FotoService } from './fotos/fotos.service';
import { FotosS } from './fotos/fotos.model';
import { NuevoeditafotoComponent } from './abcFotos/nuevoeditafoto/nuevoeditafoto.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-patient-profile',
  templateUrl: './patient-profile.component.html',
  styleUrls: ['./patient-profile.component.scss']
})
export class PatientProfileComponent implements OnInit {
  private baseURL = 'https://animatiomx.com/odonto/';
  ColumnasdeDiagnostico = [
    'fechayhora',
    'clasif',
    'observa',
    //'nombre',
    'actions',
  ];
  ColumnasPlanTratamientos = [
    'fechat',
    'ProcedimientoServiciot',
    'Doctort',
    'Subtotalt',
    'Totalt',
    'actionst',
  ];
  ColumnasPagos = [
    'fechap',
    'noref',
    'ProcedimientoServiciop',
    'Subtotalp',
    'Totalp',
    'debep',
    'actionsp',
  ];
  ColumnasRecetas = [
    'Fecha',
    'Descripcion',
    'actions',
  ];
  ColumnasEvoluciones = [
    'fecha',
    'descripcion',
    'complicacion',
    'actions',
  ];
  ColumnasFotos = [
    'fecha',
    'imagen',
    'descripcion',
    'actions',
  ];
  ColumnasPresupuestos = [
    'Numero',
    'Fecha',
    'Servicio',
    'Total',
    'actions',
  ];
  ColumnasDocumentos = [
    'select',
    'fecha',
    'doctor',
    'receta',
    'actions',
  ];
  ColumnasListaPresupuestos = [
    'select',
    'fecha',
    'Npresupuesto',
    'Doctor',
    'Npaciente',
    'Tipo',
    'TotalTratamiento',
    'ProcedimientoServicio',
    'Estatus',
    'actions',
  ];
Nombredepaciente : String = '';
idPaciente: number;
diagnostico : boolean = false;
receta : boolean = false;
evolucion : boolean = false;
Ptratamiento : boolean = false;
Pagos : boolean = false;
diagnosticoBasededatos: DiagnosticoService | null;
datosDiagnostico: Diagnosticodatos | null;
selection = new SelectionModel<Diagnostico>(true, []);
index: number;
id: number;
idDiagnostico: number;
historialForm: FormGroup;
si:any = 'si';
historialPaciente:any=[];
form=false;
editar:boolean;
isedit:boolean;
// Evoluciones
EvolucionBD: EvolucionesService | null;
selectionEv = new SelectionModel<Evoluciones>(true, []);
datosEvoluciones: EvolucionDatos | null;
evolucionB: boolean = false;
idEvolucion: number;
// Tratamientos
TratamientosBD: TratamientoService | null;
selectiontra = new SelectionModel<Tratamiento>(true, []);
datostratamintos: TratamientosDatos | null;
idTratamiento: number;

// Pagos
PagosBD: PagosService | null;
selectionPa = new SelectionModel<Pagos>(true, []);
datosPagos: PagosDatos | null;

// Recetas
recetaBD: RecetasService | null;
selectionRe = new SelectionModel<Recetas>(true, []);
datosRecetas: recetaDatos | null;
recetaB:boolean = false;
idRecetas: number;

// Fotos
fotoBD: FotoService | null;
selectionFo = new SelectionModel<FotosS>(true, []);
datosFotos: fotoDatos | null;
fotosB: boolean = false;
idFoto: number;
isChecked = '';
idEliminados: any = [];
idEliminadosF: any = [];
// asignar nombre a paginador de evoluciones
@ViewChild('paginatore', { static: true }) paginadorEvolucion: MatPaginator;
@ViewChild('tbevoluciones', { static: true }) ordenarEvolucion: MatSort;
@ViewChild('filtroEvoluciones', { static: true }) filtroEvoluciones: ElementRef;

// asignar nombre a paginador de tratamientos
@ViewChild('paginatort', { static: true }) paginadortratamientos: MatPaginator;
@ViewChild('tbtratamiento', { static: true }) ordenartratamientos: MatSort;
@ViewChild('filtrotratamiento', { static: true }) filtrotratamientos: ElementRef;

// asignar nombre a paginador
@ViewChild('paginator', { static: true }) paginadordiagnostico: MatPaginator;
@ViewChild('tbdiagnostico', { static: true }) ordenardiagnostico: MatSort;
@ViewChild('filtrodiagnostico', { static: true }) filtrodiagnostico: ElementRef;

// asignar nombre a paginador de pagos
@ViewChild('paginatorp', { static: true }) paginadorPagos: MatPaginator;
@ViewChild('tbpagos', { static: true }) ordenarPagos: MatSort;
@ViewChild('filtroPagos', { static: true }) filtroPagos: ElementRef;

// asignar nombre a paginador de recetas
@ViewChild('paginatorR', { static: true }) paginadorRecetas: MatPaginator;
@ViewChild('tbrecetas', { static: true }) ordenarRecetas: MatSort;
@ViewChild('filtroRecetas', { static: true }) filtroRecetas: ElementRef;

// asignar nombre a paginador de recetas
@ViewChild('paginatorFo', { static: true }) paginadorFotos: MatPaginator;
@ViewChild('tbFotos', { static: true }) ordenarFotos: MatSort;
@ViewChild('filtroFotos', { static: true }) filtroFotos: ElementRef;
patientForm: FormGroup;

  constructor(
    public fotoSrvc: FotoService,
    public hitorialSv: HistorialService,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public diagnosticoServicio: DiagnosticoService,
    public EvolucionServicio: EvolucionesService,
    public TratamientoServicio: TratamientoService,
    private snackBar: MatSnackBar,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public pagoservice: PagosService,
  ) {
    this.route.params.subscribe(parametros => {
      this.idPaciente = parametros['id_P'];
      this.Nombredepaciente = parametros['Nombre'];
    });

     this.patientForm = this.fb.group({
      first: ['', [Validators.required, Validators.pattern('[a-zA-Z]+')]],
      last: [''],
      gender: ['', [Validators.required]],
      mobile: [''],
      dob: ['', [Validators.required]],
      Edad: ['', [Validators.required]],
      age: [''],
      email: [
        '',
        [Validators.required, Validators.email, Validators.minLength(5)],
      ],
      maritalStatus: [''],
      address: [''],
      bGroup: [''],
      bPresure: [''],
      sugger: [''],
      injury: [''],
      uploadImg: [''],
      rfc: [''],
      convenio: [''],
      notas: [''],
      telefono: [''],
      responsable: [''],
    });
  }

  onSubmit() {
    console.log('Form Value', this.patientForm.value);
  }


   ngOnInit() {
     this.cargardatosdiagnostico();
     this.cargardatosEvolucion();
     this.cargardatosRecetas();
     this.cargardatosFotos();
     this.ObtenerHistorial(this.idPaciente,true);
     this.cargardatostratamiento();
     this.cargardatosPagos();
    }

    //llama servicio el cual inserta la inf del historial clinico en BD
      historialSubmit() {
        this.hitorialSv.guardarHistorial(this.historialForm.value,this.idPaciente);
        this.historialForm.disable();
        this.isedit=false;
        this.ObtenerHistorial(this.idPaciente,true);
      }
    //Editar el formulario de historial clinico y enviarlo a BD
      actualizar() {
        this.hitorialSv.actualizarHistorial(this.historialForm.value,this.idPaciente);
        this.historialForm.disable();
        this.isedit=false;
      }
      //Activa formulario de historial clinico
  activarform(){
    this.historialForm.enable();
    this.isedit=true;
  }

  //trae la inf del historial clinico desde la BD
  ObtenerHistorial = (id,activo) => {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = {'caso': 1,'idPaciente':id  };
    const URL: any = this.baseURL + 'historial.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
          this.historialPaciente=respuesta;
            if (this.historialPaciente==null) {
              this.historialForm = this.fb.group({
              motivo: ['',],enfermedadActual: [''],enfermedades: ['', ],antecedentesqrs: ['', ],
              alergias: ['', ],medicamentos: ['', ],habitos: [''],antecedentesfam: [''],
              linemediadentalsuperior: [''],milsuperior: [''],lineamediadentalinferior: ['', ],
              milinferior: [''],apinamientosuperior: [''],apinamientoinferior: ['',],
              obsapinamiento: [''],obsanalisis: ['', ],perdidainser: ['', ],movilidadinser: ['', ],
              obserinsert: ['', ],pulpafrio: [''],pulpacaliente: [''],perhorizontal: [''],pervertical: [''],
              perobservaciones: ['', ],Fuma: [''],Embarazo: [''],Examenlateral: ['',],Sobremordida: [''],
              cruzado: ['', ],lineasonrisa: ['', ],CambiosPisoBoca: ['', ],lasMejillas: ['', ],           labios: [''],paladar: [''],frenillo: [''],lengua: [''],CambiosEsmalteDentina: ['', ],
              observaestoma: [''],succionDigital: [''],deglucionAti: ['',],onychophogia: [''],
              bruxismo: ['', ],resoral: ['', ],musculo: ['', ],masticarenfer: ['', ],atm: [''],
              enciasinfla: [''],placaperio: [''],calculoperio: [''],pruebaelec: [''],recegingival: ['', ],
            });
            this.editar=false;
            this.form=true;
            }else{
              this.historialForm = this.fb.group({
          motivo: [{value:this.historialPaciente[0].Motivo||'', disabled: activo}],
          enfermedadActual: [{value:this.historialPaciente[0].EnfermedadActual||'', disabled: activo}],
          enfermedades: [{value:this.historialPaciente[0].Enfermedades ||'', disabled: activo}],
          antecedentesqrs: [{value:this.historialPaciente[0].AntecedentesQuiru ||'', disabled: activo}],
          alergias: [{value:this.historialPaciente[0].Alergias ||'', disabled: activo}],
          medicamentos: [{value:this.historialPaciente[0].Medicamentos ||'', disabled: activo}],
          habitos: [{value:this.historialPaciente[0].Habitos||'', disabled: activo}],
          antecedentesfam: [{value:this.historialPaciente[0].AntecedentesFamil||'', disabled: activo}],
          linemediadentalsuperior: [{value:this.historialPaciente[0].LineaMediaDentalSuperior||'', disabled: activo}],
          milsuperior: [{value:this.historialPaciente[0].LineaMediaDentalInfMM||'', disabled: activo}],
          lineamediadentalinferior: [{value:this.historialPaciente[0].LineaMediaDentalInferior ||'', disabled: activo}],
          milinferior: [{value:this.historialPaciente[0].LineaMediaDentalInfMM||'', disabled: activo}],
          apinamientosuperior: [{value:this.historialPaciente[0].ApiSuperior||'', disabled: activo}],
          apinamientoinferior: [{value:this.historialPaciente[0].ApiInferior||'', disabled: activo}],
          obsapinamiento: [{value:this.historialPaciente[0].ApiObservacion||'', disabled: activo}],
          obsanalisis: [{value:this.historialPaciente[0].ApiObservacion ||'', disabled: activo}],
          perdidainser: [{value:this.historialPaciente[0].PerdidaInsercion ||'', disabled: activo}],
          movilidadinser: [{value:this.historialPaciente[0].Movilidad ||'', disabled: activo}],
          obserinsert: [{value:this.historialPaciente[0].ObservPeriodontal ||'', disabled: activo}],
          pulpafrio: [{value:this.historialPaciente[0].Frio||'', disabled: activo}],
          pulpacaliente: [{value:this.historialPaciente[0].Caliente||'', disabled: activo}],
          perhorizontal: [{value:this.historialPaciente[0].PercuHorizontal||'', disabled: activo}],
          pervertical: [{value:this.historialPaciente[0].PercuVertica||'', disabled: activo}],
          perobservaciones: [{value:this.historialPaciente[0].ObservacionesSensibi ||'', disabled: activo}],
          Fuma: [{value:this.historialPaciente[0].Fuma||'', disabled: activo}],
          Embarazo: [{value:this.historialPaciente[0].Embarazo||'', disabled: activo}],
          Examenlateral: [{value:this.historialPaciente[0].ExamenLateral||'', disabled: activo}],
          Sobremordida: [{value:this.historialPaciente[0].Sobremordida||'', disabled: activo}],
          cruzado: [{value:this.historialPaciente[0].Cruzado ||'', disabled: activo}],
          lineasonrisa: [{value:this.historialPaciente[0].LineaSonrisa ||'', disabled: activo}],
          CambiosPisoBoca: [{value:this.historialPaciente[0].CambiosEnElPisoDeLaBoca ||'', disabled:activo}],
          lasMejillas: [{value:this.historialPaciente[0].Mejillas ||'', disabled: activo}],
          labios: [{value:this.historialPaciente[0].Labios||'', disabled: activo}],
          paladar: [{value:this.historialPaciente[0].Paladar||'', disabled: activo}],
          frenillo: [{value:this.historialPaciente[0].Frenillos||'', disabled: activo}],
          lengua: [{value:this.historialPaciente[0].Lengua||'', disabled: activo}],
          CambiosEsmalteDentina: [{value:this.historialPaciente[0].CambioEnElEsmalteYLaDentina ||'', disabled: activo}],
          observaestoma: [{value:this.historialPaciente[0].ObservacionesEstoma||'', disabled: activo}],
          succionDigital: [{value:this.historialPaciente[0].SuccionDigi||'', disabled: activo}],
          deglucionAti: [{value:this.historialPaciente[0].DeglucionAti||'', disabled: activo}],
          onychophogia: [{value:this.historialPaciente[0].Onychophogia||'', disabled: activo}],
          bruxismo: [{value:this.historialPaciente[0].Bruxismo ||'', disabled: activo}],
          resoral: [{value:this.historialPaciente[0].RespiracionOral ||'', disabled: activo}],
          musculo: [{value:this.historialPaciente[0].Musculo ||'', disabled: activo}],
          masticarenfer: [{value:this.historialPaciente[0].MasticarEnfermedad ||'', disabled: activo}],
          atm: [{value:this.historialPaciente[0].ATM||'', disabled: activo}],
          enciasinfla: [{value:this.historialPaciente[0].EnciasInflamadas||'', disabled: activo}],
          placaperio: [{value:this.historialPaciente[0].Placa||'', disabled: activo}],
          calculoperio: [{value:this.historialPaciente[0].Calculo||'', disabled: activo}],
          pruebaelec: [{value:this.historialPaciente[0].PruebaElectr||'', disabled: activo}],
          recegingival: [{value:this.historialPaciente[0].RecesionGingi ||'', disabled: activo}],
        });
        this.editar=true;
        this.form=true;
            }
      });
  }

  //recargar datos de la tabla de diagnostico
  refrescartabladiagnostico() {
    this.cargardatosdiagnostico();
  }
  //refrescar la tabla para poner de nuevo los paginadores
  private refrescartabladediagnostico() {
    this.paginadordiagnostico._changePageSize(this.paginadordiagnostico.pageSize);
  }
  public cargardatosdiagnostico() {
    //obtenemos los datos de la tabla de diagnostico y lo guardamos en su variable
    this.diagnosticoBasededatos = new DiagnosticoService(this.httpClient);
    // se manda a llamar la clase de diagnostico y se manda los valores de base de datos el paginador
    this.datosDiagnostico = new Diagnosticodatos(
      this.diagnosticoBasededatos,
      this.paginadordiagnostico,
      this.ordenardiagnostico,
      this.idPaciente
    );
    // metodo para verificar el evento de filtro
    fromEvent(this.filtrodiagnostico.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.datosDiagnostico) {
          return;
        }
        //obtenemos los valores escritos en el input
        this.datosDiagnostico.letrafiltro = this.filtrodiagnostico.nativeElement.value;
      });
  }

  nuevodiagnostico() {
    const dialogRef = this.dialog.open(NuevoeditardiagnosticoComponent, {
      width: '60%',
      data: {
        patient: this.diagnostico,
        action: 'nuevo',
        idPaciente: this.idPaciente
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.diagnosticoBasededatos.dataChange.value.unshift(
          //this.diagnosticoBasededatos.getDialogData()
        );
        this.refrescartabladiagnostico();
        this.showNotification(
          'snackbar-success',
          'Diagnostico guardado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }

  editardiagnostico(row) {
    this.idDiagnostico = row.idDiagnostico;
    console.log(this.idDiagnostico);
    const dialogRef = this.dialog.open(NuevoeditardiagnosticoComponent, {
      width: '60%',
      data: {
        diagnostico: row,
        action: 'editar',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.diagnosticoBasededatos.dataChange.value.findIndex(
          (x) => x.idDiagnostico === this.id
        );
        this.refrescartabladiagnostico();
        this.refrescartabladediagnostico();
        this.showNotification(
          'black',
          'Diagnostico editado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }

  eliminardiagnostico(i: number, row) {
    this.index = i;
    this.idDiagnostico = row.idDiagnostico;
    console.log(this.idDiagnostico);
    const dialogRef = this.dialog.open(EliminardiagnosticoComponent, {
      data: row,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.diagnosticoBasededatos.dataChange.value.findIndex(
          (x) => x.idDiagnostico === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this.diagnosticoBasededatos.dataChange.value.splice(foundIndex, 1);
        this.refrescartabladiagnostico();
        this.refrescartabladediagnostico();
        this.showNotification(
          'snackbar-danger',
          'Diagnostico eliminado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }


  nuevoPago() {
    const dialogRef = this.dialog.open(NuevoeditarpagoComponent, {
      width: '60%',
      data: {
        patient: this.diagnostico,
        action: 'nuevo',
        idPaciente: this.idPaciente
      },
      disableClose: false
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        this.diagnosticoBasededatos.dataChange.value.unshift(
        );
        this.refrescartabladiagnostico();
        this.showNotification(
          'snackbar-success',
          'Diagnostico guardado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

ModalDiagnostico(open : boolean) : void {
    this.diagnostico = open;
}
ModalReceta(open : boolean) : void {
    this.receta = open;
}
ModalEvolucion(open : boolean) : void {
    this.evolucion = open;
}
ModalPtratamiento(open : boolean) : void {
    this.Ptratamiento = open;
}
ModalPagos(open : boolean) : void {
    this.Pagos = open;
}
  tabs = ['First', 'Second', 'Third'];
  selected = new FormControl(0);
  addTab(selectAfterAdding: boolean) {
    this.tabs.push('New');
    if (selectAfterAdding) {
      this.selected.setValue(this.tabs.length - 1);
    }
  }
  removeTab(index: number) {
    this.tabs.splice(index, 1);
  }
  // Recarga la tabla fotos
  refrescartablaFotos() {
    this.cargardatosFotos();
  }
  // Recarga paginador
  private refrescartabladeFotos() {
    this.paginadorFotos._changePageSize(this.paginadorFotos.pageSize);
  }
  // Carga las fotos del paciente
  public cargardatosFotos() {
    //obtenemos los datos de la tabla de diagnostico y lo guardamos en su variable
    this.fotoBD = new FotoService(this.httpClient);
    // se manda a llamar la clase de diagnostico y se manda los valores de base de datos el paginador
    this.datosFotos = new fotoDatos(
      this.fotoBD,
      this.paginadorFotos,
      this.ordenarFotos,
      this.idPaciente
    );
    // metodo para verificar el evento de filtro
    fromEvent(this.filtroFotos.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.datosFotos) {
          return;
        }
        //obtenemos los valores escritos en el input
        this.datosFotos.letrafiltro = this.filtroFotos.nativeElement.value;
      });
  }
  // Agrega fotos a paciente
  fotoN()
  {
    this.nuevaFoto();
    this.refrescartablaFotos();
  }
  nuevaFoto() {
    const dialogRef = this.dialog.open(NuevoeditafotoComponent, {
      width: '60%',
      data: {
        patient: this.fotosB,
        action: 'nuevo',
        idPaciente: this.idPaciente
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.fotoBD.dataChange.value.unshift(
          //this.diagnosticoBasededatos.getDialogData()
        );
        this.refrescartablaFotos();
        this.refrescartabladeFotos();
        this.showNotification(
          'snackbar-success',
          'Receta guardada correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  // Metodo para eliminar y agregar los id de las fotos a borrar
  checkValue($event, id){
    console.log($event.checked + id + 'El id a borrar');
    if ($event.checked === true)
    {
      for (let index = 0; index <= this.idEliminados.length; index++) {
        const element = this.idEliminados[index];
        this.idEliminadosF.push(id);
      }
    }
    else
    {
      for (let index = 0; index <= this.idEliminados.length; index++) {
        const element = this.idEliminados[index];
        this.idEliminadosF.splice(index, 1);
      }
    }
    console.log(this.idEliminadosF);
 }
// Elimina fotos
confirmDelete(): void {
  Swal.fire({
    title: 'Seguro de eliminar las fotos?',
    text: "Esto no se puede revertir!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Borrar',
    cancelButtonText: 'Cancelar'
  }).then((result) => {
    if (result.isConfirmed) {
      this.fotoSrvc.eliminarFoto(this.idEliminadosF);
      this.refrescartablaRecetas();
      this.refrescartabladeRecetas();
      Swal.fire(
        'Elminadas!',
        'Las fotos han sido eliminadas.',
        'success'
      );
    }
  });
  this.refrescartablaRecetas();
  this.refrescartabladeRecetas();
}

// Apartado recetas Sergio
refrescartablaRecetas() {
  this.cargardatosRecetas();
}

private refrescartabladeRecetas() {
  this.paginadorRecetas._changePageSize(this.paginadorRecetas.pageSize);
}
public cargardatosRecetas() {
  //obtenemos los datos de la tabla de diagnostico y lo guardamos en su variable
  this.recetaBD = new RecetasService(this.httpClient);
  // se manda a llamar la clase de diagnostico y se manda los valores de base de datos el paginador
  this.datosRecetas = new recetaDatos(
    this.recetaBD,
    this.paginadorRecetas,
    this.ordenarRecetas,
    this.idPaciente
  );
  // metodo para verificar el evento de filtro
  fromEvent(this.filtroRecetas.nativeElement, 'keyup')
    .subscribe(() => {
      if (!this.datosRecetas) {
        return;
      }
      //obtenemos los valores escritos en el input
      this.datosRecetas.letrafiltro = this.filtroRecetas.nativeElement.value;
    });
}
nuevaReceta() {
  const dialogRef = this.dialog.open(NuevoeditarecetaComponent, {
    width: '60%',
    data: {
      patient: this.recetaB,
      action: 'nuevo',
      idPaciente: this.idPaciente
    },
    disableClose: true
  });
  dialogRef.afterClosed().subscribe((result) => {
    if (result === 1) {
      // After dialog is closed we're doing frontend updates
      // For add we're just pushing a new row inside DataService
      this.diagnosticoBasededatos.dataChange.value.unshift(
        //this.diagnosticoBasededatos.getDialogData()
      );
      this.refrescartablaRecetas();
      this.showNotification(
        'snackbar-success',
        'Receta guardada correctamente...!!!',
        'bottom',
        'center'
      );
    }
  });
}
editarReceta(row) {
  this.idRecetas = row.idRecetas;
  console.log(this.idRecetas);
  const dialogRef = this.dialog.open(NuevoeditarecetaComponent, {
    width: '60%',
    data: {
      receta: row,
      action: 'editar',
      idPaciente: this.idPaciente
    },
  });
  dialogRef.afterClosed().subscribe((result) => {
    if (result === 1) {
      const foundIndex = this.recetaBD.dataChange.value.findIndex(
        (x) => x.idRecetas === this.id
      );
      this.refrescartablaRecetas();
      this.refrescartabladeRecetas();
      this.showNotification(
        'black',
        'Evolucion editada correctamente...!!!',
        'bottom',
        'center'
      );
    }
  });
}


  // Apartado de evoluciones
  refrescartablaEvolucion() {
    this.cargardatosEvolucion();
  }

  private refrescartabladeEvolucion() {
    this.paginadorEvolucion._changePageSize(this.paginadorEvolucion.pageSize);
  }

  public cargardatosEvolucion() {
    //obtenemos los datos de la tabla de diagnostico y lo guardamos en su variable
    this.EvolucionBD = new EvolucionesService(this.httpClient);
    // se manda a llamar la clase de diagnostico y se manda los valores de base de datos el paginador
    this.datosEvoluciones = new EvolucionDatos(
      this.EvolucionBD,
      this.paginadorEvolucion,
      this.ordenarEvolucion,
      this.idPaciente
    );
    // metodo para verificar el evento de filtro
    fromEvent(this.filtroEvoluciones.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.datosEvoluciones) {
          return;
        }
        //obtenemos los valores escritos en el input
        this.datosEvoluciones.letrafiltro = this.filtroEvoluciones.nativeElement.value;
      });
  }

  eliminarEvolucion(i: number, row) {
    this.index = i;
    this.idEvolucion = row.idEvoluciones;
    console.log(this.idEvolucion);
    const dialogRef = this.dialog.open(EliminarevolucionComponent, {
      data: row,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.EvolucionBD.dataChange.value.findIndex(
          (x) => x.idEvoluciones === this.id
        );
        // for delete we use splice in order to remove single object from DataService
        this.EvolucionBD.dataChange.value.splice(foundIndex, 1);
        this.refrescartablaEvolucion();
        this.refrescartabladediagnostico();
        this.showNotification(
          'snackbar-danger',
          'Evolución eliminada correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  editarEvol(row) {
    this.idEvolucion = row.idEvoluciones;
    console.log(this.idEvolucion);
    const dialogRef = this.dialog.open(NuevoeditevolucionComponent, {
      width: '60%',
      data: {
        evolucion: row,
        action: 'editar',
        idPaciente: this.idPaciente
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.EvolucionBD.dataChange.value.findIndex(
          (x) => x.idEvoluciones === this.id
        );
        this.refrescartablaEvolucion();
        this.refrescartabladediagnostico();
        this.showNotification(
          'black',
          'Evolucion editada correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  nuevaEvolucion() {
    const dialogRef = this.dialog.open(NuevoeditevolucionComponent, {
      width: '60%',
      data: {
        patient: this.evolucionB,
        action: 'nuevo',
        idPaciente: this.idPaciente
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.diagnosticoBasededatos.dataChange.value.unshift(
          //this.diagnosticoBasededatos.getDialogData()
        );
        this.refrescartablaEvolucion();
        this.showNotification(
          'snackbar-success',
          'Evolucion guardada correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }

  refreshpagos(){
    this.cargardatosPagos();
  }
  // Apartado de Pagos

  // recargar datos de la tabla de pagos
  refrescartablapago() {
    this.cargardatosPagos();
  }
  // refrescar la tabla para poner de nuevo los paginadores de pago
  private refrescartabladepago() {
    this.paginadortratamientos._changePageSize(this.paginadortratamientos.pageSize);
  }

  public cargardatosPagos() {
    // obtenemos los datos de la tabla de pagos y lo guardamos en su variable
    this.PagosBD = new PagosService(this.httpClient);
    // se manda a llamar la clase de pagos y se manda los valores de base de datos el paginador
    this.datosPagos = new PagosDatos(
      this.PagosBD,
      this.paginadorPagos,
      this.ordenarPagos,
      this.idPaciente
    );
    // metodo para verificar el evento de filtro
    fromEvent(this.filtroPagos.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.datosPagos) {
          return;
        }
        //obtenemos los valores escritos en el input
        this.datosPagos.letrafiltro = this.filtroPagos.nativeElement.value;
      });
  }
  // funciones de plan de tratamiento

    //recargar datos de la tabla de tratamientos
    refrescartablatratamiento() {
      this.cargardatostratamiento();
    }
    //refrescar la tabla para poner de nuevo los paginadores
    private refrescartabladetratamiento() {
      this.paginadortratamientos._changePageSize(this.paginadortratamientos.pageSize);
    }


    public cargardatostratamiento() {
      //obtenemos los datos de la tabla de diagnostico y lo guardamos en su variable
      this.TratamientosBD = new TratamientoService(this.httpClient);
      // se manda a llamar la clase de diagnostico y se manda los valores de base de datos el paginador
      this.datostratamintos = new TratamientosDatos(
        this.TratamientosBD,
        this.paginadortratamientos,
        this.ordenartratamientos,
        this.idPaciente
      );
      // metodo para verificar el evento de filtro
      fromEvent(this.filtrotratamientos.nativeElement, 'keyup')
        .subscribe(() => {
          if (!this.datostratamintos) {
            return;
          }
          //obtenemos los valores escritos en el input
          this.datostratamintos.letrafiltro = this.filtrotratamientos.nativeElement.value;
        });
    }
    // metodo nuevo plan de tratamiento
    nuevotratamiento() {
      const dialogRef = this.dialog.open(NuevoedittratamientoComponent, {
        maxWidth: '90vw',
        width: '90%',
        data: {
          tratamiento: this.diagnostico,
          action: 'nuevo',
          idPaciente: this.idPaciente
        },
        disableClose: true
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result === 1) {
          this.TratamientosBD.dataChange.value.unshift(
          );
          this.refrescartablatratamiento();
          this.refrescartabladetratamiento();
          this.showNotification(
            'snackbar-success',
            'Diagnostico guardado correctamente...!!!',
            'bottom',
            'center'
          );
        }
      });
    }
    // metodo editar plan de tratamiento
    editartratamiento(row) {
      this.idTratamiento = row.idPlanTratamiento;
      console.log(this.idTratamiento);
      const dialogRef = this.dialog.open(NuevoedittratamientoComponent, {
        maxWidth: '90vw',
        width: '90%',
        data: {
          tratamiento: row,
          action: 'editar',
        },
        disableClose: true
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result === 1) {
          const foundIndex = this.TratamientosBD.dataChange.value.findIndex(
            (x) => x.idPlanTratamiento === this.id
          );
          this.refrescartablatratamiento();
          this.refrescartabladetratamiento();
          this.showNotification(
            'black',
            'Plan de tratamiento editado correctamente...!!!',
            'bottom',
            'center'
          );
        }
      });
    }
    // metodo eliminar plan de tratamiento
    eliminartratamiento(i: number, row) {
      this.index = i;
      this.idTratamiento = row.idPlanTratamiento;
      console.log(this.idTratamiento);
      const dialogRef = this.dialog.open(ElimtratamientoComponent, {
        data: row,
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result === 1) {
          const foundIndex = this.TratamientosBD.dataChange.value.findIndex(
            (x) => x.idPlanTratamiento === this.idTratamiento
          );
          // for delete we use splice in order to remove single object from DataService
          this.diagnosticoBasededatos.dataChange.value.splice(foundIndex, 1);
          this.refrescartablatratamiento();
          this.refrescartabladetratamiento();
          this.showNotification(
            'snackbar-danger',
            'Plan de tratamiento eliminado correctamente...!!!',
            'bottom',
            'center'
          );
        }
      });
    }
}

export class Diagnosticodatos extends DataSource<Diagnostico> {
  //buscadorfiltro es metodo y letrafiltro es los valores del input
  buscadorfiltro = new BehaviorSubject('');
  //obtenemos las letras
  get letrafiltro(): string {
    return this.buscadorfiltro.value;
  }
  //mandamos las letras del filtro
  set letrafiltro(letrafiltro: string) {
    this.buscadorfiltro.next(letrafiltro);
  }
  filtrodatos: Diagnostico[] = [];
  renderizardatos: Diagnostico[] = [];
  constructor(
    public Diagnosticodatos: DiagnosticoService,
    public paginadorDiagnostico: MatPaginator,
    public ordenar: MatSort,
    public idPaciente
  ) {
    super();
    // Restablecer a la primera pagina cuando se haga un filtro
    this.buscadorfiltro.subscribe(() => (this.paginadorDiagnostico.pageIndex = 0));
  }
  /** Función de conexión llamada por la tabla para recuperar una secuencia que contiene los datos para representar. */
  connect(): Observable<Diagnostico[]> {
    // escuchar de evento
    const displayDataChanges = [
      this.Diagnosticodatos.dataChange,
      this.ordenar.sortChange,
      this.buscadorfiltro,
      this.paginadorDiagnostico.page,
    ];
    this.Diagnosticodatos.obtenerdiagnostico(this.idPaciente);
    return merge(...displayDataChanges).pipe(
      map(() => {
        // metodo para filtrar datos
        this.filtrodatos = this.Diagnosticodatos.data
          .slice()
          .filter((diagnostico: Diagnostico) => {
            const searchStr = (
              diagnostico.nombre +
              diagnostico.Observaciones +
              diagnostico.Nombre +
              diagnostico.Apellido
            ).toLowerCase();
            return searchStr.indexOf(this.letrafiltro.toLowerCase()) !== -1;
          });
        // ordenar datos filtrados
        const ordenardatos = this.metodoordenardatos(this.filtrodatos.slice());
        // metodo para order los datos y hacer las paginas
        const empezardelprincipio = this.paginadorDiagnostico.pageIndex * this.paginadorDiagnostico.pageSize;
        this.renderizardatos = ordenardatos.splice(
          empezardelprincipio,
          this.paginadorDiagnostico.pageSize
        );
        return this.renderizardatos;
      })
    );
  }
  disconnect() {}
  /** Devuelve una copia ordenada de los datos de la base de datos. */
  metodoordenardatos(data: Diagnostico[]): Diagnostico[] {
    //copia todos los datos de diagnostico y los pone en una tabla para las propiedades
    if (!this.ordenar.active || this.ordenar.direction === '') {
      return data;
    }
    // metodo para poder poner la tabla en ascendente y descendente
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this.ordenar.active) {
        case 'id':
          [propertyA, propertyB] = [a.idDiagnostico, b.idDiagnostico];
          break;
        case 'fechayhora':
          [propertyA, propertyB] = [a.Fecha, b.Fecha];
          break;
        case 'clasif':
          [propertyA, propertyB] = [a.nombre, b.nombre];
          break;
        case 'observa':
          [propertyA, propertyB] = [a.Observaciones, b.Observaciones];
          break;
        case 'nombre':
          [propertyA, propertyB] = [a.Nombre, b.Nombre];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this.ordenar.direction === 'asc' ? 1 : -1)
      );
    });
  }
}


// Clase de evoluciones
export class EvolucionDatos extends DataSource<Evoluciones> {
  //buscadorfiltro es metodo y letrafiltro es los valores del input
  buscadorfiltro = new BehaviorSubject('');
  //obtenemos las letras
  get letrafiltro(): string {
    console.log(this.buscadorfiltro.value);
    return this.buscadorfiltro.value;
  }
  //mandamos las letras del filtro
  set letrafiltro(letrafiltro: string) {
    this.buscadorfiltro.next(letrafiltro);
  }
  filtrodatos: Evoluciones[] = [];
  renderizardatos: Evoluciones[] = [];
  constructor(
    public EvoluciDatos: EvolucionesService,
    public paginadorEvolu: MatPaginator,
    public ordenar: MatSort,
    public idPaciente
  ) {
    super();
    // Restablecer a la primera pagina cuando se haga un filtro
    this.buscadorfiltro.subscribe(() => (this.paginadorEvolu.pageIndex = 0));
  }
  /** Función de conexión llamada por la tabla para recuperar una secuencia que contiene los datos para representar. */
  connect(): Observable<Evoluciones[]> {
    // escuchar de evento
    const displayDataChanges = [
      this.EvoluciDatos.dataChange,
      this.ordenar.sortChange,
      this.buscadorfiltro,
      this.paginadorEvolu.page,
    ];
    this.EvoluciDatos.obtenerEvaluacion(this.idPaciente);
    return merge(...displayDataChanges).pipe(
      map(() => {
        // metodo para filtrar datos
        this.filtrodatos = this.EvoluciDatos.data
          .slice()
          .filter((evolucion: Evoluciones) => {
            const searchStr = (
              evolucion.Observacion +
              evolucion.Descripcion +
              evolucion.Complicaciones +
              evolucion.Fecha
            ).toLowerCase();
            return searchStr.indexOf(this.letrafiltro.toLowerCase()) !== -1;
          });
        // ordenar datos filtrados
        const ordenardatos = this.metodoordenardatos(this.filtrodatos.slice());
        // metodo para order los datos y hacer las paginas
        const empezardelprincipio = this.paginadorEvolu.pageIndex * this.paginadorEvolu.pageSize;
        this.renderizardatos = ordenardatos.splice(
          empezardelprincipio,
          this.paginadorEvolu.pageSize
        );
        return this.renderizardatos;
      })
    );
  }
  disconnect() {}
  /** Devuelve una copia ordenada de los datos de la base de datos. */
  metodoordenardatos(data: Evoluciones[]): Evoluciones[] {
    //copia todos los datos de diagnostico y los pone en una tabla para las propiedades
    if (!this.ordenar.active || this.ordenar.direction === '') {
      return data;
    }
    // metodo para poder poner la tabla en ascendente y descendente
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this.ordenar.active) {
        case 'id':
          [propertyA, propertyB] = [a.idEvoluciones, b.idEvoluciones];
          break;
        case 'fechayhora':
          [propertyA, propertyB] = [a.Fecha, b.Fecha];
          break;
        case 'clasif':
          [propertyA, propertyB] = [a.Observacion, b.Observacion];
          break;
        case 'observa':
          [propertyA, propertyB] = [a.Descripcion, b.Descripcion];
          break;
        case 'nombre':
          [propertyA, propertyB] = [a.Complicaciones, b.Complicaciones];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this.ordenar.direction === 'asc' ? 1 : -1)
      );
    });
  }
}

// Clase de tratamientos
export class TratamientosDatos extends DataSource<Tratamiento> {
  //buscadorfiltro es metodo y letrafiltro es los valores del input
  buscadorfiltro = new BehaviorSubject('');
  //obtenemos las letras
  get letrafiltro(): string {
    return this.buscadorfiltro.value;
  }
  //mandamos las letras del filtro
  set letrafiltro(letrafiltro: string) {
    this.buscadorfiltro.next(letrafiltro);
  }
  filtrodatos: Tratamiento[] = [];
  renderizardatos: Tratamiento[] = [];
  constructor(
    public TratamientoDatos: TratamientoService,
    public paginadorTrata: MatPaginator,
    public ordenar: MatSort,
    public idPaciente
  ) {
    super();
    // Restablecer a la primera pagina cuando se haga un filtro
    this.buscadorfiltro.subscribe(() => (this.paginadorTrata.pageIndex = 0));
  }
  /** Función de conexión llamada por la tabla para recuperar una secuencia que contiene los datos para representar. */
  connect(): Observable<Tratamiento[]> {
    // escuchar de evento
    const displayDataChanges = [
      this.TratamientoDatos.dataChange,
      this.ordenar.sortChange,
      this.buscadorfiltro,
      this.paginadorTrata.page,
    ];
    this.TratamientoDatos.obtenerTratamiento(this.idPaciente);
    return merge(...displayDataChanges).pipe(
      map(() => {
        // metodo para filtrar datos
        this.filtrodatos = this.TratamientoDatos.data
          .slice()
          .filter((tratamientp: Tratamiento) => {
            const searchStr = (
              tratamientp.Fecha+
              tratamientp.stringplanes +
              tratamientp.stringsubtotal +
              tratamientp.TotalFinal
            ).toLowerCase();
            return searchStr.indexOf(this.letrafiltro.toLowerCase()) !== -1;
          });
        // ordenar datos filtrados
        const ordenardatos = this.metodoordenardatos(this.filtrodatos.slice());
        // metodo para order los datos y hacer las paginas
        const empezardelprincipio = this.paginadorTrata.pageIndex * this.paginadorTrata.pageSize;
        this.renderizardatos = ordenardatos.splice(
          empezardelprincipio,
          this.paginadorTrata.pageSize
        );
        return this.renderizardatos;
      })
    );
  }
  disconnect() {}
  /** Devuelve una copia ordenada de los datos de la base de datos. */
  metodoordenardatos(data: Tratamiento[]): Tratamiento[] {
    //copia todos los datos de diagnostico y los pone en una tabla para las propiedades
    if (!this.ordenar.active || this.ordenar.direction === '') {
      return data;
    }
    // metodo para poder poner la tabla en ascendente y descendente
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this.ordenar.active) {
        case 'id':
          [propertyA, propertyB] = [a.idPlanTratamiento, b.idPlanTratamiento];
          break;
        case 'fechat':
          [propertyA, propertyB] = [a.Fecha, b.Fecha];
          break;
        case 'ProcedimientoServiciot':
          [propertyA, propertyB] = [a.stringplanes, b.stringplanes];
          break;
        case 'Subtotalt':
          [propertyA, propertyB] = [a.stringsubtotal, b.stringsubtotal];
          break;
        case 'Totalt':
          [propertyA, propertyB] = [a.TotalFinal, b.TotalFinal];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this.ordenar.direction === 'asc' ? 1 : -1)
      );
    });
  }
}

// Clase de Pagos
export class PagosDatos extends DataSource<Pagos> {
  //buscadorfiltro es metodo y letrafiltro es los valores del input
  buscadorfiltro = new BehaviorSubject('');
  //obtenemos las letras
  get letrafiltro(): string {
    return this.buscadorfiltro.value;
  }
  //mandamos las letras del filtro
  set letrafiltro(letrafiltro: string) {
    this.buscadorfiltro.next(letrafiltro);
  }
  filtrodatos: Pagos[] = [];
  renderizardatos: Pagos[] = [];
  constructor(
    public pagoservice: PagosService,
    public paginadorPagos: MatPaginator,
    public ordenar: MatSort,
    public idPaciente
  ) {
    super();
    // Restablecer a la primera pagina cuando se haga un filtro
    this.buscadorfiltro.subscribe(() => (this.paginadorPagos.pageIndex = 0));
  }
  /** Función de conexión llamada por la tabla para recuperar una secuencia que contiene los datos para representar. */
  connect(): Observable<Pagos[]> {
    // escuchar de evento
    const displayDataChanges = [
      this.pagoservice.dataChange,
      this.ordenar.sortChange,
      this.buscadorfiltro,
      this.paginadorPagos.page,
    ];
    this.pagoservice.obtenerlistadodepagos(this.idPaciente)
    return merge(...displayDataChanges).pipe(
      map(() => {
        // metodo para filtrar datos
        this.filtrodatos = this.pagoservice.data
          .slice()
          .filter((pago: Pagos) => {
            const searchStr = (
              pago.idPlanTratamiento
            ).toLowerCase();
            return searchStr.indexOf(this.letrafiltro.toLowerCase()) !== -1;
          });
        // ordenar datos filtrados
        const ordenardatos = this.metodoordenardatos(this.filtrodatos.slice());
        // metodo para order los datos y hacer las paginas
        const empezardelprincipio = this.paginadorPagos.pageIndex * this.paginadorPagos.pageSize;
        this.renderizardatos = ordenardatos.splice(
          empezardelprincipio,
          this.paginadorPagos.pageSize
        );
        return this.renderizardatos;
      })
    );
  }
  disconnect() {}
  /** Devuelve una copia ordenada de los datos de la base de datos. */
  metodoordenardatos(data: Pagos[]): Pagos[] {
    //copia todos los datos de diagnostico y los pone en una tabla para las propiedades
    if (!this.ordenar.active || this.ordenar.direction === '') {
      return data;
    }
    // metodo para poder poner la tabla en ascendente y descendente
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this.ordenar.active) {
        case 'id':
          [propertyA, propertyB] = [a.idPlanTratamiento, b.idPlanTratamiento];
          break;
        case 'fechat':
          [propertyA, propertyB] = [a.Fecha, b.Fecha];
          break;
        case 'ProcedimientoServiciot':
          [propertyA, propertyB] = [a.stringplanes, b.stringplanes];
          break;
        case 'Subtotalt':
          [propertyA, propertyB] = [a.stringsubtotal, b.stringsubtotal];
          break;
        case 'Totalt':
          [propertyA, propertyB] = [a.TotalFinal, b.TotalFinal];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this.ordenar.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
// Clase de recetas
export class recetaDatos extends DataSource<Recetas> {
  //buscadorfiltro es metodo y letrafiltro es los valores del input
  buscadorfiltro = new BehaviorSubject('');
  //obtenemos las letras
  get letrafiltro(): string {
    console.log(this.buscadorfiltro.value);
    return this.buscadorfiltro.value;
  }
  //mandamos las letras del filtro
  set letrafiltro(letrafiltro: string) {
    this.buscadorfiltro.next(letrafiltro);
  }
  filtrodatos: Recetas[] = [];
  renderizardatos: Recetas[] = [];
  constructor(
    public receDatos: RecetasService,
    public paginadorReceta: MatPaginator,
    public ordenar: MatSort,
    public idPaciente
  ) {
    super();
    // Restablecer a la primera pagina cuando se haga un filtro
    this.buscadorfiltro.subscribe(() => (this.paginadorReceta.pageIndex = 0));
  }
  /** Función de conexión llamada por la tabla para recuperar una secuencia que contiene los datos para representar. */
  connect(): Observable<Recetas[]> {
    // escuchar de evento
    const displayDataChanges = [
      this.receDatos.dataChange,
      this.ordenar.sortChange,
      this.buscadorfiltro,
      this.paginadorReceta.page,
    ];
    this.receDatos.obtenerRecetas(this.idPaciente);
    return merge(...displayDataChanges).pipe(
      map(() => {
        // metodo para filtrar datos
        this.filtrodatos = this.receDatos.data
          .slice()
          .filter((receta: Recetas) => {
            const searchStr = (
              receta.Fecha +
              receta.Descripcion
            ).toLowerCase();
            return searchStr.indexOf(this.letrafiltro.toLowerCase()) !== -1;
          });
        // ordenar datos filtrados
        const ordenardatos = this.metodoordenardatos(this.filtrodatos.slice());
        // metodo para order los datos y hacer las paginas
        const empezardelprincipio = this.paginadorReceta.pageIndex * this.paginadorReceta.pageSize;
        this.renderizardatos = ordenardatos.splice(
          empezardelprincipio,
          this.paginadorReceta.pageSize
        );
        return this.renderizardatos;
      })
    );
  }
  disconnect() {}
  /** Devuelve una copia ordenada de los datos de la base de datos. */
  metodoordenardatos(data: Recetas[]): Recetas[] {
    //copia todos los datos de diagnostico y los pone en una tabla para las propiedades
    if (!this.ordenar.active || this.ordenar.direction === '') {
      return data;
    }
    // metodo para poder poner la tabla en ascendente y descendente
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this.ordenar.active) {
        case 'fechaRE':
          [propertyA, propertyB] = [a.Fecha, b.Fecha];
          break;
        case 'descRE':
          [propertyA, propertyB] = [a.Descripcion, b.Descripcion];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this.ordenar.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
// Clase de fotos
export class fotoDatos extends DataSource<FotosS> {
  //buscadorfiltro es metodo y letrafiltro es los valores del input
  buscadorfiltro = new BehaviorSubject('');
  //obtenemos las letras
  get letrafiltro(): string {
    console.log(this.buscadorfiltro.value);
    return this.buscadorfiltro.value;
  }
  //mandamos las letras del filtro
  set letrafiltro(letrafiltro: string) {
    this.buscadorfiltro.next(letrafiltro);
  }
  filtrodatos: FotosS[] = [];
  renderizardatos: FotosS[] = [];
  constructor(
    public foDatos: FotoService,
    public paginadorFotos: MatPaginator,
    public ordenar: MatSort,
    public idPaciente
  ) {
    super();
    // Restablecer a la primera pagina cuando se haga un filtro
    this.buscadorfiltro.subscribe(() => (this.paginadorFotos.pageIndex = 0));
  }
  /** Función de conexión llamada por la tabla para recuperar una secuencia que contiene los datos para representar. */
  connect(): Observable<FotosS[]> {
    // escuchar de evento
    const displayDataChanges = [
      this.foDatos.dataChange,
      this.ordenar.sortChange,
      this.buscadorfiltro,
      this.paginadorFotos.page,
    ];
    this.foDatos.obtenerFotos(this.idPaciente);
    return merge(...displayDataChanges).pipe(
      map(() => {
        // metodo para filtrar datos
        this.filtrodatos = this.foDatos.data
          .slice()
          .filter((foto: FotosS) => {
            const searchStr = (
              foto.Fecha +
              foto.Descripcion
            ).toLowerCase();
            return searchStr.indexOf(this.letrafiltro.toLowerCase()) !== -1;
          });
        // ordenar datos filtrados
        const ordenardatos = this.metodoordenardatos(this.filtrodatos.slice());
        // metodo para order los datos y hacer las paginas
        const empezardelprincipio = this.paginadorFotos.pageIndex * this.paginadorFotos.pageSize;
        this.renderizardatos = ordenardatos.splice(
          empezardelprincipio,
          this.paginadorFotos.pageSize
        );
        return this.renderizardatos;
      })
    );
  }
  disconnect() {}
  /** Devuelve una copia ordenada de los datos de la base de datos. */
  metodoordenardatos(data: FotosS[]): FotosS[] {
    //copia todos los datos de diagnostico y los pone en una tabla para las propiedades
    if (!this.ordenar.active || this.ordenar.direction === '') {
      return data;
    }
    // metodo para poder poner la tabla en ascendente y descendente
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this.ordenar.active) {
        case 'fechaF':
          [propertyA, propertyB] = [a.Fecha, b.Fecha];
          break;
        case 'descF':
          [propertyA, propertyB] = [a.Descripcion, b.Descripcion];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this.ordenar.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
