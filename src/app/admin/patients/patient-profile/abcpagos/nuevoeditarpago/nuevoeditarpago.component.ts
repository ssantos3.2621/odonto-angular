import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PagosService } from '../../pagos/pagos.service';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { Pagos } from '../../pagos/pagos.model';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-nuevoeditarpago',
  templateUrl: './nuevoeditarpago.component.html',
  styles: [
  ]
})
export class NuevoeditarpagoComponent implements OnInit {

  private baseURL = 'https://animatiomx.com/odonto/';
  accion: string;
  textodedialogo: string;
  formularioPagos: FormGroup;
  Pagos: Pagos;
  clasificaciones: any = [];
  planes: any = [];
  plan: any = [];
  pagos: any = [];
  arraypagos: any = [];
  idPaciente = '';
  totalplanes=0;
  totalpagos=0;
  totalfinal='';
  debe='';

  efectivo='';
  total='';
  importe='';
  fecha='';
  noref='';
  plani='';
  constructor(public dialogRef: MatDialogRef<NuevoeditarpagoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public PagosServicio: PagosService,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    public httpClient: HttpClient,
    private snackBar: MatSnackBar,) {
         // obtenemos la accion que va hacer
        this.accion = data.action;
        this.idPaciente = data.idPaciente;
        if (this.accion === 'editar') {
          //texto del encabezado del modal
          this.textodedialogo = data.Pagos.Nombre + ' ' +data.Pagos.Apellido;
          //le pasamos los valores de Pagos
          this.Pagos = data.Pagos;
        } else {
          this.textodedialogo = 'Nuevo Pagos';
          this.Pagos = new Pagos({});
        }

this.formularioPagos = this.fb.group({
              fecha: ['',],
              plan: ['',],
              noref: ['',],
              efectivo: ['', ],
              total: ['', ],
              importe: ['', ],
            });
     }



  ngOnInit(): void {
    this.obtenerplanes();


  }
  // trae la informacion de un plan de tratamiento
  obtenerplan(idplan){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2, 'idPlan': idplan };
    const URL: any = this.baseURL + 'pagos.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.plan = respuesta;
        console.log(this.plan);
        this.fecha=this.plan[0].Fecha;
        this.noref=idplan;
        for (let index = 0; index < this.plan.length; index++) {
          this.totalplanes+=Number(this.plan[index].TotalFinal);
        }
      });
      this.obtenerPagos(idplan);
  }
// trae la informacion de los planes de tratamiento
  obtenerplanes(){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1, 'idPaciente': this.idPaciente };
    const URL: any = this.baseURL + 'pagos.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.planes = respuesta;

      });
  }
// trae la inf de los pagos que se han realizado
  obtenerPagos(idPlan): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3, 'idPlan': idPlan };
    const URL: any = this.baseURL + 'pagos.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        console.log(respuesta);
        if (respuesta!=null) {
          this.pagos=respuesta;
          for (let index = 0; index < this.pagos.length; index++) {
            this.totalpagos+=Number(this.pagos[index].Monto);
            this.arraypagos.push({'metodo':this.pagos[index].Metodo,'total':this.pagos[index].TotalFinal,'importe':this.pagos[index].Monto,'fecha':this.pagos[index].Fecha,'idplan':this.pagos[index].idPlanTratamiento});
          }
        }else{
          this.totalpagos=0;
        }
        this.totalfinal=this.totalplanes.toFixed(2);
        this.debe=(Number(this.totalplanes) - Number(this.totalpagos)).toFixed(2);
      });
  }

  submit() {
    // emppty stuff
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
// metodo para llenar el array de pagos
  agregarpago(){
this.arraypagos.push({'metodo':this.efectivo,'total':this.total,'importe':this.importe,'fecha':this.datePipe.transform(new Date(),"yyyy-MM-dd"),'idplan':this.plani});
this.efectivo='';
this.total='';
this.importe='';
  }
// metodo para guardar el array de pago en la BD
  guardarbase(){
    console.log(this.arraypagos)
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
              const options: any = { 'caso': 4, 'arreglopagos': this.arraypagos };
              const URL: any = this.baseURL + 'pagos.php';
              this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
                respuesta => {

              });
             this.dialogRef.close();
             this.mensaje(
        'snackbar-info',
        'Se agregaron los pagos correctamente...!!!',
        'bottom',
        'center'
      );
  }

  // metodo para mostrar mensajes de alerta
  mensaje(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 3000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

}
