import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Tratamiento } from './tratamiento.model';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TratamientoService {
  //intercambio de datos
  dataChange: BehaviorSubject<Tratamiento[]> = new BehaviorSubject<Tratamiento[]>([]);
  baseURL = 'https://animatiomx.com/odonto/';
  dialogData: any;
  tratamientos: any = [];
  constructor(private httpClient: HttpClient) {}
  // poner los valores de la consulta en el dataChange
  get data(): Tratamiento[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  obtenerTratamiento(idP): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1,'idPaciente':idP};
    const URL: any = this.baseURL + 'tratamiento.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.tratamientos = respuesta;
        if (this.tratamientos !== null){
          this.dataChange.next(this.tratamientos);
          console.log(respuesta);
          console.log(this.dataChange);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }

  // DEMO ONLY, you can find working methods below
  agregartratamiento(fecha: Date,obs: String,sumato: String,totalconinpu: String,totalapagar: String,arrayser: any,textoplanes: String,textosubtotal: String,idpaciiente: Number): void {

    let fechafinal = moment(fecha).format('YYYY/MM/DD');
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2,'fecha':fechafinal,'observaciones':obs,'SumaTotal':sumato,'Totalconimpuesto':totalconinpu,
    'Totalapagar':totalapagar,'arrayservicio':arrayser,'arrayplanestexto':textoplanes,'arraysubtotaltexto':textosubtotal,'idPaciente':idpaciiente};
    const URL: any = this.baseURL + 'tratamiento.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
  actualizartratamiento(obs: String,sumato: String,totalconinpu: String,totalapagar: String,arrayser: any,textoplanes: String,textosubtotal: String,idplantratamiento: Number,arraysereliminar: any): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 5,'observaciones':obs,'SumaTotal':sumato,'Totalconimpuesto':totalconinpu,
    'Totalapagar':totalapagar,'arrayservicio':arrayser,'arrayplanestexto':textoplanes,
    'arraysubtotaltexto':textosubtotal,'idplantratamiento':idplantratamiento,'arrayservicioe':arraysereliminar};
    const URL: any = this.baseURL + 'tratamiento.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
  eliminartratamiento(id: number): void {
    console.log(id);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 4,'idplantratamiento':id  };
    const URL: any = this.baseURL + 'tratamiento.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
}
