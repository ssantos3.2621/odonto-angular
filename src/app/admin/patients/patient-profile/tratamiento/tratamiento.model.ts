export class Tratamiento {
  idPlanTratamiento: number;
  Fecha: string;
  stringplanes: string;
  Observaciones: string;
  stringsubtotal: string;
  Subtotalfinal: string;
  TotalFinal: string;
  idPaciente: string;
  fechahoy = new Date();

  constructor(tratamiento) {
    {
      this.idPlanTratamiento = tratamiento.idPlanTratamiento || this.getRandomID();
      this.Fecha = tratamiento.Fecha || this.fechahoy;
      this.stringplanes = tratamiento.stringplanes || '';
      this.Observaciones = tratamiento.Observaciones || '';
      this.stringsubtotal = tratamiento.stringsubtotal || '';
      this.Subtotalfinal = tratamiento.Subtotalfinal || '';
      this.TotalFinal = tratamiento.TotalFinal || '';
      this.idPaciente = tratamiento.idPaciente || '';
    }
  }
  public getRandomID(): string {
    var S4 = function() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
