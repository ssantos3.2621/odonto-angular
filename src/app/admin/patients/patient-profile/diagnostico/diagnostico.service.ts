import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Diagnostico } from './diagnostico.model';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DiagnosticoService {
  //intercambio de datos
  dataChange: BehaviorSubject<Diagnostico[]> = new BehaviorSubject<Diagnostico[]>([]);
  baseURL = 'https://animatiomx.com/odonto/';
  dialogData: any;
  diagnosticos: any = [];
  constructor(private httpClient: HttpClient) {}
  // poner los valores de la consulta en el dataChange
  get data(): Diagnostico[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  obtenerdiagnostico(idP): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0,'idPaciente':idP};
    const URL: any = this.baseURL + 'diagnostico.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.diagnosticos = respuesta;
        if (this.diagnosticos !== null){
          this.dataChange.next(this.diagnosticos);
          console.log(respuesta);
          console.log(this.dataChange);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }

  // DEMO ONLY, you can find working methods below
  agregardiagnostico(diagnostico: Diagnostico,idpaciiente: Number): void {
    this.dialogData = diagnostico;
    let fecha = moment(this.dialogData.fecha).format('YYYY/MM/DD');
    let clasificacion = this.dialogData.idclasificacion;
    let observacion = this.dialogData.obs;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2,'fecha':fecha,'clasificacion':clasificacion,'observaciones':observacion,'idPaciente':idpaciiente};
    const URL: any = this.baseURL + 'diagnostico.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
    //console.log(this.dialogData);
  }
  actualizardiagnostico(diagnostico: Diagnostico): void {
    this.dialogData = diagnostico;
    let iddiagnostico = this.dialogData.id;
    let clasificacion = this.dialogData.idclasificacion;
    let observacion = this.dialogData.obs;;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'clasificacion':clasificacion,'observaciones':observacion,'idDiagnosticos':iddiagnostico};
    const URL: any = this.baseURL + 'diagnostico.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
  eliminardiagnostico(id: number): void {
    console.log(id);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 4,'idDiagnosticos':id  };
    const URL: any = this.baseURL + 'diagnostico.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
}
