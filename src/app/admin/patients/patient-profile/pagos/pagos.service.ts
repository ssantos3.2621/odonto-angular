import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { Pagos } from './pagos.model';

@Injectable({
  providedIn: 'root'
})
export class PagosService {
//intercambio de datos
  dataChange: BehaviorSubject<Pagos[]> = new BehaviorSubject<Pagos[]>([]);
  baseURL = 'https://animatiomx.com/odonto/';
  dialogData: any;
  diagnosticos: any = [];
  pagos: any = [];
  constructor(private httpClient: HttpClient) {}
  // poner los valores de la consulta en el dataChange
  get data(): Pagos[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  obtenerPagos(idP): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0, 'idPaciente': idP };
    const URL: any = this.baseURL + 'pagos.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.diagnosticos = respuesta;
        if (this.diagnosticos !== null){
          this.dataChange.next(this.diagnosticos);
          console.log(respuesta);
          console.log(this.dataChange);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }


  // obtenemos el listado de nuestros pagos con la cantidad que aun debemo 

  obtenerlistadodepagos(idP){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1,'idPaciente':idP};
    const URL: any = this.baseURL + 'tratamiento.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.pagos = respuesta;
        if (this.pagos !== null){
          this.dataChange.next(this.pagos);
          console.log(respuesta);
          console.log(this.dataChange);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }
}
