export class Pagos {
  idPlanTratamiento: string;
  Fecha: string;
  stringplanes: string;
  Observaciones: string;
  stringsubtotal: string;
  Subtotalfinal: string;
  TotalFinal: string;
  idPaciente: string;
  TotalPagado: string;
  fechahoy = new Date();
    constructor(pago) {
        {
          this.idPlanTratamiento = pago.idPlanTratamiento || this.getRandomID();
          this.Fecha = pago.Fecha || this.fechahoy;
          this.stringplanes = pago.stringplanes || '';
          this.Observaciones = pago.Observaciones || '';
          this.stringsubtotal = pago.stringsubtotal || '';
          this.Subtotalfinal = pago.Subtotalfinal || '';
          this.TotalFinal = pago.TotalFinal || '';
          this.idPaciente = pago.idPaciente || '';
          this.TotalPagado = pago.TotalPagado || '';
        //   this.idDiagnostico = diagnostico.idDiagnostico || '';
        }
      }
      public getRandomID(): string {
        var S4 = function() {
          return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
      }

}
