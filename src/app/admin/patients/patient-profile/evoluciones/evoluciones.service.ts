import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { Evoluciones } from './evoluciones.model';
@Injectable({
  providedIn: 'root'
})
export class EvolucionesService {
  //intercambio de datos
  dataChange: BehaviorSubject<Evoluciones[]> = new BehaviorSubject<Evoluciones[]>([]);
  baseURL = 'https://animatiomx.com/odonto/';
  dialogData: any;
  diagnosticos: any = [];
  constructor(private httpClient: HttpClient) {}
  // poner los valores de la consulta en el dataChange
  get data(): Evoluciones[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  // Obtiene las evoluciones del paciente
  obtenerEvaluacion(idP): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 6, 'idUsuario': idP };
    const URL: any = this.baseURL + 'pacientes.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.diagnosticos = respuesta;
        if (this.diagnosticos !== null){
          this.dataChange.next(this.diagnosticos);
          console.log(respuesta);
          console.log(this.dataChange);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }
  // Agregar evolucion
  agregarEvolucion(evolucion: Evoluciones, idpaciente: number): void {
    this.dialogData = evolucion;
    let fecha = moment(this.dialogData.fecha).format('YYYY/MM/DD');
    let idDiagnostico = this.dialogData.idDia;
    let Observacion = this.dialogData.obser;
    let Descripcion = this.dialogData.descri;
    let Complicaciones = this.dialogData.compli;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 7,'fecha': fecha,'obser': Observacion,'desc': Descripcion,'compli': Complicaciones, 'idD': idDiagnostico, 'idP': idpaciente };
    const URL: any = this.baseURL + 'pacientes.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
  // Elimina evolucion
  eliminarEvolucion(id: number): void {
    console.log(id);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 9,'idE':id  };
    const URL: any = this.baseURL + 'pacientes.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
  // Modifica evolucion
  modificarEvol(evolucion: Evoluciones): void {
    this.dialogData = evolucion;
    let idDiagnostico = this.dialogData.idDia;
    let Observacion = this.dialogData.obser;
    let Descripcion = this.dialogData.descri;
    let Complicaciones = this.dialogData.compli;
    let idEvolucion = this.dialogData.idE;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 10, 'obser': Observacion, 'desc': Descripcion, 'compli': Complicaciones, 'idD': idDiagnostico,
                           'idE': idEvolucion};
    const URL: any = this.baseURL + 'pacientes.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
}
