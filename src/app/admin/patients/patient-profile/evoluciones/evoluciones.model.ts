export class Evoluciones {
    idEvoluciones: number;
    Fecha: string;
    Observacion: string;
    Descripcion: string;
    Complicaciones: string;
    idDiagnostico: number;
    idPaciente: number;
    fechahoy = new Date();

    constructor(evolucion) {
        {
          this.idEvoluciones = evolucion.idDiagnostico || this.getRandomID();
          this.Fecha = evolucion.Fecha || this.fechahoy;
          this.Observacion = evolucion.Observacion || '';
          this.Descripcion = evolucion.Descripcion || '';
          this.Complicaciones = evolucion.Complicaciones || '';
          this.idDiagnostico = evolucion.idDiagnostico || '';
          this.idPaciente = evolucion.idPaciente || '';
        }
      }
      public getRandomID(): string {
        var S4 = function() {
          return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
      }
}