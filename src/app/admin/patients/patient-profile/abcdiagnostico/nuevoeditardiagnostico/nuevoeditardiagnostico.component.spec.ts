import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoeditardiagnosticoComponent } from './nuevoeditardiagnostico.component';

describe('NuevoeditardiagnosticoComponent', () => {
  let component: NuevoeditardiagnosticoComponent;
  let fixture: ComponentFixture<NuevoeditardiagnosticoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoeditardiagnosticoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoeditardiagnosticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
