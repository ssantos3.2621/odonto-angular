import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { DiagnosticoService } from '../../diagnostico/diagnostico.service';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import { Diagnostico } from '../../diagnostico/diagnostico.model';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-nuevoeditardiagnostico',
  templateUrl: './nuevoeditardiagnostico.component.html',
  styleUrls: ['./nuevoeditardiagnostico.component.sass']
})
export class NuevoeditardiagnosticoComponent {
  private baseURL = 'https://animatiomx.com/odonto/';
  accion: string;
  textodedialogo: string;
  formulariodiagnostico: FormGroup;
  diagnostico: Diagnostico;
  clasificaciones: any = [];
  idPaciente = '';
  constructor(
    public dialogRef: MatDialogRef<NuevoeditardiagnosticoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public diagnosticoServicio: DiagnosticoService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
  ) {
        // obtenemos la accion que va hacer
        this.accion = data.action;
        this.idPaciente = data.idPaciente;
        if (this.accion === 'editar') {
          //texto del encabezado del modal
          this.textodedialogo = data.diagnostico.Nombre + ' ' +data.diagnostico.Apellido;
          //le pasamos los valores de diagnostico
          this.diagnostico = data.diagnostico;
        } else {
          this.textodedialogo = 'Nuevo diagnostico';
          this.diagnostico = new Diagnostico({});
        }
        this.formulariodiagnostico = this.formulariodediagnostico();
        this.clasificacionestodos();
  }

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }

  formulariodediagnostico(): FormGroup {
    const fecha = moment(this.diagnostico.Fecha).format('MM/DD/YYYY');
    return this.fb.group({
      id: [this.diagnostico.idDiagnostico],
      fecha: [new Date(fecha)],
      aclasif: [this.diagnostico.pertenece],
      idclasificacion: [this.diagnostico.idClasificacion],
      obs: [this.diagnostico.Observaciones],
      idpaciente: [this.diagnostico.idPacientes],
      idClasificacion: [this.diagnostico.Nombre],
      apellido: [this.diagnostico.Apellido],
    });
  }
  clasificacionestodos() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 1};
    const URL: any = this.baseURL + 'diagnostico.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.clasificaciones = respuesta;
        console.log(this.clasificaciones);
      });
  }
  submit() {
    // emppty stuff
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  public confirmAdd(): void {
    if (this.accion === 'editar') {
      this.diagnosticoServicio.actualizardiagnostico(this.formulariodiagnostico.getRawValue());
    } else {
      this.diagnosticoServicio.agregardiagnostico(this.formulariodiagnostico.getRawValue(),Number(this.idPaciente));
    }
  }
}
