import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminardiagnosticoComponent } from './eliminardiagnostico.component';

describe('EliminardiagnosticoComponent', () => {
  let component: EliminardiagnosticoComponent;
  let fixture: ComponentFixture<EliminardiagnosticoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EliminardiagnosticoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminardiagnosticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
