import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { DiagnosticoService } from '../../diagnostico/diagnostico.service';

@Component({
  selector: 'app-eliminardiagnostico',
  templateUrl: './eliminardiagnostico.component.html',
  styleUrls: ['./eliminardiagnostico.component.sass']
})
export class EliminardiagnosticoComponent {

  constructor(
    public dialogRef: MatDialogRef<EliminardiagnosticoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public diagnosticoServicio: DiagnosticoService
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    this.diagnosticoServicio.eliminardiagnostico(this.data.idDiagnostico);
  }

}
