import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import {  Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { SubirarchivoService } from '../../../subirarchivo.service';
import { AuthService } from 'src/app/shared/security/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.sass'],
})
export class AddPatientComponent {
private baseURL = 'https://animatiomx.com/odonto/';
archivos: string[] = [];
docs: any = [];
docs1: any = [];
pacientes: any = [];
pacientes1: any = [];
conveniosclinica: any = [];
conveniosclinica1: any = [];
selectedFile: null;
nombrefoto: any;
fotoFinal: any;
fileData: File = null;
foto = '';
Name = '';
Ape = '';
Rfc = '';
Convenio = '';
Notas_Convenio = ''
Dire = '';
Celu = '';
Mail = '';
Fecha = '';
Edad = '';
Grupo = '';
Gen = '';
Civil = '';
Ocu = '';
Obser = '';
FNombre = '';
Parente = '';
idDoc = '';
Img = 'user_default.png';
idH = '0';
idT = '0';
idC = '';
Nombre_E = '';
Tel_E = '';
Parentesco_E = '';
Responsable_P = '';
idClinica = '';

public respuestaImagenEnviada;
public resultadoCarga;
patientForm: FormGroup;

public filtroconvenio: FormControl = new FormControl();
private _onDestroyconvenio = new Subject<void>();

public filtrodoctor: FormControl = new FormControl();
private _onDestroydoctor = new Subject<void>();

public filtrofamiliar: FormControl = new FormControl();
private _onDestroyfamiliar = new Subject<void>();

  constructor(private router: Router,
              private enviandoImagen: SubirarchivoService,
              public http: HttpClient,
              private fb: FormBuilder,
              private authService: AuthService,
              private snackBar: MatSnackBar,
              ) {
                this.idClinica = this.authService.getIdClinica();
                if (this.idClinica === null){
                  let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
                  snack.onAction().subscribe( () => this.iraclinica());
                }
    this.patientForm = this.fb.group({
      nombre: ['', [Validators.required, Validators.pattern('[a-zA-Z]+')]],
      nombref: [''],
      apellido: ['', [Validators.required, Validators.pattern('[a-zA-Z]+')]],
      last: [''],
      gender: ['', [Validators.required]],
      mobile: [''],
      dob: ['', [Validators.required]],
      Edad: ['', [Validators.required]],
      age: [''],
      email: [
        '',
        [Validators.required, Validators.email, Validators.minLength(5)],
      ],
      maritalStatus: ['', [Validators.required]],
      address: [''],
      bGroup: [''],
      bPresure: [''],
      sugger: [''],
      injury: [''],
      uploadImg: [''],
      rfc: [''],
      parent: [''],
      convenio: [''],
      notas: [''],
      telefono: [''],
      responsable: [''],
      nombree: [''],
      telefonoe: [''],
      parentescoe: [''],
    });
  }
  onSubmit() {
    //console.log('Form Value', this.patientForm.value);
  }
  ngOnInit() {
    this.doctores();
    this.obtenerconvenios();
    this.listapacientes();
  }

  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }

  onFileChanged(event, files: FileList, fileInput: any) {
    const Numero1 = Math.floor(Math.random() * 10001).toString();
    const Numero2 = Math.floor(Math.random() * 1001).toString();
    // Nombre del archivo
    this.selectedFile = event.target.files[0];
    this.nombrefoto = event.target.files[0].name;
    this.foto = Numero1 + Numero2 + this.nombrefoto.replace(/ /g, "");
    console.log(this.foto);
    this.fotoFinal = files;
    this.fileData = <File>fileInput.target.files[0];
    this.cargandoImagen(this.fotoFinal, this.foto);
  }
  // Sube imagen a servidor
public cargandoImagen(files: FileList, nombre: string) {
  this.enviandoImagen.postFileImagen(files[0] , nombre).subscribe(
  response => {
  this.respuestaImagenEnviada = response;
  if(this.respuestaImagenEnviada <= 1) {
  console.log("Error en el servidor");
  } else {
  if (this.respuestaImagenEnviada.code == 200 && this.respuestaImagenEnviada.status == "success"){
  this.resultadoCarga = 1;
  } else {
  this.resultadoCarga = 2;
  }
  }
  },
  error => {
  console.log(<any>error);
  }

  ); // FIN DE METODO SUBSCRIBE
  }
  updatefoto(){
    if (this.foto === '')
    {
      this.foto = 'user_default.png';
    }
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 4, 'foto': this.foto, 'idUsuario': this.idC};
    const URL: any = this.baseURL + 'pacientes.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        console.log( 'Done' );
      });
  }
  crearPaciente() {
    let responsabledepago = '';
    if (this.Responsable_P === ''){
      responsabledepago = 'sin responsable de pago';
    }
    const fechaM = moment(this.Fecha).format('YYYY-MM-DD');
    console.log(fechaM);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    console.log(
      'Nombre paciente '+ this.Name+'\n '+
      'Apellido paciente '+ this.Ape+'\n '+
      'Rfc paciente '+ this.Rfc+'\n '+
      'Convenio paciente '+ this.Convenio+'\n '+
      'Notas convenio paciente '+ this.Notas_Convenio+'\n '+
      'direccion paciente '+ this.Dire+'\n '+
      'telefono paciente '+ this.Celu+'\n '+
      'correo paciente '+ this.Mail+'\n '+
      'Fecha de nacimiento paciente '+ fechaM+'\n '+
      'edad paciente '+ this.Edad+'\n '+
      'Grupo sanguineo paciente '+ this.Grupo+'\n '+
      'genero paciente '+ this.Gen+'\n '+
      'estado civil paciente '+ this.Civil+'\n '+
      'ocupacion paciente '+ this.Ocu+'\n '+
      'observacion paciente '+ this.Obser+'\n '+
      'doctor paciente '+ this.idDoc+'\n '+
      'imagen paciente '+ this.Img+'\n '+
      'responsable de pago paciente '+ responsabledepago+'\n '+'\n '+'\n '+
      'nombre familiar paciente '+ this.FNombre+'\n '+
      'parentesco paciente '+ this.Parente+'\n '+'\n '+'\n '+'\n '+
      'nombre emergencia paciente '+ this.Nombre_E+'\n '+
      'telefono emergencia paciente '+ this.Tel_E+'\n '+
      'parentesco emergencia paciente '+ this.Parentesco_E+'\n'
    )
    const options: any = {
      caso: 1,
      nom: this.Name,
      apel: this.Ape,
      RFC: this.Rfc,
      Convenio: this.Convenio, 
      NConvenio: this.Notas_Convenio,
      dir: this.Dire,
      cel: this.Celu,
      corr: this.Mail,
      fecha: fechaM,
      edad: this.Edad,
      sangre: this.Grupo,
      gen: this.Gen,
      civil: this.Civil,
      ocu: this.Ocu,
      obse: this.Obser,
      nombreF: this.FNombre,
      paren: this.Parente,
      idD: this.idDoc,
      img: this.Img,
      NomEmerg: this.Nombre_E,
      TelEmerg: this.Tel_E,
      ParentescoEmerg: this.Parentesco_E,
      Resposabledepago: responsabledepago,
      idH : this.idH,
      idT: this.idT,
      Activo: 1,
      idClinica: this.idClinica

    };
    const URL: any = this.baseURL + "pacientes.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        var res = respuesta.toString();
        if (res === "0") {
          Swal.fire(
            "El hubo un error",
            "",
            "warning"
          );
        } else {
          if (this.archivos.length > 0) {
            const idcita = respuesta.toString();
            this.idC = idcita;
            this.updatefoto();
            // this.servidorarchivo(idcita);
          } else {
            const idcita = respuesta.toString();
            this.idC = idcita;
            this.updatefoto();
            console.log(idcita + 'respsta  json');
            Swal.fire("Paciente agregado correctamente", "", "success")
              .then((value) => {
                console.log('Se inserto correctamente');
                this.router.navigate(['/admin/patients/all-patients/patient-profile/' + this.idC + '/' + this.Name + ' ' + this.Ape ]);
              });
          }
        }
      });
  }

  // Metodo para calcular edad
  calcularedad (e){
    this.Edad = String( moment().diff(this.Fecha, 'years',false)); 
  }

  // metodo para obtener los convenios
  obtenerconvenios() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 8, idclinica: this.idClinica};
    const URL: any = this.baseURL + 'convenios.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.conveniosclinica = respuesta;
        this.conveniosclinica1 = respuesta;
        if (this.conveniosclinica !== null) {
          this.filtroconvenio.valueChanges
          .pipe(takeUntil(this._onDestroyconvenio))
          .subscribe(() => {
            this.filtrobuscadorconvenio();
          });
        }
        console.log(this.conveniosclinica);
        // costruir el filtro pasandole ondestroy
      });
  }

  // metodo para pasar los valores de convenio a un nuevo arreglo
  inicializarconvenio(){
    this.conveniosclinica1 = this.conveniosclinica;
  }
  // metodo de buscador
  private filtrobuscadorconvenio() {
    this.inicializarconvenio();
    const search = this.filtroconvenio.value;
    this.conveniosclinica1 = this.conveniosclinica.filter((item) => {
        return (item.NombreConvenio.toLowerCase().indexOf(search.toLowerCase()) > -1);
      });
  }

  //obtener lista de doctores activos

  doctores() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 5,idClinica: this.idClinica};
    const URL: any = this.baseURL + 'pacientes.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.docs = respuesta;
        this.docs1 = respuesta;
        if (this.docs !== null) {
          this.filtrodoctor.valueChanges
          .pipe(takeUntil(this._onDestroydoctor))
          .subscribe(() => {
            this.filtrobuscadordoctor();
          });
        }
        console.log(this.docs);
      });
  }

  // metodo para pasar los valores de doctor a un nuevo arreglo
  inicializardoctor(){
    this.docs1 = this.docs;
  }

  // metodo de buscador
  private filtrobuscadordoctor() {
    this.inicializardoctor();
    const search = this.filtrodoctor.value;
    this.docs1 = this.docs.filter((item) => {
        return (item.NombreUsuario.toLowerCase().indexOf(search.toLowerCase()) > -1,item.Apellidos.toLowerCase().indexOf(search.toLowerCase()) > -1);
      });
  }

  //obtener lista de pacientes activos

  listapacientes() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 0,idClinica:this.idClinica};
    const URL: any = this.baseURL + 'pacientes.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.pacientes = respuesta;
        this.pacientes1 = respuesta;
        if (this.pacientes !== null) {
          this.filtrofamiliar.valueChanges
          .pipe(takeUntil(this._onDestroyconvenio))
          .subscribe(() => {
            this.filtrobuscadorfamiliar();
          });
        }
        console.log(this.docs);
      });
  }

  // metodo para pasar los valores de pacientes a un nuevo arreglo
  inicializarfamiliar(){
    this.pacientes1 = this.pacientes;
  }

  // metodo de buscador
  private filtrobuscadorfamiliar() {
    this.inicializarfamiliar();
    const search = this.filtrofamiliar.value;
    this.pacientes1 = this.pacientes.filter((item) => {
        return (item.Nombre.toLowerCase().indexOf(search.toLowerCase()) > -1, item.Apellido.toLowerCase().indexOf(search.toLowerCase()) > -1);
      });
  }

}
