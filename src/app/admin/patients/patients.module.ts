import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatRadioModule } from "@angular/material/radio";
import { MatDialogModule } from "@angular/material/dialog";
import { MatMenuModule } from "@angular/material/menu";
import { MatSortModule } from "@angular/material/sort";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MaterialFileInputModule } from "ngx-material-file-input";
import { PatientsRoutingModule } from "./patients-routing.module";
import { AddPatientComponent } from "./add-patient/add-patient.component";
import { AllpatientsComponent } from "./allpatients/allpatients.component";
import { EditPatientComponent } from "./edit-patient/edit-patient.component";
import { PatientProfileComponent } from "./patient-profile/patient-profile.component";
import { DeleteComponent } from "./allpatients/dialog/delete/delete.component";
import { FormDialogComponent } from "./allpatients/dialog/form-dialog/form-dialog.component";

import { MatBadgeModule } from "@angular/material/badge";
import { MatChipsModule } from "@angular/material/chips";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatBottomSheetModule } from "@angular/material/bottom-sheet";
import { MatListModule } from "@angular/material/list";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatCardModule } from "@angular/material/card";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatSliderModule } from "@angular/material/slider";
import { MatTabsModule } from "@angular/material/tabs";
import { MatProgressButtonsModule } from "mat-progress-buttons";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NuevoeditardiagnosticoComponent } from './patient-profile/abcdiagnostico/nuevoeditardiagnostico/nuevoeditardiagnostico.component';
import { EliminardiagnosticoComponent } from './patient-profile/abcdiagnostico/eliminardiagnostico/eliminardiagnostico.component';
import { ElimtratamientoComponent } from './patient-profile/abctratamiento/elimtratamiento/elimtratamiento.component';
import { NuevoedittratamientoComponent } from './patient-profile/abctratamiento/nuevoedittratamiento/nuevoedittratamiento.component';
import { EliminarpagoComponent } from './patient-profile/abcpagos/eliminarpago/eliminarpago.component';
import { NuevoeditarpagoComponent } from './patient-profile/abcpagos/nuevoeditarpago/nuevoeditarpago.component';
import { EliminarecetaComponent } from './patient-profile/abcrecetas/eliminareceta/eliminareceta.component';
import { NuevoeditarecetaComponent } from './patient-profile/abcrecetas/nuevoeditareceta/nuevoeditareceta.component';
import { NuevoeditevolucionComponent } from './patient-profile/abcevoluciones/nuevoeditevolucion/nuevoeditevolucion.component';
import { EliminarevolucionComponent } from './patient-profile/abcevoluciones/eliminarevolucion/eliminarevolucion.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NuevoeditafotoComponent } from './patient-profile/abcFotos/nuevoeditafoto/nuevoeditafoto.component';
import { AbcpresupuestosComponent } from './patient-profile/abcpresupuestos/abcpresupuestos.component';
import { AbcdocumentosComponent } from './patient-profile/abcdocumentos/abcdocumentos.component';


@NgModule({
  declarations: [
    AddPatientComponent,
    AllpatientsComponent,
    EditPatientComponent,
    PatientProfileComponent,
    DeleteComponent,
    FormDialogComponent,
    NuevoeditardiagnosticoComponent,
    EliminardiagnosticoComponent,
    ElimtratamientoComponent,
    NuevoedittratamientoComponent,
    EliminarpagoComponent,
    NuevoeditarpagoComponent,
    EliminarecetaComponent,
    NuevoeditarecetaComponent,
    NuevoeditevolucionComponent,
    EliminarevolucionComponent,
    NuevoeditafotoComponent,
    AbcpresupuestosComponent,
    AbcdocumentosComponent,
  ],
  providers: [DatePipe],
  imports: [
    CommonModule,
    PatientsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    NgxMatSelectSearchModule,
    MatSnackBarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatSelectModule,
    MatRadioModule,
    MatMenuModule,
    MatCheckboxModule,
    MaterialFileInputModule,
    MatBadgeModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatBottomSheetModule,
    MatListModule,
    MatSidenavModule,
    MatExpansionModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSliderModule,
    MatTabsModule,
    NgbModule,
    MatProgressButtonsModule,
  ],
})
export class PatientsModule {}
