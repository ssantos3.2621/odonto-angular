export class Motivo {
    idMotivo: number;
    NombreMotivo: string;
    Tiempo: number;
    MostrarAgenda: string;
    idClinica: number;
  
    constructor(sucursal) {
      {
        this.idMotivo = sucursal.idMotivo || this.getRandomID();
        this.NombreMotivo = sucursal.NombreMotivo || '';
        this.Tiempo= sucursal.Tiempo || '';
        this.MostrarAgenda = sucursal.MostrarAgenda || '';
        this.idClinica = sucursal.idClinica || '';
      }
    }
    public getRandomID(): string {
      var S4 = function() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      };
      return S4() + S4();
    }
  }
  