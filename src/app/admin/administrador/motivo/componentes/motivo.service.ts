import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { Motivo } from './motivo.model';


@Injectable({
  providedIn: 'root'
})
export class MotivoService {
  //intercambio de datos
  dataChange: BehaviorSubject<Motivo[]> = new BehaviorSubject<Motivo[]>([]);
  baseURL = 'https://animatiomx.com/odonto/';
  motivoData: any;
  motivo: any = [];
  dialogData: any;

  constructor(private httpClient: HttpClient) {}
  // poner los valores de la consulta en el dataChange
  get data(): Motivo[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */

  // obtener en un arreglo motivos
  obtenermotivos(idC): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0,'idClinica': idC};
    const URL: any = this.baseURL + 'motivos.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.motivo = respuesta;
        if (this.motivo !== null){
          this.dataChange.next(this.motivo);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }

  //agregar motivo
  agregarmotivo(motivo: Motivo,idclinica: Number) {
    
    this.dialogData = motivo;

    let nommotivo = this.dialogData.NombreMotivo;
    let tiempo = this.dialogData.Tiempo;
    let mostrar = this.dialogData.MostrarAgenda;
    let mostraagenda = '';

    if (mostrar === true){
        mostraagenda = 'SI';
    }else{
        mostraagenda = 'NO';
    }

    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1,'Nombredemotivo':nommotivo,'Tiempo':tiempo,'Mostrarenagenda':mostraagenda,'idClinica':idclinica};
    const URL: any = this.baseURL + 'motivos.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);

  }
   //actualizar informacion de motivo
  actualizarmotivo(motivo: Motivo) {
    this.dialogData = motivo;

    let nommotivo = this.dialogData.NombreMotivo;
    let tiempo = this.dialogData.Tiempo;
    let mostrar = this.dialogData.MostrarAgenda;
    let idmotivo = this.dialogData.id;
    let mostraagenda = '';

    if (mostrar === true){
        mostraagenda = 'SI';
    }else{
        mostraagenda = 'NO';
    }
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2,'Nombredemotivo':nommotivo,'Tiempo':tiempo,'Mostrarenagenda':mostraagenda,'idMotivo':idmotivo};
    const URL: any = this.baseURL + 'motivos.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);
    
  }

  //eliminar motivo
  eliminarmotivo(id: number) {
    console.log(id);

    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'idMotivo':id  };
    const URL: any = this.baseURL + 'motivos.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);
    
  }

 }
