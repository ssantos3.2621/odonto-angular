import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { MotivoService } from '../componentes/motivo.service';
import { Motivo } from '../componentes/motivo.model';
import { NuevoyeditarmotivoComponent } from './abcmotivo/nuevoyeditarmotivo/nuevoyeditarmotivo.component';
import { EliminarmotivoComponent } from './abcmotivo/eliminarmotivo/eliminarmotivo.component';
import { AuthService } from 'src/app/shared/security/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-listamotivo',
  templateUrl: './listamotivo.component.html',
  styleUrls: ['./listadomotivo.component.scss']
})
export class ListamotivoComponent implements OnInit {
columnamotivo = [
    'motivo',
    'tiempo',
    'mostrar',
    'actions',
  ];
  motivoDB: MotivoService | null;
  datosmotivo: motivoDatos | null;
  selection = new SelectionModel<Motivo>(true, []);
  index: number;
  idmotivo: number;
  idClinica = '';

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public motivoService: MotivoService,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router,
  ) {
    this.idClinica = this.authService.getIdClinica();
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  ngOnInit() {
    if (this.idClinica !== null){
      this.cargandodatosmotivos();
    }else{
      this.cargandodatosmotivos();
      let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
      snack.onAction().subscribe( () => this.iraclinica());
    }
  }
  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }
  refrescartablamotivo() {
    this.cargandodatosmotivos();
  }
  private refrescartablademotivo() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  public cargandodatosmotivos() {
    this.motivoDB = new MotivoService(this.httpClient);
    this.datosmotivo = new motivoDatos(
      this.motivoDB,
      this.paginator,
      this.sort,
      this.idClinica
    );
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.datosmotivo) {
          return;
        }
        this.datosmotivo.filter = this.filter.nativeElement.value;
      });
  }
  nuevoMotivo() {
    const dialogRef = this.dialog.open(NuevoyeditarmotivoComponent, {
      width: '30%',
      data: {
        action: 'nuevo',
        idClinica: 1
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.motivoDB.dataChange.value.unshift(
          //this.diagnosticoBasededatos.getDialogData()
        );
        this.refrescartablamotivo();
        this.showNotification(
          'snackbar-success',
          'Motivo guardado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  editarMotivo(row) {
    this.idmotivo = row.idMotivo;
    console.log(this.idmotivo);
    const dialogRef = this.dialog.open(NuevoyeditarmotivoComponent, {
      width: '30%',
      data: {
        motivo: row,
        action: 'editar',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.motivoDB.dataChange.value.findIndex(
          (x) => x.idMotivo === this.idmotivo
        );
        this.refrescartablamotivo();
        this.refrescartablademotivo();
        this.showNotification(
          'black',
          'Motivo editado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  eliminarMotivo(i: number, row) {
    this.index = i;
    this.idmotivo = row.idMotivo;
    console.log(this.idmotivo);
    const dialogRef = this.dialog.open(EliminarmotivoComponent, {
      data: row,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.motivoDB.dataChange.value.findIndex(
          (x) => x.idMotivo === this.idmotivo
        );
        // for delete we use splice in order to remove single object from DataService
        this.motivoDB.dataChange.value.splice(foundIndex, 1);
        this.refrescartablamotivo();
        this.refrescartablademotivo();
        this.showNotification(
          'snackbar-danger',
          'Motivo eliminado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}

export class motivoDatos extends DataSource<Motivo> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  filteredData: Motivo[] = [];
  renderedData: Motivo[] = [];
  constructor(
    public _exampleDatabase: MotivoService,
    public _paginator: MatPaginator,
    public _sort: MatSort,
    public idclinica,
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Motivo[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page,
    ];
    this._exampleDatabase.obtenermotivos(this.idclinica);
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._exampleDatabase.data
          .slice()
          .filter((motivo: Motivo) => {
            const searchStr = (
              motivo.NombreMotivo +
              motivo.Tiempo +
              motivo.MostrarAgenda
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {}
  /** Returns a sorted copy of the database data. */
  sortData(data: Motivo[]): Motivo[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.idMotivo, b.idMotivo];
          break;
        case 'name':
          [propertyA, propertyB] = [a.NombreMotivo, b.NombreMotivo];
          break;
        case 'email':
          [propertyA, propertyB] = [a.Tiempo, b.Tiempo];
          break;
        case 'date':
          [propertyA, propertyB] = [a.MostrarAgenda, b.MostrarAgenda];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}