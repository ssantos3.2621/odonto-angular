import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Listamotivo } from './listamotivo.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListamotivoService {
private readonly API_URL = 'assets/data/staff.json';
  dataChange: BehaviorSubject<Listamotivo[]> = new BehaviorSubject<Listamotivo[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private httpClient: HttpClient) {}
  get data(): Listamotivo[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllListamotivos(): void {
    this.httpClient.get<Listamotivo[]>(this.API_URL).subscribe(
      data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
    );
  }
  // DEMO ONLY, you can find working methods below
  addListamotivo(Listamotivo: Listamotivo): void {
    this.dialogData = Listamotivo;
  }
  updateListamotivo(Listamotivo: Listamotivo): void {
    this.dialogData = Listamotivo;
  }
  deleteListamotivo(id: number): void {
    console.log(id);
  }
}
