import { formatDate } from '@angular/common';
export class Listamotivo {
    id: number;
  img: string;
  name: string;
  email: string;
  date: string;
  address: string;
  mobile: string;
  designation: string;
  constructor(Listamotivo) {
    {
      this.id = Listamotivo.id || this.getRandomID();
      this.img = Listamotivo.avatar || 'assets/images/user/user1.jpg';
      this.name = Listamotivo.name || '';
      this.designation = Listamotivo.designation || '';
      this.email = Listamotivo.email || '';
      this.date = formatDate(new Date(), 'yyyy-MM-dd', 'en') || '';
      this.address = Listamotivo.address || '';
      this.mobile = Listamotivo.mobile || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
