import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { MotivoService } from '../../../componentes/motivo.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-eliminarmotivo',
  templateUrl: './eliminarmotivo.component.html',
  styleUrls: ['./eliminarmotivo.component.sass']
})
export class EliminarmotivoComponent {

  constructor(
    public dialogRef: MatDialogRef<EliminarmotivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public motivoServicio: MotivoService,
    private snackBar: MatSnackBar,
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    this.motivoServicio.eliminarmotivo(this.data.idMotivo).subscribe(respuesta => {
      const res = respuesta;
      if (res.toString() === 'Se elimino'){
        this.dialogRef.close(1);
      }else {
        this.showNotification(
          'snackbar-error',
          'Ocurrio un error, intente de nuevo por favor...!!!',
          'bottom',
          'center'
        );
      }
    });;
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

}

