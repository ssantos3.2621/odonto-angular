import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarmotivoComponent } from './eliminarmotivo.component';

describe('EliminarmotivoComponent', () => {
  let component: EliminarmotivoComponent;
  let fixture: ComponentFixture<EliminarmotivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EliminarmotivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarmotivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
