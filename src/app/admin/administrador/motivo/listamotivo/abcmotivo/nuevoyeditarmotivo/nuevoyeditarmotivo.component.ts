import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MotivoService } from '../../../componentes/motivo.service';
import { Motivo } from '../../../componentes/motivo.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/shared/security/auth.service';


@Component({
  selector: 'app-nuevoyeditarmotivo',
  templateUrl: './nuevoyeditarmotivo.component.html',
  styleUrls: ['./nuevoyeditarmotivo.component.sass']
})

export class NuevoyeditarmotivoComponent {
  private baseURL = 'https://animatiomx.com/odonto/';
  accion: string;
  textodedialogo: string;
  formulariomotivo: FormGroup;
  motivo: Motivo;
  clasificaciones: any = [];
  idClinica = '';
  constructor(
    public dialogRef: MatDialogRef<NuevoyeditarmotivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public motivoServicio: MotivoService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) {
        // obtenemos la accion que va hacer
        this.accion = data.action;
        this.idClinica = data.idClinica;
        if (this.accion === 'editar') {
          //texto del encabezado del modal
          this.textodedialogo = 'Editar motivo' + ' ( ' +data.motivo.NombreMotivo + ' )';
          //le pasamos los valores de diagnostico
          this.motivo = data.motivo;
        } else {
          this.idClinica = this.authService.getIdClinica();
          this.textodedialogo = 'Nuevo motivo de consulta';
          this.motivo = new Motivo({});
        }
        this.formulariomotivo = this.formulariodemotivo();
  }

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }

  // metodo para llena el formulario

  formulariodemotivo(): FormGroup {
    let MostrarAgenda;
    let mostrarA = this.motivo.MostrarAgenda
    if (mostrarA === 'SI'){
      MostrarAgenda = true;
    }else{
      MostrarAgenda= false;
    }
    return this.fb.group({
      id: [this.motivo.idMotivo],
      NombreMotivo: [this.motivo.NombreMotivo],
      Tiempo: [this.motivo.Tiempo],
      MostrarAgenda: [MostrarAgenda]
    });
  }

  submit() {
    // emppty stuff
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  // metodo para registrar en base de datos la informacion
  public confirmAdd(): void {
    if (this.accion === 'editar') {
      console.log(this.formulariomotivo.getRawValue());
      this.motivoServicio.actualizarmotivo(this.formulariomotivo.getRawValue()).subscribe(respuesta => {
        const res = respuesta;
        if (res.toString() === 'Se modifico'){
          this.dialogRef.close(1);
        }else {
          this.showNotification(
            'snackbar-error',
            'Ocurrio un error, intente de nuevo por favor...!!!',
            'bottom',
            'center'
          );
        }
      });
    } else {
      this.motivoServicio.agregarmotivo(this.formulariomotivo.getRawValue(),Number(this.idClinica)).subscribe(respuesta => {
        const res = respuesta;
        if (res.toString() === 'Se inserto'){
          this.dialogRef.close(1);
        }else {
          this.showNotification(
            'snackbar-error',
            'Ocurrio un error, intente de nuevo por favor...!!!',
            'bottom',
            'center'
          );
        }
      });
    }
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

}
