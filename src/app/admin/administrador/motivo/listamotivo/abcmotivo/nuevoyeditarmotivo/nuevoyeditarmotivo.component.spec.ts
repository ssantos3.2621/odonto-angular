import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoyeditarmotivoComponent } from './nuevoyeditarmotivo.component';

describe('NuevoyeditarmotivoComponent', () => {
  let component: NuevoyeditarmotivoComponent;
  let fixture: ComponentFixture<NuevoyeditarmotivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoyeditarmotivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoyeditarmotivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
