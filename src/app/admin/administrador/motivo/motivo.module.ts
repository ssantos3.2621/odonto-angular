import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MotivoRoutingModule } from './motivo-routing.module';
import { NuevomotivComponent } from './nuevomotiv/nuevomotiv.component';
import { ListamotivoComponent } from './listamotivo/listamotivo.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSortModule } from "@angular/material/sort";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { NuevoyeditarmotivoComponent } from './listamotivo/abcmotivo/nuevoyeditarmotivo/nuevoyeditarmotivo.component';
import { EliminarmotivoComponent } from './listamotivo/abcmotivo/eliminarmotivo/eliminarmotivo.component';

@NgModule({
  declarations: [NuevomotivComponent, ListamotivoComponent, NuevoyeditarmotivoComponent, EliminarmotivoComponent,],
  imports: [
    CommonModule,
    MotivoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSelectModule,
    MatDatepickerModule,
  ]
})
export class MotivoModule { }
