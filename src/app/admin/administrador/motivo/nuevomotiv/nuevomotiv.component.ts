import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/security/auth.service';
import { Motivo } from '../componentes/motivo.model';
import { MotivoService } from '../componentes/motivo.service';

@Component({
  selector: 'app-nuevomotiv',
  templateUrl: './nuevomotiv.component.html',
  styles: [
  ]
})
export class NuevomotivComponent implements OnInit {

  motivo: Motivo;

  formulariomotivo: FormGroup;
  idClinica = '';

  constructor(
    public motivoServicio: MotivoService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router
    ) {
      this.idClinica = this.authService.getIdClinica();
      if (this.idClinica === null){
        let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
        snack.onAction().subscribe( () => this.iraclinica());
      }
      this.motivo = new Motivo({});
      this.formulariomotivo = this.formulariodemotivo();
  }

  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }

  //llenar el formulario

  formulariodemotivo(): FormGroup {
    let MostrarAgenda;
    let mostrarA = this.motivo.MostrarAgenda
    if (mostrarA === 'SI'){
      MostrarAgenda = true;
    }else{
      MostrarAgenda= false;
    }
    return this.fb.group({
      id: [this.motivo.idMotivo],
      NombreMotivo: [this.motivo.NombreMotivo,[Validators.required]],
      Tiempo: [this.motivo.Tiempo,[Validators.required]],
      MostrarAgenda: [MostrarAgenda]
    });
  }

  ngOnInit(){
  }


  //metod para guardar la informacion en base de datos

  onSubmit() {

    this.motivoServicio.agregarmotivo(this.formulariomotivo.getRawValue(),Number(this.idClinica)).subscribe(respuesta => {
      const res = respuesta;
      if (res.toString() === 'Se inserto'){
        this.showNotification(
          'snackbar-success',
          'Motivo guardado correctamente...!!!',
          'bottom',
          'center'
        );
        this.limpiarvariables();
      }else {
        this.showNotification(
          'snackbar-error',
          'Ocurrio un error, intente de nuevo por favor...!!!',
          'bottom',
          'center'
        );
      }
    });
    
  }

  //metodo para indicar un toas de la respuesta de peticiones

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  //metodo para limpiarformulario

  limpiarvariables(){
    this.formulariomotivo.reset();
  }

}
