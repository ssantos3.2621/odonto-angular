import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListamotivoComponent } from './listamotivo/listamotivo.component';
import { NuevomotivComponent } from './nuevomotiv/nuevomotiv.component';

const routes: Routes = [
  {
    path: 'nuevo-motivo',
    component: NuevomotivComponent,
  },
  {
    path: 'listado-motivo',
    component: ListamotivoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotivoRoutingModule { }
