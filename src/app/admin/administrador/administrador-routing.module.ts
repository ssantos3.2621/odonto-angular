import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'usuarios',
    loadChildren: () => import('./usuarios/usuarios.module').then((m) => m.UsuariosModule),
  },
  {
    path: 'convenios',
    loadChildren: () => import('./convenios/convenio.module').then((m) => m.ConvenioModule),
  },
  {
    path: 'motivo',
    loadChildren: () => import('./motivo/motivo.module').then((m) => m.MotivoModule),
  },
  {
    path: 'categoria',
    loadChildren: () => import('./categoria/categoria.module').then((m) => m.CategoriaModule),
  },
  {
    path: 'servicios',
    loadChildren: () => import('./servicios/servicios.module').then((m) => m.ServiciosModule),
  },
  {
    path: 'documentos',
    loadChildren: () => import('./documentos/documentos.module').then((m) => m.DocumentosModule),
  },
  {
    path: 'plantilla',
    loadChildren: () => import('./plantilla/plantilla.module').then((m) => m.PlantillaModule),
  },
  {
    path: 'reportes',
    loadChildren: () => import('./reportes/reportes.module').then((m) => m.ReportesModule),
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministradorRoutingModule { }
