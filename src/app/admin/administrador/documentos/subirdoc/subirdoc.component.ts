import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-subirdoc',
  templateUrl: './subirdoc.component.html',
  styles: [
  ]
})
export class SubirdocComponent implements OnInit {
  roomForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.roomForm = this.fb.group({
      rNo: ['', [Validators.required]],
      rType: ['', [Validators.required]],
      pName: ['', [Validators.required]],
      aDate: ['', [Validators.required]],
      dDate: ['', [Validators.required]],
      email: ['', [Validators.required]],
      namecrto: ['', [Validators.required]],
      logo: ['', [Validators.required]]
    });
  }
  onSubmit() {
    console.log('Form Value', this.roomForm.value);
  }
  ngOnInit(): void {
  }

}
