import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubirdocComponent } from './subirdoc/subirdoc.component';

const routes: Routes = [
  {
    path: 'subir-documento',
    component: SubirdocComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentosRoutingModule { }
