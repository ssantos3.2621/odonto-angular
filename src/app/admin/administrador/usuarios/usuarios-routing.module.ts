import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page404Component } from 'src/app/authentication/page404/page404.component';
import { InactivosComponent } from './inactivos/inactivos.component';
import { ListadoComponent } from './listado/listado.component';
import { NuevoComponent } from './nuevo/nuevo.component';

const routes: Routes = [
  {
    path: 'Nuevo_Usuario',
    component: NuevoComponent,
  },
  {
    path: 'Usuarios_Inactivos',
    component: InactivosComponent,
  },
  {
    path: 'Listado-Usuarios',
    component: ListadoComponent,
  },
  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
