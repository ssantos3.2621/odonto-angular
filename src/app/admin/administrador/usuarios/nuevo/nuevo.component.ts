import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SubirarchivoService } from '../../../../subirarchivo.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogNuevoComponent } from './dialogNew/dialog-nuevo/dialog-nuevo.component';
import * as moment from 'moment';
@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styles: [
  ]
})
export class NuevoComponent implements OnInit {
  private baseURL = 'https://animatiomx.com/odonto/';
  sucursales: any = [];
  Fecha = '';
  Grupo = '';
  Civil = '';
  selectedFile: null;
  nombrefoto: any;
  foto = '';
  fotoFinal: any;
  fileData: File = null;

  nombres = '';
  apellidos = '';
  perfil = '';
  sucursal = '';
  rfc = '';
  direccion = '';
  celular = '';
  email = '';
  fechanaci = '';
  edad = '';
  gruposanguineo = '';
  genero = '';
  estadocivil = '';
  titulo = '';
  especialidad = '';
  cedula = '';
  imagen = 'user_default.png';
  public respuestaImagenEnviada;
  public resultadoCarga;
  roomForm: FormGroup;
  constructor(public dialog: MatDialog,
              private enviandoImagen: SubirarchivoService, private httpClient: HttpClient, private fb: FormBuilder) {
    this.roomForm = this.fb.group({
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      perfil: ['', [Validators.required]],
      sucursal: ['', [Validators.required]],
      agenda: ['', [Validators.required]],
      rfc: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      celular: ['', [Validators.required]],
      email: ['', [Validators.required]],
      fechanaci: ['', [Validators.required]],
      edad: ['', [Validators.required]],
      gruposanguineo: ['', [Validators.required]],
      genero: ['', [Validators.required]],
      estadocivil: ['', [Validators.required]],
      titulo: ['', [Validators.required]],
      especialidad: ['', [Validators.required]],
      cedula: ['', [Validators.required]],
      imagen: ['', [Validators.required]],
    });
  }
  onSubmit() {
    console.log('Form Value', this.roomForm.value);
  }

  ngOnInit(){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0, idClinica: 1};
    const URL: any = this.baseURL + 'sucursal.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.sucursales = respuesta;
        console.log(this.sucursales);
      });
  }
  onFileChanged(event, files: FileList, fileInput: any) {
    const Numero1 = Math.floor(Math.random() * 10001).toString();
    const Numero2 = Math.floor(Math.random() * 1001).toString();
    // Nombre del archivo
    this.selectedFile = event.target.files[0];
    this.nombrefoto = event.target.files[0].name;
    this.foto = Numero1 + Numero2 + this.nombrefoto.replace(/ /g, "");
    console.log(this.foto);
    this.fotoFinal = files;
    this.fileData = <File>fileInput.target.files[0];
    this.cargandoImagen(this.fotoFinal, this.foto);
  }
  public cargandoImagen(files: FileList, nombre: string) {
    this.enviandoImagen.postFileImagen(files[0] , nombre).subscribe(
    response => {
    this.respuestaImagenEnviada = response;
    if(this.respuestaImagenEnviada <= 1) {
    console.log("Error en el servidor");
    } else {
    if (this.respuestaImagenEnviada.code == 200 && this.respuestaImagenEnviada.status == "success"){
    this.resultadoCarga = 1;
    } else {
    this.resultadoCarga = 2;
    }
    }
    },
    error => {
    console.log(<any>error);
    }

    ); // FIN DE METODO SUBSCRIBE
    }
    
    addNew() {
      const dialogRef = this.dialog.open(DialogNuevoComponent, {
        data: {
          nombreUS: this.nombres,
          apell: this.apellidos,
          rol: this.perfil,
          sucu: this.sucursal,
          rf: this.rfc,
          direc: this.direccion,
          cel: this.celular,
          mail: this.email,
          fecha: moment(this.fechanaci).format("YYYY-MM-DD"),
          age: this.edad,
          grup: this.gruposanguineo,
          gener: this.genero,
          estado: this.estadocivil,
          titu: this.titulo,
          especi: this.especialidad,
          dec: this.cedula,
          img: this.imagen,
          action: 'add'
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result === 1) {
          // After dialog is closed we're doing frontend updates
          // For add we're just pushing a new row inside DataServicex
          // this.exampleDatabase.dataChange.value.unshift(
          //   this.doctorsService.getDialogData()
          // );
          // this.refreshTable();
          // this.showNotification(
          //   'snackbar-success',
          //   'Add Record Successfully...!!!',
          //   'bottom',
          //   'center'
          // );
        }
      });
    }
}
