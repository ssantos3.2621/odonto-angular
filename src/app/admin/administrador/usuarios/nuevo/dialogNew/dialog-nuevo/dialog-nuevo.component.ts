import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-dialog-nuevo',
  templateUrl: './dialog-nuevo.component.html',
  styleUrls: ['./dialog-nuevo.component.css']
})
export class DialogNuevoComponent implements OnInit {
  baseURL = 'https://animatiomx.com/odonto/';
  roomForm: FormGroup;
  lunes = 'NULL';
  martes = 'NULL';
  miercoles = 'NULL';
  jueves = 'NULL';
  viernes = 'NULL';
  sabado = 'NULL';
  domingo = 'NULL';
  horaIL = 'NULL';
  horaFL = 'NULL';
  horaIM = 'NULL';
  horaFM = 'NULL';
  horaIMi = 'NULL';
  horaFMi = 'NULL';
  horaIJ = 'NULL';
  horaFJ = 'NULL';
  horaIV = 'NULL';
  horaFV = 'NULL';
  horaIS = 'NULL';
  horaFS = 'NULL';
  horaID = 'NULL';
  horaFD = 'NULL';
  diaGeneral = '0';
  horaIGeneral = '';
  horaFGeneral = '';
  datosInsert: any = [];
  datosInsertF: any = [];

  nombreU = '';
  apellidos = '';
  perfil = '';
  sucursal = '';
  rfc = '';
  direccion = '';
  celular = '';
  email = '';
  fechanaci = '';
  edad = '';
  gruposanguineo = '';
  genero = '';
  estadocivil = '';
  titulo = '';
  especialidad = '';
  cedula = '';
  imagen = '';
  constructor(public dialogRef: MatDialogRef<DialogNuevoComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private router: Router,
              private httpClient: HttpClient,
              private fb: FormBuilder,
  ) {
    this.nombreU = data.nombreUS;
    this.apellidos = data.apell;
    this.perfil = data.rol;
    this.sucursal = data.sucu;
    this.rfc = data.rf;
    this.direccion = data.direc;
    this.celular = data.cel;
    this.email = data.mail;
    this.fechanaci = data.fecha;
    this.edad = data.age;
    this.gruposanguineo = data.grup;
    this.genero = data.gener;
    this.estadocivil = data.estado;
    this.titulo = data.titu;
    this.especialidad = data.especi;
    this.cedula = data.dec;
    this.imagen = data.img;
  }

  ngOnInit(): void {
    console.log(this.nombreU);
  }
  agregar()
  {
    for (let index = 0; index <= this.datosInsert.length; index++) {
      const element = this.datosInsert[index];
      if (this.diaGeneral === '1') {
        this.lunes = '1';
        this.horaIL = this.horaIGeneral;
        this.horaFL = this.horaFGeneral;
        this.datosInsertF.push(this.lunes, this.horaIGeneral, this.horaFGeneral);
      }
      if (this.diaGeneral === '2') {
        this.martes = '2';
        this.horaIM = this.horaIGeneral;
        this.horaFM = this.horaFGeneral;
        this.datosInsertF.push(this.martes, this.horaIGeneral, this.horaFGeneral);
      }
      if (this.diaGeneral === '3') {
        this.miercoles = '3';
        this.horaIMi = this.horaIGeneral;
        this.horaFMi = this.horaFGeneral;
        this.datosInsertF.push(this.miercoles, this.horaIGeneral, this.horaFGeneral);
      }
      if (this.diaGeneral === '4') {
        this.jueves = '4';
        this.horaIJ = this.horaIGeneral;
        this.horaFJ = this.horaFGeneral;
        this.datosInsertF.push(this.jueves, this.horaIGeneral, this.horaFGeneral);
      }
      if (this.diaGeneral === '5') {
        this.viernes = '5';
        this.horaIV = this.horaIGeneral;
        this.horaFV = this.horaFGeneral;
        this.datosInsertF.push(this.viernes, this.horaIGeneral, this.horaFGeneral);
      }
      if (this.diaGeneral === '6') {
        this.sabado = '6';
        this.horaIS = this.horaIGeneral;
        this.horaFS = this.horaFGeneral;
        this.datosInsertF.push(this.sabado, this.horaIGeneral, this.horaFGeneral);
      }
      if (this.diaGeneral === '7') {
        this.domingo = '7';
        this.horaID = this.horaIGeneral;
        this.horaFD = this.horaFGeneral;
        this.datosInsertF.push(this.domingo, this.horaIGeneral, this.horaFGeneral);
      }
    }
    console.log(this.datosInsertF);
  }

  agregarUsuario(): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3, 'horario': this.datosInsertF, 'NomU': this.nombreU, 'Ape': this.apellidos, 'TypeU': this.perfil, 'sucu': this.sucursal, 'rfc': this.rfc
    , 'dire': this.direccion, 'cel': this.celular, 'mail': this.email, 'fnac': this.fechanaci, 'edad': this.edad, 'sangre': this.gruposanguineo, 'sexo': this.genero
    , 'ecivil': this.estadocivil, 'titulo': this.titulo, 'especi': this.especialidad, 'cedu': this.cedula, 'img': this.imagen};
    const URL: any = this.baseURL + 'horarios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
    Swal.fire("Agregado correctamente", "", "success")
      .then((value) => {
        console.log('Se insertó correctamente');
        this.router.navigate(['/admin/administrador/usuarios/Listado-Usuarios' ]);
      });
    this.onNoClick();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
