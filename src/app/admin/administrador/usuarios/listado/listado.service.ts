import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Listado } from './listado.model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListadoService {
  baseURL = 'https://animatiomx.com/odonto/';
  users: any = [];
private readonly API_URL = 'assets/data/staff.json';
  dataChange: BehaviorSubject<Listado[]> = new BehaviorSubject<Listado[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private httpClient: HttpClient) {}
  get data(): Listado[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllListados(): void {
    this.httpClient.get<Listado[]>(this.API_URL).subscribe(
      data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
    );
  }
  // DEMO ONLY, you can find working methods below
  addListado(Listado: Listado): void {
    this.dialogData = Listado;
  }
  updateListado(Listado: Listado): void {
    this.dialogData = Listado;
  }
  deleteListado(id: number): void {
    console.log(id);
  }

  obtenerUsuarios(): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 12};
    const URL: any = this.baseURL + 'usuarios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.users = respuesta;
        this.dataChange.next(this.users);
        console.log(respuesta);
        console.log(this.dataChange);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }
  desactivaUser(id: number): void {
    console.log(id);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 11,'idUsuario':id, 'idActivo': 1};
    const URL: any = this.baseURL + 'usuarios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
}
