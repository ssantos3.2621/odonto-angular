import { formatDate } from '@angular/common';
export class Listado {
  idUsuario: number;
  NombreUsuario: string;
  Correo: string;
  Contraseña: string;
  Imagen: string;
  constructor(Listado) {
    {
      this.idUsuario = Listado.idUsuario || this.getRandomID();
      this.NombreUsuario = Listado.NombreUsuario || '';
      this.Correo = Listado.Correo || '';
      this.Contraseña = Listado.Contraseña || '';
      this.Imagen = Listado.Imagen || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
