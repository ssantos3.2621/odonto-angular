import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Inactivos } from './inactivos.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InactivosService {
private readonly API_URL = 'assets/data/staff.json';
  dataChange: BehaviorSubject<Inactivos[]> = new BehaviorSubject<Inactivos[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private httpClient: HttpClient) {}
  get data(): Inactivos[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllInactivoss(): void {
    this.httpClient.get<Inactivos[]>(this.API_URL).subscribe(
      data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
    );
  }
  // DEMO ONLY, you can find working methods below
  addInactivos(Inactivos: Inactivos): void {
    this.dialogData = Inactivos;
  }
  updateInactivos(Inactivos: Inactivos): void {
    this.dialogData = Inactivos;
  }
  deleteInactivos(id: number): void {
    console.log(id);
  }
}
