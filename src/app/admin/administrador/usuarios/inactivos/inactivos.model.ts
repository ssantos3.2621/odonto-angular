import { formatDate } from '@angular/common';
export class Inactivos {
    id: number;
  img: string;
  name: string;
  email: string;
  date: string;
  address: string;
  mobile: string;
  designation: string;
  constructor(Inactivos) {
    {
      this.id = Inactivos.id || this.getRandomID();
      this.img = Inactivos.avatar || 'assets/images/user/user1.jpg';
      this.name = Inactivos.name || '';
      this.designation = Inactivos.designation || '';
      this.email = Inactivos.email || '';
      this.date = formatDate(new Date(), 'yyyy-MM-dd', 'en') || '';
      this.address = Inactivos.address || '';
      this.mobile = Inactivos.mobile || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
