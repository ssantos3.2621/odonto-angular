import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportesRoutingModule } from './reportes-routing.module';

import { ResumenreporteComponent } from './ResumendeResporte/resumenreporte/resumenreporte.component';
import { ListapacientantendComponent } from './ReportesPaciente/listapacientantend/listapacientantend.component';
import { AtendidoprimeravezComponent } from './ReportesPaciente/atendidoprimeravez/atendidoprimeravez.component';
import { AtendidoprimeravezabnoComponent } from './ReportesPaciente/atendidoprimeravezabno/atendidoprimeravezabno.component';
import { ConcitanoatendidoComponent } from './ReportesPaciente/concitanoatendido/concitanoatendido.component';
import { NoatendidofechaComponent } from './ReportesPaciente/noatendidofecha/noatendidofecha.component';
import { NuncatendidoComponent } from './ReportesPaciente/nuncatendido/nuncatendido.component';
import { TratanoiniciadoComponent } from './ReportesPaciente/tratanoiniciado/tratanoiniciado.component';
import { NoiniciadofechaComponent } from './ReportesPaciente/noiniciadofecha/noiniciadofecha.component';
import { PacientemorosoComponent } from './ReportesPaciente/pacientemoroso/pacientemoroso.component';
import { MorosofechComponent } from './ReportesPaciente/morosofech/morosofech.component';
import { CuentacorrienteComponent } from './ReportesPaciente/cuentacorriente/cuentacorriente.component';
import { UltimavezComponent } from './ReportesPaciente/ultimavez/ultimavez.component';
import { SinhistorialComponent } from './ReportesPaciente/sinhistorial/sinhistorial.component';
import { UltiagendaComponent } from './ReportesPaciente/ultiagenda/ultiagenda.component';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSortModule } from "@angular/material/sort";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { ListadopacienteComponent } from './ReportesPaciente/listadopaciente/listadopaciente.component';
import { AgendvsatendComponent } from './ReportesAdministrativos/agendvsatend/agendvsatend.component';
import { EstadocitasComponent } from './ReportesAdministrativos/estadocitas/estadocitas.component';
import { InfRecaudacionComponent } from './ReportesAdministrativos/inf-recaudacion/inf-recaudacion.component';
import { MotivoconsultaComponent } from './ReportesAdministrativos/motivoconsulta/motivoconsulta.component';
import { PresupemitidoComponent } from './ReportesAdministrativos/presupemitido/presupemitido.component';
import { PresupvsrealizaComponent } from './ReportesAdministrativos/presupvsrealiza/presupvsrealiza.component';


@NgModule({
  declarations: [ResumenreporteComponent, ListapacientantendComponent, AtendidoprimeravezComponent, AtendidoprimeravezabnoComponent, ConcitanoatendidoComponent, NoatendidofechaComponent, NuncatendidoComponent, TratanoiniciadoComponent, NoiniciadofechaComponent, PacientemorosoComponent, MorosofechComponent, CuentacorrienteComponent, UltimavezComponent, SinhistorialComponent, UltiagendaComponent,ListadopacienteComponent,AgendvsatendComponent,EstadocitasComponent,InfRecaudacionComponent,MotivoconsultaComponent,PresupemitidoComponent,PresupvsrealizaComponent],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSelectModule,
    MatDatepickerModule,
  ]
})
export class ReportesModule { }
