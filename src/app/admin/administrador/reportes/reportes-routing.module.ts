import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgendvsatendComponent } from './ReportesAdministrativos/agendvsatend/agendvsatend.component';
import { EstadocitasComponent } from './ReportesAdministrativos/estadocitas/estadocitas.component';
import { InfRecaudacionComponent } from './ReportesAdministrativos/inf-recaudacion/inf-recaudacion.component';
import { MotivoconsultaComponent } from './ReportesAdministrativos/motivoconsulta/motivoconsulta.component';
import { PresupemitidoComponent } from './ReportesAdministrativos/presupemitido/presupemitido.component';
import { PresupvsrealizaComponent } from './ReportesAdministrativos/presupvsrealiza/presupvsrealiza.component';
import { AtendidoprimeravezComponent } from './ReportesPaciente/atendidoprimeravez/atendidoprimeravez.component';
import { AtendidoprimeravezabnoComponent } from './ReportesPaciente/atendidoprimeravezabno/atendidoprimeravezabno.component';
import { ConcitanoatendidoComponent } from './ReportesPaciente/concitanoatendido/concitanoatendido.component';
import { CuentacorrienteComponent } from './ReportesPaciente/cuentacorriente/cuentacorriente.component';
import { ListadopacienteComponent } from './ReportesPaciente/listadopaciente/listadopaciente.component';
import { ListapacientantendComponent } from './ReportesPaciente/listapacientantend/listapacientantend.component';
import { NoatendidofechaComponent } from './ReportesPaciente/noatendidofecha/noatendidofecha.component';
import { NoiniciadofechaComponent } from './ReportesPaciente/noiniciadofecha/noiniciadofecha.component';
import { NuncatendidoComponent } from './ReportesPaciente/nuncatendido/nuncatendido.component';
import { PacientemorosoComponent } from './ReportesPaciente/pacientemoroso/pacientemoroso.component';
import { SinhistorialComponent } from './ReportesPaciente/sinhistorial/sinhistorial.component';
import { TratanoiniciadoComponent } from './ReportesPaciente/tratanoiniciado/tratanoiniciado.component';
import { UltiagendaComponent } from './ReportesPaciente/ultiagenda/ultiagenda.component';
import { UltimavezComponent } from './ReportesPaciente/ultimavez/ultimavez.component';
import { ResumenreporteComponent } from './ResumendeResporte/resumenreporte/resumenreporte.component';

const routes: Routes = [
  {
    path: 'Grafica-recaudado',
    component: ResumenreporteComponent,
  },
  //URL Reportes Administrativos
  {
    path: 'Informe-recaudacion',
    component: InfRecaudacionComponent,
  },
  {
    path: 'Agendadas-vs-atendidas',
    component: AgendvsatendComponent,
  },
  {
    path: 'Estado-citas',
    component: EstadocitasComponent,
  },
  {
    path: 'Motivos-consultas',
    component: MotivoconsultaComponent,
  },
  {
    path: 'Presupuestos-emitidos',
    component: PresupemitidoComponent,
  },
  {
    path: 'Presupuestadas-vs-realizadas',
    component: PresupvsrealizaComponent,
  },
  //URL Reportes Pacientes
  {
    path: 'Listado-pacientes',
    component: ListadopacienteComponent,
  },
  {
    path: 'Paciente-atendido',
    component: ListapacientantendComponent,
  },
  {
    path: 'Paciente-atendido-primera-vez',
    component: AtendidoprimeravezComponent,
  },
  {
    path: 'Paciente-atendido-primera-vez-abono',
    component: AtendidoprimeravezabnoComponent,
  },
  {
    path: 'Con-cita-no-atendido',
    component: ConcitanoatendidoComponent,
  },
  {
    path: 'No-atendido-fecha',
    component: NoatendidofechaComponent,
  },
  {
    path: 'Nunca-atendido',
    component: NuncatendidoComponent,
  },
  {
    path: 'Sin-tratamiento-iniciado',
    component: TratanoiniciadoComponent,
  },
  {
    path: 'Sin-tratamiento-iniciado-fecha',
    component: NoiniciadofechaComponent,
  },
  {
    path: 'Paciente-morosos',
    component: PacientemorosoComponent,
  },
  {
    path: 'Cuenta-corriente',
    component: CuentacorrienteComponent,
  },
  {
    path: 'Atendidos-ultima-vez',
    component: UltimavezComponent,
  },
  {
    path: 'Atendidos-sin-historial',
    component: SinhistorialComponent,
  },
  {
    path: 'Ultimos-agendados',
    component: UltiagendaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
