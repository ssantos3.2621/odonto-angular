import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Listaplantilla } from './listaplantilla.model';
import { HttpClient,HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
@Injectable({
  providedIn: 'root'
})
export class ListaplantillaService {
private baseURL = 'https://animatiomx.com/odonto/';
  dataChange: BehaviorSubject<Listaplantilla[]> = new BehaviorSubject<Listaplantilla[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  categorias:any=[];
  constructor(private httpClient: HttpClient,private snackBar: MatSnackBar) {}
  get data(): Listaplantilla[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  //obtiene los datos del listado de categorias desde la BD
  getAllListaplantillas(): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 4};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      data => {
        this.categorias=data;
        this.dataChange.next(this.categorias);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }
  addListaplantilla(Listaplantilla: Listaplantilla): void {
    this.dialogData = Listaplantilla;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 6,'plantilla':Listaplantilla};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       console.log(respuesta);
        this.showNotification(
        'snackbar-success',
        'plantilla guardada correctamente',
        'bottom',
        'center'
        );
    });
  }
  //actualizada los nuevos datos en la BD
  updateListaplantilla(Listaplantilla: Listaplantilla,id): void {
    this.dialogData = Listaplantilla;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 5,'plantilla':Listaplantilla,'idplantilla':id};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       console.log('Actualizado correctamente');
      });
  }
  deleteListaplantilla(id: number): void {
    console.log(id);
  }
   showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}
