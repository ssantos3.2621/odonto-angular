import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ListaplantillaService } from './listaplantilla.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Listaplantilla } from './listaplantilla.model';
import { DataSource } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { ModalplantillaComponent } from "./modalplantilla/modalplantilla.component";

@Component({
  selector: 'app-listaplantilla',
  templateUrl: './listaplantilla.component.html',
  styles: [
  ]
})
export class ListaplantillaComponent implements OnInit {
displayedColumns = [
    'select',
    'id',
    'titulo',
    'notas',
    'actions',
  ];
  CategoriDatabase: ListaplantillaService | null;
  dataSource: ExampleDataSource | null;
  selection = new SelectionModel<Listaplantilla>(true, []);
  index: number;
  id: number;
  Listaplantilla: Listaplantilla | null;
  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public ListaplantillaService: ListaplantillaService,
    private snackBar: MatSnackBar
  ) {}
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  ngOnInit() {
    this.loadData();
  }
  refresh() {
    this.loadData();
  }
  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
          this.selection.select(row)
        );
  }
  removeSelectedRows() {
    const totalSelect = this.selection.selected.length;
    this.selection.selected.forEach((item) => {
      const index: number = this.dataSource.renderedData.findIndex(
        (d) => d === item
      );
      // console.log(this.dataSource.renderedData.findIndex((d) => d === item));
      this.CategoriDatabase.dataChange.value.splice(index, 1);
      this.refreshTable();
      this.selection = new SelectionModel<Listaplantilla>(true, []);
    });
    this.showNotification(
      'snackbar-danger',
      totalSelect + ' Record Delete Successfully...!!!',
      'bottom',
      'center'
    );
  }
  //carga los datos desde el servicio
  public loadData() {
    this.CategoriDatabase = new ListaplantillaService(this.httpClient,this.snackBar);
    this.dataSource = new ExampleDataSource(
      this.CategoriDatabase,
      this.paginator,
      this.sort
    );
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
  //llama el modal y envia los datos para agregar la plantilla
  NuevaPlantilla() {
    const dialogRef = this.dialog.open(ModalplantillaComponent, {
      data: {
        Plantilla: this.Listaplantilla,
        action: 'agregar',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.CategoriDatabase.dataChange.value.unshift(
          this.ListaplantillaService.getDialogData()
        );
        this.refreshTable();
        this.showNotification(
          'snackbar-success',
          'Plantilla agragada Correctamente!',
          'bottom',
          'center'
        );
      }
    });
  }
//llama el modal y envia los datos para editar la plantilla
  editarPlantilla(row) {
    const dialogRef = this.dialog.open(ModalplantillaComponent, {
      width: '60%',
      data: {
        Plantilla: row,
        action: 'editar',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.CategoriDatabase.dataChange.value.findIndex(
          (x) => x.idPlantilla === this.id
        );
        this.loadData();
        this.showNotification(
          'black',
          'Plantilla editada correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
}

export class ExampleDataSource extends DataSource<Listaplantilla> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  filteredData: Listaplantilla[] = [];
  renderedData: Listaplantilla[] = [];
  constructor(
    public CategoriDatabase: ListaplantillaService,
    public _paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Listaplantilla[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this.CategoriDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page,
    ];
    this.CategoriDatabase.getAllListaplantillas();
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this.CategoriDatabase.data
          .slice()
          .filter((Listaplantilla: Listaplantilla) => {
            const searchStr = (
              Listaplantilla.Titulo +
              Listaplantilla.Notas
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {}
  /** Returns a sorted copy of the database data. */
  sortData(data: Listaplantilla[]): Listaplantilla[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'idPlantilla':
          [propertyA, propertyB] = [a.idPlantilla, b.idPlantilla];
          break;
        case 'Titulo':
          [propertyA, propertyB] = [a.Titulo, b.Titulo];
          break;
          case 'Notas':
          [propertyA, propertyB] = [a.Notas, b.Notas];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}