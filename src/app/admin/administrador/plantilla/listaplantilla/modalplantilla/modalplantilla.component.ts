import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ListaplantillaService } from '../listaplantilla.service';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { Listaplantilla } from '../listaplantilla.model';


@Component({
  selector: 'app-modalplantilla',
  templateUrl: './modalplantilla.component.html',
  styles: [
  ]
})
export class ModalplantillaComponent implements OnInit {
action: string;
  dialogTitle: string;
  PlantillaForm: FormGroup;
  Plantilla: Listaplantilla;
  constructor(public dialogRef: MatDialogRef<ModalplantillaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public ListaplantillaService: ListaplantillaService,
    private fb: FormBuilder
  ) {
    // Set the defaults
    this.action = data.action;
    if (this.action === 'editar') {
      this.dialogTitle = data.Plantilla.Titulo;
      this.Plantilla = data.Plantilla;
    } else {
      this.dialogTitle = 'Nueva Plantilla';
      this.Plantilla = new Listaplantilla({});
    }
    this.PlantillaForm = this.createContactForm();
  }
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }
  createContactForm(): FormGroup {
    return this.fb.group({
      id: [this.Plantilla.idPlantilla],
      Titulo: [this.Plantilla.Titulo],
      Notas: [this.Plantilla.Notas],
    });
  }
  submit() {
    console.log(this.PlantillaForm.getRawValue(),this.Plantilla.idPlantilla);
    this.ListaplantillaService.updateListaplantilla(this.PlantillaForm.getRawValue(),this.Plantilla.idPlantilla);
  }
  nuevaplantilla() {
    console.log(this.PlantillaForm.value);
    this.ListaplantillaService.addListaplantilla(this.PlantillaForm.value);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }
}
