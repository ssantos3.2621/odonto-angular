export class Listaplantilla {
    idPlantilla: number;
    Titulo: string;
    Notas: string;
    constructor(Listplantilla) {
    {
      this.idPlantilla = Listplantilla.idPlantilla || this.getRandomID();
      this.Titulo = Listplantilla.Titulo || '';
      this.Notas = Listplantilla.Notas || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
