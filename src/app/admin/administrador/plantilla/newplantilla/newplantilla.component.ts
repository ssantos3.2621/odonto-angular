import { Component, OnInit,ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormGroupDirective } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient,HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-newplantilla',
  templateUrl: './newplantilla.component.html',
  styles: [
  ]
})
export class NewplantillaComponent implements OnInit {
  private baseURL = 'https://animatiomx.com/odonto/';
@ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  PlantillaForm: FormGroup;
  constructor(private fb: FormBuilder,private httpClient:HttpClient,private snackBar: MatSnackBar) {
    this.PlantillaForm = this.fb.group({
      Nombre: ['', [Validators.required]],
      Nota: ['', [Validators.required]],
    });
  }
  onSubmit() {
    console.log('Form Value', this.PlantillaForm.value);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'plantilla':this.PlantillaForm.value};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       console.log(respuesta);
       this.showNotification(
      'snackbar-success',
      'plantilla guardada correctamente',
      'bottom',
      'center'
    );
    setTimeout(() => this.formGroupDirective.resetForm(), 0);
      });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
  ngOnInit(): void {
  }
}
