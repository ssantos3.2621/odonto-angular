import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlantillaRoutingModule } from './plantilla-routing.module';
import { NewplantillaComponent } from './newplantilla/newplantilla.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSortModule } from "@angular/material/sort";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { ListaplantillaComponent } from './listaplantilla/listaplantilla.component';
import { ModalplantillaComponent } from './listaplantilla/modalplantilla/modalplantilla.component';


@NgModule({
  declarations: [NewplantillaComponent, ListaplantillaComponent, ModalplantillaComponent],
  imports: [
    CommonModule,
    PlantillaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSelectModule,
    MatDatepickerModule,
  ]
})
export class PlantillaModule { }
