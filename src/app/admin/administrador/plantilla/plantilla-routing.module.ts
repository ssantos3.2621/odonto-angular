import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaplantillaComponent } from './listaplantilla/listaplantilla.component';
import { NewplantillaComponent } from './newplantilla/newplantilla.component';

const routes: Routes = [
  {
    path: 'nueva-plantilla',
    component: NewplantillaComponent,
  },
  {
    path: 'listado-plantilla',
    component: ListaplantillaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlantillaRoutingModule { }
