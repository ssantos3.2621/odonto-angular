import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ListacategoService } from '../../listacatego.service';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { Listacatego } from '../../listacatego.model';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-modalcatego',
  templateUrl: './modalcatego.component.html',
  styles: [
  ]
})
export class ModalcategoComponent implements OnInit {
  private baseURL = 'https://animatiomx.com/odonto/';
action: string;
  dialogTitle: string;
  CategoriaForm: FormGroup;
  Categoria: Listacatego;
  clinicas:any=[];
  constructor(private httpClient:HttpClient,public dialogRef: MatDialogRef<ModalcategoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public ListacategoService: ListacategoService,
    private fb: FormBuilder
  ) {
    // Set the defaults
    this.action = data.action;
    if (this.action === 'editar') {
      this.dialogTitle = data.Categoria.Nombre;
      this.Categoria = data.Categoria;
    } else {
      this.dialogTitle = 'Nueva Categoria';
      this.Categoria = new Listacatego({});
    }
    this.CategoriaForm = this.createContactForm();
  }
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }
  createContactForm(): FormGroup {
    return this.fb.group({
      id: [this.Categoria.idCategorias],
      idClinica:[this.Categoria.idClinica],
      Nombre: [this.Categoria.Nombre],
    });
  }
  submit() {
    console.log(this.CategoriaForm.getRawValue(),this.Categoria.idCategorias);
    this.ListacategoService.updateListacatego(this.CategoriaForm.getRawValue(),this.Categoria.idCategorias);
  }
  NuevaCategoria() {
    console.log(this.CategoriaForm.value);
    this.ListacategoService.addListacatego(this.CategoriaForm.value);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2.1};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.clinicas=respuesta;
        console.log(this.clinicas);
      });
  }

}
