import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ListacategoService } from './listacatego.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Listacatego } from './listacatego.model';
import { DataSource } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { ModalcategoComponent } from "./modalcatego/modalcatego/modalcatego.component";

@Component({
  selector: 'app-listacatego',
  templateUrl: './listacatego.component.html',
  styles: [
  ]
})
export class ListacategoComponent implements OnInit {
  private baseURL = 'https://animatiomx.com/odonto/';
displayedColumns = [
    'select',
    'name',
    'actions',
  ];
  CategoriDatabase: ListacategoService | null;
  dataSource: ExampleDataSource | null;
  selection = new SelectionModel<Listacatego>(true, []);
  index: number;
  id: number;
  Listacatego: Listacatego | null;
  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public ListacategoService: ListacategoService,
    private snackBar: MatSnackBar
  ) {}
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  ngOnInit() {
    this.loadData();
  }
  refresh() {
    this.loadData();
  }
  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
          this.selection.select(row)
        );
  }
  removeSelectedRows() {
    const totalSelect = this.selection.selected.length;
    this.selection.selected.forEach((item) => {
      const index: number = this.dataSource.renderedData.findIndex(
        (d) => d === item
      );
      // console.log(this.dataSource.renderedData.findIndex((d) => d === item));
      this.CategoriDatabase.dataChange.value.splice(index, 1);
      this.refreshTable();
      this.selection = new SelectionModel<Listacatego>(true, []);
    });
    this.showNotification(
      'snackbar-danger',
      totalSelect + ' Record Delete Successfully...!!!',
      'bottom',
      'center'
    );
  }
  //carga los datos desde el servicio
  public loadData() {
    this.CategoriDatabase = new ListacategoService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.CategoriDatabase,
      this.paginator,
      this.sort
    );
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
    //llama el modal y envia los datos para agregar la plantilla
  NuevaCategoria() {
    const dialogRef = this.dialog.open(ModalcategoComponent, {
      width: '60%',
      data: {
        Categoria: this.Listacatego,
        action: 'agregar',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.CategoriDatabase.dataChange.value.unshift(
          this.ListacategoService.getDialogData()
        );
        this.refreshTable();
        this.showNotification(
          'snackbar-success',
          'Categoria agragada Correctamente!',
          'bottom',
          'center'
        );
      }
    });
  }
//llama el modal y envia los datos para editar la categoria
  editarCategoria(row) {
    const dialogRef = this.dialog.open(ModalcategoComponent, {
      width: '60%',
      data: {
        Categoria: row,
        action: 'editar',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.CategoriDatabase.dataChange.value.findIndex(
          (x) => x.idCategorias === this.id
        );
        this.loadData();
        this.showNotification(
          'black',
          'Categoria editada correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }

  desactivar(id){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2.2,'idcategoria':id};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.loadData();
       this.showNotification(
          'black',
          'Categoria desactivada correctamente!',
          'bottom',
          'center'
        );
      });
  }
  activar(id){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2.3,'idcategoria':id};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.loadData();
       this.showNotification(
          'black',
          'Categoria activada correctamente!',
          'bottom',
          'center'
        );
      });
  }

}

export class ExampleDataSource extends DataSource<Listacatego> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  filteredData: Listacatego[] = [];
  renderedData: Listacatego[] = [];
  constructor(
    public CategoriDatabase: ListacategoService,
    public _paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Listacatego[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this.CategoriDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page,
    ];
    this.CategoriDatabase.getAllListacategos();
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this.CategoriDatabase.data
          .slice()
          .filter((Listacatego: Listacatego) => {
            const searchStr = (
              Listacatego.Nombre
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {}
  /** Returns a sorted copy of the database data. */
  sortData(data: Listacatego[]): Listacatego[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'idCategorias':
          [propertyA, propertyB] = [a.idCategorias, b.idCategorias];
          break;
        case 'Nombre':
          [propertyA, propertyB] = [a.Nombre, b.Nombre];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}