export class Listacatego {
  idCategorias: number;
  idClinica:number;
  Nombre: string;
  constructor(Listacatego) {
    {
      this.idCategorias = Listacatego.idCategorias || this.getRandomID();
      this.idClinica = Listacatego.idClinica || this.getRandomID();
      this.Nombre = Listacatego.Nombre || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
