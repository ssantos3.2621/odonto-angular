import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Listacatego } from './listacatego.model';
import { HttpClient,HttpErrorResponse,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListacategoService {
private baseURL = 'https://animatiomx.com/odonto/';
  dataChange: BehaviorSubject<Listacatego[]> = new BehaviorSubject<Listacatego[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  categorias:any=[];
  constructor(private httpClient: HttpClient) {}
  get data(): Listacatego[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  //obtiene los datos del listado de categorias desde la BD
  getAllListacategos(): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      data => {
        this.categorias=data;
        this.dataChange.next(this.categorias);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }
  addListacatego(Listacatego: Listacatego): void {
    this.dialogData = Listacatego;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0,'categoria':Listacatego};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       console.log("Agregado correctamente", respuesta);
      });
  }
  //actualizada los nuevos datos en la BD
  updateListacatego(Listacatego: Listacatego,id): void {
    this.dialogData = Listacatego;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2,'categoria':Listacatego,'idcategoria':id};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       console.log('Actualizado correctamente');
      });
  }
  deleteListacatego(id: number): void {
    console.log(id);
  }
}