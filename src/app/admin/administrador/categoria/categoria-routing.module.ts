import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListacategoComponent } from './listacatego/listacatego.component';
import { NewcategoComponent } from './newcatego/newcatego.component';

const routes: Routes = [
  {
    path: 'nueva-categoria',
    component: NewcategoComponent,
  },
  {
    path: 'listado-categoria',
    component: ListacategoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriaRoutingModule { }
