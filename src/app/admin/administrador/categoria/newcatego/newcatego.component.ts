import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-newcatego',
  templateUrl: './newcatego.component.html',
  styles: [
  ]
})
export class NewcategoComponent implements OnInit {
private baseURL = 'https://animatiomx.com/odonto/';
@ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  CategoriaForm: FormGroup;
  formState: any = null;
  clinicas:any=[];
  constructor(private fb: FormBuilder,private httpClient:HttpClient,private snackBar: MatSnackBar) {
    this.CategoriaForm = this.fb.group({
      Nombre: ['', [Validators.required]],
      idClinica: ['', [Validators.required]],
      });
  }
  onSubmit() {
    console.log('Form Value', this.CategoriaForm.value);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0,'categoria':this.CategoriaForm.value};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       console.log(respuesta);
       this.showNotification(
      'snackbar-success',
      'Categoria guardada correctamente',
      'bottom',
      'center'
    );
    setTimeout(() => this.formGroupDirective.resetForm(), 0);
      });
  }
  ngOnInit(): void {
     const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2.1};
    const URL: any = this.baseURL + 'AdminCategorias.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.clinicas=respuesta;
        console.log(this.clinicas);
      });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}
