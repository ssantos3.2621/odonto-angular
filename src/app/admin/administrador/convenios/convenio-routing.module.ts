import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaconvComponent } from './listaconv/listaconv.component';
import { NuevoconvComponent } from './nuevoconv/nuevoconv.component';

const routes: Routes = [
  {
    path: 'nuevo-convenio',
    component: NuevoconvComponent,
  },
  {
    path: 'listado-convenio',
    component: ListaconvComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConvenioRoutingModule { }
