import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators,FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/security/auth.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-nuevoconv',
  templateUrl: './nuevoconv.component.html',
  styles: [
  ]
})
export class NuevoconvComponent implements OnInit {
private baseURL = 'https://animatiomx.com/odonto/';
@ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
ConvForm: FormGroup;
idClinica = '';
public filtrodoctor: FormControl = new FormControl();
private _onDestroydoctor = new Subject<void>();
docs: any = [];
docs1: any = [];

  constructor(
    private fb: FormBuilder,
    public httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router
    ) {
    this.idClinica = this.authService.getIdClinica();
    if (this.idClinica === null){
      let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
      snack.onAction().subscribe( () => this.iraclinica());
    }
    this.ConvForm = this.fb.group({
      nombre: ['', [Validators.required]],
      descuento: ['', [Validators.required]],
      nombreDoc: ['', [Validators.required]],
      notas: ['']
    });
  }

  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }

  onSubmit() {
    console.log('Form Value', this.ConvForm.value);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1,'conv':this.ConvForm.value, 'idclinica': this.idClinica};
    const URL: any = this.baseURL + 'convenios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       this.showNotification(
      'snackbar-success',
      'Convenio guardado correctamente',
      'bottom',
      'center'
    );
    setTimeout(() => this.formGroupDirective.resetForm(), 0);
      });
  }

  ngOnInit() {
    this.doctores();
  }

  //obtener lista de doctores activos

  doctores() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 5,idClinica: this.idClinica};
    const URL: any = this.baseURL + 'pacientes.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.docs = respuesta;
        this.docs1 = respuesta;
        if (this.docs !== null) {
          this.filtrodoctor.valueChanges
          .pipe(takeUntil(this._onDestroydoctor))
          .subscribe(() => {
            this.filtrobuscadordoctor();
          });
        }
        console.log(this.docs);
      });
  }

  // metodo para pasar los valores de doctor a un nuevo arreglo
  inicializardoctor(){
    this.docs1 = this.docs;
  }

  // metodo de buscador
  private filtrobuscadordoctor() {
    this.inicializardoctor();
    const search = this.filtrodoctor.value;
    this.docs1 = this.docs.filter((item) => {
        return (item.NombreUsuario.toLowerCase().indexOf(search.toLowerCase()) > -1,item.Apellidos.toLowerCase().indexOf(search.toLowerCase()) > -1);
      });
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

}
