import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Listaconv } from './listaconv.model';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListaconvService {
private baseURL = 'https://animatiomx.com/odonto/';
  dataChange: BehaviorSubject<Listaconv[]> = new BehaviorSubject<Listaconv[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  dataconv: any = [];
  constructor(private httpClient: HttpClient) {}
  get data(): Listaconv[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllListaconvs(idClinica): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2, 'idclinica': idClinica};
    const URL: any = this.baseURL + 'convenios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.dataconv=respuesta;
        if (this.dataconv !== null){
          this.dataChange.next(this.dataconv);
        }
      });
  }
  // DEMO ONLY, you can find working methods below
  addListaconv(Listaconv: Listaconv): void {
    this.dialogData = Listaconv;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 5,'conv':Listaconv};
    const URL: any = this.baseURL + 'convenios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        console.log(respuesta);
      });
  }
  //envia los convenios modificados a la BD
  updateListaconv(Listaconv: Listaconv,id): void {
    this.dialogData = Listaconv;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'conv':Listaconv,'idconv':id};
    const URL: any = this.baseURL + 'convenios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
       console.log('actualizado correctamente');
      });
  }
  deleteListaconv(id: number): void {
    console.log(id);
  }
}
