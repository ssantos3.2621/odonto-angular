import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ListaconvService } from './listaconv.service';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Listaconv } from './listaconv.model';
import { DataSource } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { FormDialogComponent } from 'src/app/admin/administrador/convenios/listaconv/form-dialog/form-dialog.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/security/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listaconv',
  templateUrl: './listaconv.component.html',
  styles: [
  ]
})
export class ListaconvComponent implements OnInit {
  private baseURL = 'https://animatiomx.com/odonto/';
displayedColumns = [
    'NombreConvenio',
    'Descuento',
    'Notas',
    'FechaCreacion',
    'FechaMod',
    'Nombre',
    'Apellido',
    'actions',
  ];
  exampleDatabase: ListaconvService | null;
  dataSource: ExampleDataSource | null;
  selection = new SelectionModel<Listaconv>(true, []);
  index: number;
  id: number;
  Historial:boolean=false;
  Desactivar:boolean=false;
  HistorialConv:any=[];
  Listaconv: Listaconv | null;
  MotivoForm: FormGroup;
  idDesactivar:number;
  idClinica = '';

  constructor(
    private fb: FormBuilder,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public ListaconvService: ListaconvService,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router
  ) {
    this.idClinica = this.authService.getIdClinica();
    if (this.idClinica === null){
      let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
      snack.onAction().subscribe( () => this.iraclinica());
    }
    this.MotivoForm = this.fb.group({
      motivo: ['', [Validators.required]],
      });
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  ngOnInit() {
    this.loadData();
  }
  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }
  refresh() {
    this.loadData();
  }
  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.renderedData.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.renderedData.forEach((row) =>
          this.selection.select(row)
        );
  }
  removeSelectedRows() {
    const totalSelect = this.selection.selected.length;
    this.selection.selected.forEach((item) => {
      const index: number = this.dataSource.renderedData.findIndex(
        (d) => d === item
      );
      // console.log(this.dataSource.renderedData.findIndex((d) => d === item));
      this.exampleDatabase.dataChange.value.splice(index, 1);
      this.refreshTable();
      this.selection = new SelectionModel<Listaconv>(true, []);
    });
    this.showNotification(
      'snackbar-danger',
      totalSelect + ' Record Delete Successfully...!!!',
      'bottom',
      'center'
    );
  }
  //llama al servicio que a su vez obtiene los datos del listado de convenios
  public loadData() {
    this.exampleDatabase = new ListaconvService(this.httpClient);
    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort,
      this.idClinica
    );
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
  //llama el modal y envia los datos para agregar la plantilla
  NuevoConvenio() {
    const dialogRef = this.dialog.open(FormDialogComponent, {
      data: {
        room: this.Listaconv,
        action: 'nuevo',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.exampleDatabase.dataChange.value.unshift(
          this.ListaconvService.getDialogData()
        );
        this.refreshTable();
        this.showNotification(
          'snackbar-success',
          'Add Record Successfully...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
//llama el modal del formulario para editl el covenio
   editCall(row) {
    this.id = row.id;
    const dialogRef = this.dialog.open(FormDialogComponent, {
      data: {
        room: row,
        action: 'edit',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside DataService by id
        const foundIndex = this.exampleDatabase.dataChange.value.findIndex(
          (x) => x.id === this.id
        );
        // Then you update that record using data from dialogData (values you enetered)
        this.exampleDatabase.dataChange.value[
          foundIndex
        ] = this.ListaconvService.getDialogData();
        // And lastly refresh table
        this.loadData();
        this.showNotification(
          'black',
          'Edit Record Successfully...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
//Abre el modal del historial de convenios
  ModalHistorial(open : boolean) : void {
    this.Historial = open;
}
//Abre el modal del historial de convenios
  ModalMotivoDesctivar(open : boolean,id) : void {
    this.Desactivar = open;
    this.idDesactivar = id;
}
//Obtiene el historial de conveniosdesde la BD
obtenerHistorial(id){
  console.log(id);
  const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
  const options: any = { 'caso': 4,'idconv':id};
  const URL: any = this.baseURL + 'convenios.php';
  this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
    respuesta => {
      this.HistorialConv=respuesta;
      this.ModalHistorial(true);
    });
}

desactivar(id){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 6,'idconv':id,'conv':this.MotivoForm.value};
    const URL: any = this.baseURL + 'convenios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.loadData();
       this.showNotification(
          'black',
          'Convenio desactivado correctamente!',
          'bottom',
          'center'
        );
        this.ModalMotivoDesctivar(false,0)
      });
  }
activar(id){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 7,'idconv':id};
    const URL: any = this.baseURL + 'convenios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.loadData();
       this.showNotification(
          'black',
          'Convenio activado correctamente!',
          'bottom',
          'center'
        );
      });
  }

}

export class ExampleDataSource extends DataSource<Listaconv> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  filteredData: Listaconv[] = [];
  renderedData: Listaconv[] = [];
  constructor(
    public _exampleDatabase: ListaconvService,
    public _paginator: MatPaginator,
    public _sort: MatSort,
    public idClinica
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Listaconv[]> {

    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page,
    ];
    this._exampleDatabase.getAllListaconvs(this.idClinica);
    return merge(...displayDataChanges).pipe(
      map(() => {

        this.filteredData = this._exampleDatabase.data
          .slice()
          .filter((Listaconv: Listaconv) => {
            const searchStr = (
              Listaconv.NombreConvenio +
              Listaconv.Descuento +
              Listaconv.Notas +
              Listaconv.FechaCreacion +
              Listaconv.FechaMod
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });

        const sortedData = this.sortData(this.filteredData.slice());

        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {}
  /** Returns a sorted copy of the database data. */
  sortData(data: Listaconv[]): Listaconv[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'NombreConvenio':
          [propertyA, propertyB] = [a.NombreConvenio, b.NombreConvenio];
          break;
        case 'Descuento':
          [propertyA, propertyB] = [a.Descuento, b.Descuento];
          break;
        case 'Notas':
          [propertyA, propertyB] = [a.Notas, b.Notas];
          break;
        case 'FechaCreacion':
          [propertyA, propertyB] = [a.FechaCreacion, b.FechaCreacion];
          break;
        case 'FechaMod':
          [propertyA, propertyB] = [a.FechaMod, b.FechaMod];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}