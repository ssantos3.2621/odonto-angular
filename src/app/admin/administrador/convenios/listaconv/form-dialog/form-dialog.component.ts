import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ListaconvService } from '../listaconv.service';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { Listaconv } from '../listaconv.model';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/security/auth.service';
@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styles: [
  ]
})
export class FormDialogComponent implements OnInit {
  private baseURL = 'https://animatiomx.com/odonto/';
  action: string;
  dialogTitle: string;
  roomForm: FormGroup;
  room: Listaconv;
  doctores: any = [];
  doctores1: any = [];
  public filtrodoctor: FormControl = new FormControl();
  private _onDestroydoctor = new Subject<void>();
  idClinica = '';

  constructor(public dialogRef: MatDialogRef<FormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public ListaconvService: ListaconvService,
    private fb: FormBuilder,
    private httpClient:HttpClient,
    private authService: AuthService
  ) {
    this.idClinica = this.authService.getIdClinica();
    // Set the defaults
    this.action = data.action;
    if (this.action === 'edit') {
      this.dialogTitle = 'Editar Convenio';
      this.room = data.room;
    } else {
      this.dialogTitle = 'Nuevo convenio';
      this.room = new Listaconv({});
    }
    this.roomForm = this.createContactForm();
  }
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }
  //se define el formulario
  createContactForm(): FormGroup {
    return this.fb.group({
      id: [this.room.id],
      NombreConvenio: [this.room.NombreConvenio],
      Descuento: [this.room.Descuento],
      Notas: [this.room.Notas],
      Nombre: [this.room.idDoctores],
    });
  }
  //envia los datos modificados al servicio para guardarlo en la BD
  submit() {
    this.ListaconvService.updateListaconv(this.roomForm.getRawValue(),this.room.id)
  }
  nuevoConvenio(){
    console.log(this.roomForm.value);
    this.ListaconvService.addListaconv(this.roomForm.value);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    this.GetDoctores();
  }
  // GetDoctores(){
  //   const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
  //   const options: any = { 'caso': 0};
  //   const URL: any = this.baseURL + 'convenios.php';
  //   this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
  //     respuesta => {
  //       this.doctores = respuesta;
  //     });
  // }

  //obtener lista de doctores activos

  GetDoctores() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 5,idClinica: this.idClinica};
    const URL: any = this.baseURL + 'pacientes.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.doctores = respuesta;
        this.doctores1 = respuesta;
        if (this.doctores !== null) {
          this.filtrodoctor.valueChanges
          .pipe(takeUntil(this._onDestroydoctor))
          .subscribe(() => {
            this.filtrobuscadordoctor();
          });
        }
        console.log(this.doctores);
      });
  }

  // metodo para pasar los valores de doctor a un nuevo arreglo
  inicializardoctor(){
    this.doctores1 = this.doctores;
  }

  // metodo de buscador
  private filtrobuscadordoctor() {
    this.inicializardoctor();
    const search = this.filtrodoctor.value;
    this.doctores1 = this.doctores.filter((item) => {
        return (item.NombreUsuario.toLowerCase().indexOf(search.toLowerCase()) > -1,item.Apellidos.toLowerCase().indexOf(search.toLowerCase()) > -1);
      });
  }

}