import { formatDate } from '@angular/common';
export class Listaconv {
      id: number;
      idDoctores: number;
  NombreConvenio: string;
  Descuento: string;
  Notas: string;
  FechaCreacion: string;
  FechaMod: string;
  Nombre: string;
  Apellido: string;
  constructor(Listaconv) {
    {
      this.id = Listaconv.id || this.getRandomID();
      this.idDoctores=Listaconv.idDoctores || this.getRandomID();
      this.NombreConvenio = Listaconv.NombreConvenio || '';
      this.Descuento = Listaconv.Descuento || '';
      this.Notas = Listaconv.Notas || '';
      this.FechaCreacion = Listaconv.FechaCreacion || '';
      this.FechaMod = Listaconv.FechaMod || '';
      this.Nombre = Listaconv.Nombre || '';
      this.Apellido = Listaconv.Apellido || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
