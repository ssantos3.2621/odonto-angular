import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaservisComponent } from './listaservis/listaservis.component';
import { NewservisComponent } from './newservis/newservis.component';

const routes: Routes = [
  {
    path: 'nuevo-servicio',
    component: NewservisComponent,
  },
  {
    path: 'listado-servicios',
    component: ListaservisComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiciosRoutingModule { }
