import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoyeditarservicioComponent } from './nuevoyeditarservicio.component';

describe('NuevoyeditarservicioComponent', () => {
  let component: NuevoyeditarservicioComponent;
  let fixture: ComponentFixture<NuevoyeditarservicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoyeditarservicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoyeditarservicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
