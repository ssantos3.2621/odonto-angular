import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder,
} from '@angular/forms';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ListaservisService } from '../../listaservis.service';
import { Listaservis } from '../../listaservis.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/security/auth.service';

@Component({
  selector: 'app-nuevoyeditarservicio',
  templateUrl: './nuevoyeditarservicio.component.html',
  styleUrls: ['./nuevoyeditarservicio.component.sass']
})
export class NuevoyeditarservicioComponent {

  private baseURL = 'https://animatiomx.com/odonto/';
  public controldefiltro: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  accion: string;
  textodedialogo: string;
  formularioservicio: FormGroup;
  servicio: Listaservis;
  categorias: any = [];
  categorias1: any = [];
  idClinica = '';
  
  constructor(
    public dialogRef: MatDialogRef<NuevoyeditarservicioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public servicioServicio: ListaservisService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) {
        // obtenemos la accion que va hacer
        this.accion = data.action;
        this.idClinica = data.idClinica;
        if (this.accion === 'editar') {
          //texto del encabezado del modal
          this.textodedialogo = 'Editar motivo' + ' ( ' +data.servicio.Nombre+ ' )';
          //le pasamos los valores de diagnostico
          this.servicio = data.servicio;
        } else {
          this.idClinica = this.authService.getIdClinica();
          this.textodedialogo = 'Nuevo servicio';
          this.servicio = new Listaservis({});
          this.obtenercategorias(this.idClinica);
        }
        this.formularioservicio = this.formularioservicios();
  }

  //obtener las categorias de la clinica

  obtenercategorias(idC) {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 1, idClinica: idC};
    const URL: any = this.baseURL + 'servicios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.categorias = respuesta;
        this.categorias1 = respuesta;
        console.log(this.categorias);
        // costruir el filtro pasandole ondestroy
        this.controldefiltro.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filtrobuscar();
        });
      });
  }

  // metodo para pasar los valores de servios a un nuevo arreglo
  pasararreglodeservicios(){
    this.categorias1 = this.categorias;
  }
  // metodo de buscador
  private filtrobuscar() {
    this.pasararreglodeservicios();
    const buscar = this.controldefiltro.value;
    this.categorias1 = this.categorias.filter((categoria) => {
        return (categoria.Nombre.toLowerCase().indexOf(buscar.toLowerCase()) > -1);
      });
  }

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }

  // metodo para llena el formulario

  formularioservicios(): FormGroup {
    let MostrarDesh;
    let mostrarD = this.servicio.Deshabilitar
    if (mostrarD === 'HABILITADO'){
      MostrarDesh = true;
    }else{
      MostrarDesh= false;
    }
    return this.fb.group({
      id: [this.servicio.idServicios],
      Nombre: [this.servicio.Nombre],
      Descripcion: [this.servicio.Descripcion],
      Costo: [this.servicio.Costo],
      MostrarDesh: [MostrarDesh],
      idCategorias: [this.servicio.idCategorias],
      Fecharegistro: [this.servicio.Fecharegistro],
    });
  }

  submit() {
    // emppty stuff
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  // metodo para registrar en base de datos la informacion
  public confirmAdd(): void {

    console.log(this.formularioservicio.getRawValue());
    if (this.accion === 'editar') {
      console.log(this.formularioservicio.getRawValue());
      this.servicioServicio.actualizarservicio(this.formularioservicio.getRawValue()).subscribe(respuesta => {
        const res = respuesta;
        if (res.toString() === 'Se modifico'){
          this.dialogRef.close(1);
        }else {
          this.showNotification(
            'snackbar-error',
            'Ocurrio un error, intente de nuevo por favor...!!!',
            'bottom',
            'center'
          );
        }
      });
    } else {
      this.servicioServicio.agregarservicio(this.formularioservicio.getRawValue()).subscribe(respuesta => {
        const res = respuesta;
        if (res.toString() === 'Se inserto'){
          this.dialogRef.close(1);
        }else {
          this.showNotification(
            'snackbar-error',
            'Ocurrio un error, intente de nuevo por favor...!!!',
            'bottom',
            'center'
          );
        }
      });
    }
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

}

