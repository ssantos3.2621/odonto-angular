import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { Listaservis } from './listaservis.model';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class ListaservisService {
  //intercambio de datos
  dataChange: BehaviorSubject<Listaservis[]> = new BehaviorSubject<Listaservis[]>([]);
  baseURL = 'https://animatiomx.com/odonto/';
  servicioData: any;
  servicio: any = [];
  dialogData: any;

  constructor(private httpClient: HttpClient) {}
  // poner los valores de la consulta en el dataChange
  get data(): Listaservis[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */

  // obtener en un arreglo los servicios
  obtenerservicios(idC): void {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0,'idClinica': idC};
    const URL: any = this.baseURL + 'servicios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.servicio = respuesta;
        if (this.servicio !== null){
          this.dataChange.next(this.servicio);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }

  //agregar servicio
  agregarservicio(servicio: Listaservis) {
    
    this.dialogData = servicio;

    let Nombre = this.dialogData.Nombre;
    const Costo = this.dialogData.Costo;
    let Descripcion = this.dialogData.Descripcion;
    let Fecharegistro = moment(this.dialogData.Fecharegistro).format('YYYY-MM-DD');
    let idCategorias = this.dialogData.idCategorias;
    let mostrar = this.dialogData.MostrarDesh;
    let MostrarDesh = '';

    if (mostrar === true){
      MostrarDesh = 'HABILITADO';
    }else{
      MostrarDesh = 'DESHABILITADO';
    }

    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2,'Nombre':Nombre,'Descripcion':Descripcion,'Costo': Number(Costo).toFixed(2),'Deshabilitar':MostrarDesh,'idCategorias':idCategorias,'Fecharegistro':Fecharegistro};
    const URL: any = this.baseURL + 'servicios.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);

  }
   //actualizar informacion de motivo
  actualizarservicio(servicio: Listaservis) {
    this.dialogData = servicio;

    let idServicios = this.dialogData.id;
    let Nombre = this.dialogData.Nombre;
    const Costo = this.dialogData.Costo;
    let Descripcion = this.dialogData.Descripcion;
    let idCategorias = this.dialogData.idCategorias;
    let mostrar = this.dialogData.MostrarDesh;
    let MostrarDesh = '';

    if (mostrar === true){
      MostrarDesh = 'HABILITADO';
    }else{
      MostrarDesh = 'DESHABILITADO';
    }

    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'Nombre':Nombre,'Descripcion':Descripcion,'Costo': Number(Costo).toFixed(2),'Deshabilitar':MostrarDesh,
    'idCategorias':idCategorias,'idservicio':idServicios};
    const URL: any = this.baseURL + 'servicios.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);
    
  }

  //eliminar motivo
  eliminarmotivo(id: number) {
    console.log(id);

    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 3,'idMotivo':id  };
    const URL: any = this.baseURL + 'motivos.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);
    
  }

 }
