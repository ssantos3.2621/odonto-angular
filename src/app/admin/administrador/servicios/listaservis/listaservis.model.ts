import { formatDate } from '@angular/common';
export class Listaservis {
  idServicios: number;
  Nombre: string;
  Descripcion: string;
  Costo: string;
  Deshabilitar: string;
  idCategorias: string;
  Fecharegistro: string;
  NombreCat: string;
  fechahoy = new Date();
  constructor(Listaservis) {
    {
      this.idServicios = Listaservis.idServicios || this.getRandomID();
      this.Nombre = Listaservis.Nombre || '';
      this.Descripcion = Listaservis.Descripcion || '';
      this.Costo = Listaservis.Costo || '';
      this.Deshabilitar = Listaservis.Deshabilitar || '';
      this.Fecharegistro = Listaservis.Fecharegistro || this.fechahoy;
      this.idCategorias = Listaservis.idCategorias || '';
      this.NombreCat = Listaservis.NombreCat || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
