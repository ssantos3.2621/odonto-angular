import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, fromEvent, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { ListaservisService } from './listaservis.service';
import { Listaservis } from './listaservis.model';
import { NewservisComponent } from '../newservis/newservis.component';
import { NuevoyeditarservicioComponent } from './abcservicio/nuevoyeditarservicio/nuevoyeditarservicio.component';
import { AuthService } from 'src/app/shared/security/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listaservis',
  templateUrl: './listaservis.component.html',
  styles: [
  ]
})
export class ListaservisComponent implements OnInit {
columnaservicio = [
    'nombre',
    'categoria',
    'des',
    'costo',
    'habili',
    'actions',
  ];
  servicioDB: ListaservisService | null;
  datosservicios: datosservicios | null;
  selection = new SelectionModel<Listaservis>(true, []);
  index: number;
  id: number;
  idClinica = '';


  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public ListaservisService: ListaservisService,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router,
  ) {
    this.idClinica = this.authService.getIdClinica();
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  ngOnInit() {
    if (this.idClinica !== null){
      this.cargandodatosservicios();
    }else{
      this.cargandodatosservicios();
      let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
      snack.onAction().subscribe( () => this.iraclinica());
    }
  }

  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }

  refrescartablaservicios() {
    this.cargandodatosservicios();
  }
  private refrescartabladeservicios() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  public cargandodatosservicios() {
    this.servicioDB = new ListaservisService(this.httpClient);
    this.datosservicios = new datosservicios(
      this.servicioDB,
      this.paginator,
      this.sort,
      this.idClinica
    );
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.datosservicios) {
          return;
        }
        this.datosservicios.filter = this.filter.nativeElement.value;
      });
  }
  nuevoMotivo() {
    const dialogRef = this.dialog.open(NuevoyeditarservicioComponent, {
      width: '30%',
      data: {
        action: 'nuevo',
        idClinica: 1
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.servicioDB.dataChange.value.unshift(
          //this.diagnosticoBasededatos.getDialogData()
        );
        this.refrescartablaservicios();
        this.showNotification(
          'snackbar-success',
          'Servicio guardado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  editarServicio(row) {
    this.id = row.idServicios;
    console.log(this.id);
    const dialogRef = this.dialog.open(NuevoyeditarservicioComponent, {
      width: '30%',
      data: {
        servicio: row,
        action: 'editar',
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        const foundIndex = this.servicioDB.dataChange.value.findIndex(
          (x) => x.idServicios === this.id
        );
        this.refrescartablaservicios();
        this.refrescartabladeservicios();
        this.showNotification(
          'black',
          'Servicio editado correctamente...!!!',
          'bottom',
          'center'
        );
      }
    });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}

export class datosservicios extends DataSource<Listaservis> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  filteredData: Listaservis[] = [];
  renderedData: Listaservis[] = [];
  constructor(
    public _servicioDB: ListaservisService,
    public _paginator: MatPaginator,
    public _sort: MatSort,
    public idclinica
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Listaservis[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._servicioDB.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page,
    ];
    this._servicioDB.obtenerservicios(this.idclinica);
    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._servicioDB.data
          .slice()
          .filter((Listaservis: Listaservis) => {
            const searchStr = (
              Listaservis.Nombre +
              Listaservis.Descripcion +
              Listaservis.Costo +
              Listaservis.Deshabilitar +
              Listaservis.NombreCat
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());
        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }
  disconnect() {}
  /** Returns a sorted copy of the database data. */
  sortData(data: Listaservis[]): Listaservis[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.idServicios, b.idServicios];
          break;
        case 'nombre':
          [propertyA, propertyB] = [a.Nombre, b.Nombre];
          break;
        case 'categoria':
          [propertyA, propertyB] = [a.NombreCat, b.NombreCat];
          break;
        case 'des':
          [propertyA, propertyB] = [a.Descripcion, b.Descripcion];
          break;
        case 'costo':
          [propertyA, propertyB] = [a.Costo, b.Costo];
          break;
        case 'deshabilitar':
          [propertyA, propertyB] = [a.Deshabilitar, b.Deshabilitar];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}