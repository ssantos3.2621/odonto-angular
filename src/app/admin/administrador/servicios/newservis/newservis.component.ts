import { Component, OnInit } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ListaservisService } from '../listaservis/listaservis.service';
import { Listaservis } from '../listaservis/listaservis.model';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/security/auth.service';

@Component({
  selector: 'app-newservis',
  templateUrl: './newservis.component.html',
  styles: [
  ]
})
export class NewservisComponent implements OnInit {

  private baseURL = 'https://animatiomx.com/odonto/';
  public controldefiltro: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  formularioservicio: FormGroup;
  servicio: Listaservis;
  categorias: any = [];
  categorias1: any = [];
  idClinica = '';

    roomForm: FormGroup;
  constructor(
    public servicioServicio: ListaservisService,
    private fb: FormBuilder,
    public httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router
    ) {
      this.idClinica = this.authService.getIdClinica();
      if (this.idClinica === null){
        let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
        snack.onAction().subscribe( () => this.iraclinica());
      }
      this.servicio = new Listaservis({});
      this.obtenercategorias(this.idClinica);
      this.formularioservicio = this.formularioservicios();
  }

  ngOnInit() {
  }

  iraclinica(){
    this.router.navigate(["/admin/room/add-allotment"]);
  }

  //obtener las categorias de la clinica

  obtenercategorias(idC) {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 1, idClinica: idC};
    const URL: any = this.baseURL + 'servicios.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.categorias = respuesta;
        this.categorias1 = respuesta;
        console.log(this.categorias);
        // costruir el filtro pasandole ondestroy
        this.controldefiltro.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filtrobuscar();
        });
      });
  }
  
  // metodo para pasar los valores de servios a un nuevo arreglo
  pasararreglodeservicios(){
    this.categorias1 = this.categorias;
  }
  // metodo de buscador
  private filtrobuscar() {
    this.pasararreglodeservicios();
    const buscar = this.controldefiltro.value;
    this.categorias1 = this.categorias.filter((categoria) => {
        return (categoria.Nombre.toLowerCase().indexOf(buscar.toLowerCase()) > -1);
      });
  }
  
  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  getErrorMessage() {
    return this.formControl.hasError('required')
      ? 'Required field'
      : this.formControl.hasError('email')
      ? 'Not a valid email'
      : '';
  }
  
  // metodo para llena el formulario
  
  formularioservicios(): FormGroup {
    let MostrarDesh;
    let mostrarD = this.servicio.Deshabilitar
    if (mostrarD === 'HABILITADO'){
      MostrarDesh = true;
    }else{
      MostrarDesh= false;
    }
    return this.fb.group({
      id: [this.servicio.idServicios],
      Nombre: [this.servicio.Nombre],
      Descripcion: [this.servicio.Descripcion],
      Costo: [this.servicio.Costo],
      MostrarDesh: [MostrarDesh],
      idCategorias: [this.servicio.idCategorias],
      Fecharegistro: [this.servicio.Fecharegistro],
    });
  }
  
  onSubmit() {
    this.servicioServicio.agregarservicio(this.formularioservicio.getRawValue()).subscribe(respuesta => {
      const res = respuesta;
      if (res.toString() === 'Se inserto'){
        this.showNotification(
          'snackbar-success',
          'Servicio guardado correctamente...!!!',
          'bottom',
          'center'
        );
        this.limpiarvariables();
      }else {
        this.showNotification(
          'snackbar-error',
          'Ocurrio un error, intente de nuevo por favor...!!!',
          'bottom',
          'center'
        );
      }
    });
    
  }
  //metodo para limpiarformulario

  limpiarvariables(){
    this.formularioservicio.reset();
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

}
