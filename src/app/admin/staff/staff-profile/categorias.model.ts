import { formatDate } from '@angular/common';
export class Categorias {
    id: number;
  name: string;

  constructor(staff) {
    {
      this.id = staff.id || this.getRandomID();
      this.name = staff.name || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
