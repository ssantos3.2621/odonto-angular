import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Categorias } from './categorias.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {
 private readonly API_URL = 'assets/data/staff.json';
  dataChange: BehaviorSubject<Categorias[]> = new BehaviorSubject<Categorias[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  constructor(private httpClient: HttpClient) {}
  get data(): Categorias[] {
    return this.dataChange.value;
  }
  getDialogData() {
    return this.dialogData;
  }
  /** CRUD METHODS */
  getAllStaffs(): void {
    this.httpClient.get<Categorias[]>(this.API_URL).subscribe(
      data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
    );
  }
  // DEMO ONLY, you can find working methods below
  addStaff(staff: Categorias): void {
    this.dialogData = staff;
  }
  updateStaff(staff: Categorias): void {
    this.dialogData = staff;
  }
  deleteStaff(id: number): void {
    console.log(id);
  }
}
