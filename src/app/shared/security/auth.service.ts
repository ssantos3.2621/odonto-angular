import { Role } from "./role";
import { User } from "./user";
import { Injectable } from "@angular/core";
import { of } from "rxjs";
import { LocalService } from "./local.service";
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private baseURL = 'https://animatiomx.com/odonto/';
  isLogin = false;
  roleAs: string;

  constructor(
    private localService: LocalService,
    private httpClient:HttpClient
  ) {}

  // metodo para verificar si el usuario existe y recuperar su informacion para guardarlo en storage 

  login(uname: string, pwd: string) {
    let err = '';

      return this.httpClient.post<any>(this.baseURL + 'usuario.php', { 'caso': 1,'email': uname, 'Password': pwd })
          .pipe(map(respuesta => {
            console.log(respuesta);

            if (respuesta.toString() === 'La contrasena es incorrecta') {
              console.log('La contrasena es incorrecta')
              err = 'El correo u contraseña son incorrectos';
              this.roleAs = "";
              this.isLogin = false;
              this.localService.setJsonValue("STATE", "false");
              return ({ success: this.isLogin, role: this.roleAs, error: err});
            } 
            else if(respuesta.toString() === 'El usuario no esta registrado'){
              console.log('El usuario no esta registrado')
              err = 'El usuario no esta registrado';
              this.roleAs = "";
              this.isLogin = false;
              this.localService.setJsonValue("STATE", "false");
              return ({ success: this.isLogin, role: this.roleAs, error: err});
            } 
            else {

              console.log(respuesta)
              this.roleAs = respuesta.role;
              this.localService.setJsonValue("STATE", "true");
              this.localService.setJsonValue("ROLE", respuesta.role);
              this.localService.setJsonValue("USERIMG", respuesta.img);
              this.localService.setJsonValue("FULLNAME", respuesta.firstName + ' ' + respuesta.lastName);
              this.localService.setJsonValue("IDUSUARIO", respuesta.id);
              this.localService.setJsonValue("IDCLINICA", respuesta.idclinica);
              this.localService.setJsonValue("IDSUCURSAL", respuesta.sucursal);
              this.isLogin = true;
              return ({ success: this.isLogin, role: this.roleAs, idclinica: respuesta.idclinica, error: ''});
            }
              
          }));


  }

  // metodo para cerrar session y limpiar las variables de storage del usuario

  logout() {
    this.isLogin = false;
    this.roleAs = "";
    this.localService.setJsonValue("STATE", "false");
    this.localService.setJsonValue("ROLE", "");
    this.localService.setJsonValue("FULLNAME", "");
    this.localService.setJsonValue("USERIMG", "");
    this.localService.setJsonValue("IDUSUARIO", "");
    this.localService.setJsonValue("IDCLINICA", "");
    this.localService.setJsonValue("IDSUCURSAL", "");
    return of({ success: this.isLogin, role: "" });
  }

  // comprobar si el usuario tiene una session iniciada 

  isLoggedIn() {
    const loggedIn = this.localService.getJsonValue("STATE");
    if (loggedIn === "true") {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
    return this.isLogin;
  }

  //metodo para obtener el rol del usuario

  getRole() {
    return this.localService.getJsonValue("ROLE");
  }

  // metodo para obtener el nombre del usuario

  getUserFullName() {
    return this.localService.getJsonValue("FULLNAME");
  }

  //metodo para obtener la imagen del usuario

  getUserImg() {
    return this.localService.getJsonValue("USERIMG");
  }

  //metodo para obtener el id del usuario

  getIdUser(){
    return this.localService.getJsonValue("IDUSUARIO")
  }

  //metodo para obtener la clinica del uuario  
  
  getIdClinica(){
    return this.localService.getJsonValue("IDCLINICA")
  }

  //metodo para obtener el id de sucursal 

  getIdSucursal(){
    return this.localService.getJsonValue("IDSUCURSAL")
  }

}
