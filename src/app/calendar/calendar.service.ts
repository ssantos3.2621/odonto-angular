import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Calendar } from './calendar.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class CalendarService {
    baseURL = 'https://animatiomx.com/odonto/';
    private readonly API_URL = 'assets/data/calendar.json';
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }
    dataChange: BehaviorSubject<Calendar[]> = new BehaviorSubject<Calendar[]>(
        []
    );
    // Temporarily stores data from dialogs
    dialogData: any;
    constructor(private httpClient: HttpClient) { }
    get data(): Calendar[] {
        return this.dataChange.value;
    }
    getDialogData() {
        return this.dialogData;
    }
    getAllCalendars(): Observable<Calendar[]> {
        return this.httpClient.get<Calendar[]>(this.API_URL)
            .pipe(
                catchError(this.errorHandler)
            )
    }

    addUpdateCalendar(calendar: Calendar): void {
        this.dialogData = calendar;
    }
    deleteCalendar(calendar: Calendar): void {
        this.dialogData = calendar;
    }
    errorHandler(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
      // Modifica cita
  modificarCita(cita: Calendar): void {
    this.dialogData = cita;
    let idCita = this.dialogData.id;
    let fecha = this.dialogData.idDia;
    let horaInicio = this.dialogData.obser;
    let horaFIn = this.dialogData.descri;
    let Motivo = this.dialogData.title;
    let Observa = this.dialogData.details;
    let Servicio = this.dialogData.idE;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 6, 'fecha': fecha, 'horai': horaInicio, 'horaf': horaFIn, 'motivo': Motivo, 'obser': Observa,
                           'idS': Servicio, 'idc': idCita};
    const URL: any = this.baseURL + 'citas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
      });
  }
}
